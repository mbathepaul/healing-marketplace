import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:healing/core/routes.dart';
import 'package:healing/core/providers/theme_provider.dart';
import 'package:provider/provider.dart';
import 'resources/dio_wrapper.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_rounded_date_picker/flutter_rounded_date_picker.dart';

class Healing extends StatelessWidget {
  final bool is_login;
  Healing({this.is_login});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en', 'US'), // English
      ],
      title: DotEnv().env['APP_NAME'],
      debugShowCheckedModeBanner: false,
      routes: routes,
      initialRoute: this.is_login == true ? '/container' : '/begin',
      theme: context.watch<ThemeProvider>().appTheme,
      navigatorKey: dioWrapper.navigatorKey,
    );
  }
}
