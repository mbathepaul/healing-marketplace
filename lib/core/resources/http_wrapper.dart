import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class Http {
  uploadFile(String url, File file, String price, String category, String name,
      token) async {
    print("Uploading...");
    //create multipart request for POST or PATCH method
    String myUrl = url;
    final prefs = await SharedPreferences.getInstance();
    final key = token;
    final value = prefs.get(key) ?? 0;

    var request = http.MultipartRequest("POST", Uri.parse(myUrl));

    //create multipart using filepath, string or bytes
    request.headers["Authorization"] = "Token $token";
    var rec = await http.MultipartFile.fromPath("image", file.path);

    //add multipart to request
    request.files.add(rec);
    request.fields["price"] = price as String;
    request.fields["category"] = category;
    request.fields["label"] = name;

    var response = await request.send();

    //Get the response from the server
    var responseData = await response.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);
    print(responseData);
    return responseData;
  }
}
