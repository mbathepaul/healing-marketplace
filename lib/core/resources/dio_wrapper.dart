import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class DioWrapper {
  Dio _dio;
  BaseOptions _options;

  /// The navigatorKey will further be used to perform redirections
  /// Example:
  ///   `navigatorKey.currentState.push(...)`
  /// Useful if you need for example to redirect a user back to
  /// the login page after a 401 redirection from the API
  ///
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  DioWrapper() {
    _options = BaseOptions(
      baseUrl: DotEnv().env["ENDPOINT"],
      headers: {
        "Accept": "application/json",
        "Content-type": "application/json",
      },
    );
    _dio = Dio(_options);
  }

  Future post(route, {params}) {
    print("paramss");
    print(params);
    return _dio.post(route, data: params);
  }

  Future put(route, {params}) {
    return _dio.put(route, data: params);
  }

  Future get(route, {bool useBase = false}) {
    if (useBase)
      return _dio.get(route);
    else
      return Dio().get(route);
  }

  Future gettherapetist(route, token, {bool useBase = false}) {
    Dio _dio3;
    print("autor");
    print("Authorization : " + "Token $token");

    BaseOptions _optionse = BaseOptions(
      baseUrl: DotEnv().env["ENDPOINT"],
      headers: {
        "Accept": "application/json",
        "Content-type": "application/json",
        "Authorization": "Token $token",
      },
    );
    _dio3 = Dio(_optionse);

    if (useBase)
      return _dio3.get(route);
    else
      return Dio().get(route);
  }

  Future gettoken(route, token, {bool useBase = false}) {
    Dio _dio3;
    print("autor");
    print("Authorization : " + "Token $token");

    BaseOptions _optionse = BaseOptions(
      baseUrl: DotEnv().env["ENDPOINT"],
      headers: {
        "Accept": "application/json",
        "Content-type": "application/json",
        "Authorization": "Token $token",
      },
    );
    _dio3 = Dio(_optionse);

    if (useBase)
      return _dio3.get(route);
    else
      return Dio().get(route);
  }

  Future delete(route) {
    return _dio.delete(route);
  }

  Future posttoken(route, token, {params}) {
    Dio _dio1;
    print("autorisation");
    print("Authorization : " + "Token $token");

    BaseOptions _optionse = BaseOptions(
      baseUrl: DotEnv().env["ENDPOINT"],
      headers: {
        "Accept": "application/json",
        "Content-type": "application/json",
        "Authorization": "Token $token",
      },
    );
    _dio1 = Dio(_optionse);

    print(params);
    return _dio1.post(route, data: params);
  }

  Future deletetoken(route, token, {params}) {
    Dio _dio1;
    print("autorisation");
    print("Authorization : " + "Token $token");

    BaseOptions _optionse = BaseOptions(
      baseUrl: DotEnv().env["ENDPOINT"],
      headers: {
        "Accept": "application/json",
        "Content-type": "application/json",
        "Authorization": "Token $token",
      },
    );
    _dio1 = Dio(_optionse);

    print(params);
    return _dio1.delete(route, data: params);
  }

  Future putoken(route, token, {params}) {
    Dio _dio1;
    print("autorisation");
    print("Authorization : " + "Token $token");

    BaseOptions _optionse = BaseOptions(
      baseUrl: DotEnv().env["ENDPOINT"],
      headers: {
        "Accept": "application/json",
        "Content-type": "application/json",
        "Authorization": "Token $token",
      },
    );
    _dio1 = Dio(_optionse);

    print(params);
    return _dio1.put(route, data: params);
  }

  Future<Response> sendFile(String url, File file, token) async {
    Dio _dio2;

    var len = await file.length();
    BaseOptions _optionse = BaseOptions(
      baseUrl: DotEnv().env["ENDPOINT"],
      headers: {
        "Accept": "application/json",
        "Content-type": "application/json",
        "Authorization": "Token $token",
      },
    );
    _dio2 = Dio(_optionse);

    String fileName = file.path.split('/').last;
    FormData formData = FormData.fromMap({
      "image": await MultipartFile.fromFile(file.path, filename: fileName),
    });
    Response response = await _dio2.post(url, data: formData);
    return response;
  }

  Future<Response> send_products(String url, File file, double price,
      String category, String name, token) async {
    Dio _dio2;

    var len = await file.length();
    BaseOptions _optionse = BaseOptions(
      baseUrl: DotEnv().env["ENDPOINT"],
      headers: {
        "Accept": "application/json",
        "Content-type": "multipart/form-data",
        "Authorization": "Token $token",
      },
    );
    _dio2 = Dio(_optionse);
    print(
        "on ppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppp");

    String fileName = file.path.split('/').last;
    print(fileName);
    FormData formData = FormData.fromMap({
      "price": "35",
      "category": category,
      "label": name,
      "image": await MultipartFile.fromFile(file.path, filename: fileName)
    });
    print(formData);
    Response response = await _dio2.post(url, data: formData);
    return response;
  }

  Future<Response> sendFiles(String url, List<File> files, token) async {
    Dio _dio2;

    BaseOptions _optionse = BaseOptions(
      baseUrl: DotEnv().env["ENDPOINT"],
      headers: {
        "Accept": "application/json",
        "Content-type": "application/json",
        "Authorization": "Token $token",
      },
    );
    _dio2 = Dio(_optionse);

    var formData = FormData();
    for (var file in files) {
      formData.files.addAll([
        MapEntry("image", await MultipartFile.fromFile(file.path)),
      ]);
    }
    /*
    print("longeur");
    print(multipathFiles.length);
    FormData formData = FormData.fromMap({
      "image": multipathFiles,
    });
    */
    Response response = await _dio2.post(url, data: formData);
    return response;
  }
}

final DioWrapper dioWrapper = DioWrapper();
