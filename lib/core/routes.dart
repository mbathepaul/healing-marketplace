import 'package:flutter/material.dart';
import 'package:healing/screens/auth/login.dart';
import 'package:healing/screens/auth/register.dart';
import 'package:healing/screens/dashboard/container.dart';
import 'package:healing/screens/package.dart';
import 'package:healing/screens/sign_up.dart';
import 'package:healing/screens/therapeutist.dart';
import 'package:healing/screens/therapeutistList.dart';
import 'package:healing/screens/welcome.dart';
import 'package:healing/screens/profile.dart';
import 'package:healing/screens/profile_niew.dart';
import 'package:healing/screens/blog.dart';
import 'package:healing/screens/addProduct.dart';
import 'package:healing/screens/payement.dart';
import 'package:healing/screens/articleDetail.dart';
import 'package:healing/screens/addProduct_Order.dart';
import 'package:healing/screens/commentstherapist.dart';
import 'package:healing/screens/listesearch.dart';
import 'package:healing/screens/auth/resetlogin.dart';
import 'package:healing/screens/productAdd.dart';
import 'package:healing/screens/booking.dart';
import 'package:healing/screens/calandar.dart';

final Map<String, Widget Function(BuildContext)> routes = {
  "/begin": (context) => WelcomePage(),
  "/container": (context) => DashboardContainer(),
  "/login": (context) => LoginScreen(),
  "/register": (context) => RegisterScreen(),
  "/therapeutistList": (context) => TherapeutistList(),
  "/therapeutist": (context) => TherapeutistDetails(),
  "/package": (context) => PackageScreen(),
  "/signup": (context) => SignUp(),
  "/profile": (context) => ProfilePage(),
  "/proifele_new": (context) => ProfileUI(),
  "/blog": (context) => BlogPage(),
  "/add_product": (context) => AddProduct(),
  "/payement": (context) => Payement(),
  "/article_detail": (context) => ArticleDetail(),
  "/product_order": (context) => AddProductOrder(),
  "/comment_therapist": (context) => CommentsTherapist(),
  '/searchresult': (context) => SearchtList(),
  '/resetpassword': (context) => ResetScreen(),
  '/product_add': (context) => ProductAddPage(),
  '/booking': (context) => Booking(),
  '/calandar': (context) => MyHomePage(),
};
