import 'package:flutter/material.dart';

class Category {
  final String title;
  int therapeutist;
  final Color color;
  final Image image;

  Category({@required this.title, this.therapeutist, this.color, this.image});
}
