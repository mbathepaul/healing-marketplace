import 'package:flutter/material.dart';
import 'package:healing/core/models/comment.dart';

class Article {
  final String author;
  final String category;
  final String content;
  final String createdAt;
  final String updateAt;
  final int nb_comments;
  final List<Comm> comments;
  final String label;
  final String image;
  final String id;

  Article(
      {this.createdAt,
      this.label,
      this.updateAt,
      this.image,
      this.category,
      this.author,
      this.nb_comments,
      this.content,
      this.id,
      this.comments}) {}

  factory Article.fromJson(Map<String, dynamic> json) {
    List<Comm> list2 = [];
    List<Comm>.from(
      ((json["comments"]) as List).map(
        (son) {
          Comm com = Comm(
            article: son["article"] as String,
            content: son["content"] as String,
            createdAt: son["created_at"] as String,
            updateAt: son["updated_at"] as String,
            commentator: son["commentator"] as String,
          );
          list2.add(com);
        },
      ),
    );
    try {
      Article article = Article(
        id: json["id"] != null ? json["id"] as String : null,
        createdAt:
            json["created_at"] != null ? json["created_at"] as String : null,
        label: json["label"] != null ? json["label"] as String : null,
        updateAt:
            json["update_at"] != null ? json["update_at"] as String : null,
        image: json['image'] as String,
        author: json["author"] != null ? json["email"] as String : null,
        content: json["content"] != null ? json["content"] as String : null,
        category: json["category"] != null ? json["category"] as String : null,
        nb_comments:
            json["nb_comments"] != null ? json["nb_comments"] as int : null,
        comments: list2,
      );
      return article;
    } catch (err) {
      return null;
    }
  }

  Map<String, dynamic> toJson() => {
        "outhor": author,
        "comments": comments,
        "label": label,
        "image": image,
        "content": content,
        "nb_comments": nb_comments,
        "createdAt": createdAt,
        "updateAt": updateAt,
        "image": image,
        "id": id,
      };
}
