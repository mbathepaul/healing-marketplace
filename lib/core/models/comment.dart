import 'package:flutter/material.dart';

class Comm {
  final String commentator;
  final String content;
  final String createdAt;
  final String updateAt;

  final String article;

  Comm({
    this.createdAt,
    this.article,
    this.updateAt,
    this.commentator,
    this.content,
  }) {}

  factory Comm.fromJson(Map<String, dynamic> json) {
    try {
      Comm article = Comm(
        article: json["article"] != null ? json["article"] as String : null,
        createdAt:
            json["created_at"] != null ? json["created_at"] as String : null,
        commentator:
            json["commentator"] != null ? json["commentator"] as String : null,
        updateAt:
            json["update_at"] != null ? json["update_at"] as String : null,
        content: json["content"] != null ? json["content"] as String : null,
      );
      return article;
    } catch (err) {
      return null;
    }
  }

  Map<String, dynamic> toJson() => {
        "commentator": commentator,
        "article": article,
        "content": content,
        "createdAt": createdAt,
        "updateAt": updateAt,
      };
}
