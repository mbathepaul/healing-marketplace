import 'package:flutter/material.dart';
import 'package:healing/core/models/category.dart';

import 'comment.dart';

class Product {
  final String id;
  final String label;
  final String createdAt;
  final String updateAt;
  final String code;
  final String owner;
  final String category;
  final double price;
  final String image;

  Product({
    this.id,
    this.createdAt,
    this.updateAt,
    this.label,
    this.image,
    this.owner,
    this.category,
    this.price,
    this.code,
  }) {}

  factory Product.fromJson(Map<String, dynamic> json, id) {
    try {
      Product doctor = Product(
        id: id != null ? id as String : "",
        createdAt:
            json["created_at"] != null ? json["created_at"] as String : null,
        updateAt:
            json["update_at"] != null ? json["update_at"] as String : null,
        category: json["category"] != null ? json["category"] as String : null,
        code: json["code"] != null ? json["code"] as String : null,
        label: json["label"] != null ? json["label"] as String : null,
        owner: json["owner"] != null ? json["owner"] as String : null,
        price: json["price"] != null ? json["price"] as double : null,
        image: json['image'] != null ? json['image'] as String : null,
      );
      return doctor;
    } catch (err) {
      return null;
    }
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "createdAt": createdAt,
        "updateAt": updateAt,
        "image": image,
        "code": code,
        "label": label,
        "image": image,
        "price": price,
      };
}
