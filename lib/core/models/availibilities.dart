class Availibilitie {
  String id;
  String therapist;
  String createdAt;
  String updateAt;
  String start_date;
  String end_date;

  Availibilitie(
      {this.id,
      this.createdAt,
      this.updateAt,
      this.start_date,
      this.end_date,
      this.therapist}) {}

  factory Availibilitie.fromJson(Map<String, dynamic> json) {
    try {
      Availibilitie doctor = Availibilitie(
        id: json["id"] != null ? json["id"] as String : "",
        createdAt:
            json["created_at"] != null ? json["created_at"] as String : null,
        updateAt:
            json["update_at"] != null ? json["update_at"] as String : null,
        therapist:
            json["therapist"] != null ? json["therapist"] as String : null,
        start_date:
            json["start_date"] != null ? json["start_date"] as String : null,
        end_date: json["end_date"] != null ? json["end_date"] as String : null,
      );
      return doctor;
    } catch (err) {
      return null;
    }
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "createdAt": createdAt,
        "updateAt": updateAt,
        "therapist": therapist,
        "end_date": end_date,
        "start_date": start_date,
      };
}
