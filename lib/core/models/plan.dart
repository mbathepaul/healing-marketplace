import 'package:flutter/material.dart';

class Plan {
  final String id;
  final String url;
  final String label;
  final String amount;
  final String period;
  final String created_at;
  final String update_at;

  Plan({
    this.id,
    this.url,
    this.label,
    this.amount,
    this.period,
    this.created_at,
    this.update_at,
  }) {}

  factory Plan.fromJson(Map<String, dynamic> json) {
    return Plan(
      id: json["id"] as String,
      url: json["url"] as String,
      label: json["label"] as String,
      amount: json["amount"] as String,
      period: json["period"] as String,
      created_at: json["created_at"] as String,
      update_at: json["update_at"] as String,
    );
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "url": url,
        "label": label,
        "amount": amount,
        "period": period,
        "created_at": created_at,
        "update_at": update_at,
      };
}
