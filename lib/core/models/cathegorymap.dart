import 'package:flutter/material.dart';

class Categorymap {
  final String id;
  final String label;
  final String description;
  final String created_at;
  final String update_at;

  Categorymap(
      {@required this.id,
      this.label,
      this.description,
      this.created_at,
      this.update_at});
  factory Categorymap.fromJson(Map<String, dynamic> json) => (json["label"]);
}
