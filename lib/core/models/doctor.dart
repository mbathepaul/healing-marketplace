import 'package:flutter/material.dart';

import 'comment.dart';

class Doctor {
  final String id;
  final String user;
  final String label;
  final String address;
  final String phone;
  final String description;
  final String createdAt;
  final String email;
  final String updateAt;
  final double rate;
  final String website;
  final String facebook;
  final String instalgram;
  final String region;
  final String city;
  final int nb_reviews;
  final String end_subscription;
  final List<String> tags;
  final List<Comm> reviews;

  final List<String> photos;

  final String image;

  Doctor(
      {this.id,
      this.user,
      this.address,
      this.phone,
      this.description,
      this.createdAt,
      this.updateAt,
      this.label,
      this.image,
      this.email,
      this.website,
      this.facebook,
      this.instalgram,
      this.region,
      this.city,
      this.rate,
      this.photos,
      this.end_subscription,
      this.nb_reviews,
      this.tags,
      this.reviews}) {}

  factory Doctor.fromJson(Map<String, dynamic> json, id) {
    List<String> list = [];
    List<Comm> list2 = [];
    if (json["tags"] != null) {
      List<String>.from(
        ((json["tags"]) as List).map(
          (son) {
            if (son != null) {
              list.add(son as String);
            }
          },
        ),
      );
    }

    try {
      if (json["reviews"] != null) {
        List<String>.from(
          ((json["reviews"]) as List).map(
            (son) {
              Comm comm = Comm(
                  article: son["therapist"] as String,
                  commentator: son["client"] as String,
                  content: son["description"] as String,
                  createdAt: son["created_at"] as String,
                  updateAt: son["update_at"] as String);
              list2.add(comm);
            },
          ),
        );
      }
    } catch (e) {
      print(e);
    }

    try {
      Doctor doctor = Doctor(
        id: id != null ? id as String : "",
        user: json["user"] != null ? json["user"] as String : null,
        label: json["category"] != null ? json["category"] as String : null,
        address: json["address"] != null ? json["address"] as String : null,
        phone: json["phone"] != null ? json["phone"] as String : null,
        description:
            json["description"] != null ? json["description"] as String : null,
        createdAt:
            json["created_at"] != null ? json["created_at"] as String : null,
        updateAt:
            json["update_at"] != null ? json["update_at"] as String : null,
        image: json["photos"][0]['image'] != null
            ? json["photos"][0]['image'] as String
            : null,
        email: json["email"] != null ? json["email"] as String : null,
        rate: json["rating"] != null ? json["rating"].toDouble() : null,
        instalgram:
            json["instagram"] != null ? json["instagram"] as String : null,
        facebook: json["facebook"] != null ? json["facebook"] as String : null,
        website: json["website"] != null ? json["website"] as String : null,
        region: json["region"] != null ? json["region"] as String : null,
        city: json["city"] != null ? json["city"] as String : null,
        end_subscription: json["end_subscription"] != null
            ? json["end_subscription"] as String
            : null,
        nb_reviews:
            json["nb_reviews"] != null ? json["nb_reviews"] as int : null,
        tags: list.length > 0 ? list[0].split(",") : [],
        reviews: list2,
      );
      return doctor;
    } catch (err) {
      return null;
    }
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "user": user,
        "label": label,
        "address": address,
        "phone": phone,
        "description": description,
        "createdAt": createdAt,
        "updateAt": updateAt,
        "image": image,
        "rate": rate,
        "email": email,
        "website": website,
        "facebook": facebook,
        "instalgram": instalgram,
        "region": region,
        "city": city,
        "end_subscription": end_subscription,
        "nb_reviews": nb_reviews,
        "tags": tags,
        "reviews": reviews,
      };
}
