import 'package:flutter/material.dart';
import 'package:healing/core/models/category.dart';

import 'comment.dart';

class Booki {
  String id;
  String therapist;
  String createdAt;
  String updateAt;
  String booker;
  int status;
  String start_date;
  String end_date;

  Booki(
      {this.id,
      this.createdAt,
      this.updateAt,
      this.booker,
      this.status,
      this.start_date,
      this.end_date,
      this.therapist}) {}

  factory Booki.fromJson(Map<String, dynamic> json) {
    try {
      Booki doctor = Booki(
        id: json["id"] != null ? json["id"] as String : "",
        createdAt:
            json["created_at"] != null ? json["created_at"] as String : null,
        updateAt:
            json["update_at"] != null ? json["update_at"] as String : null,
        therapist:
            json["therapist"] != null ? json["therapist"] as String : null,
        booker: json["booker"] != null ? json["booker"] as String : null,
        start_date:
            json["start_date"] != null ? json["start_date"] as String : null,
        status: json["status"] != null ? json["status"] as int : null,
        end_date: json["end_date"] != null ? json["end_date"] as String : null,
      );
      return doctor;
    } catch (err) {
      return null;
    }
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "createdAt": createdAt,
        "updateAt": updateAt,
        "status": status,
        "therapist": therapist,
        "booker": booker,
        "end_date": end_date,
        "start_date": start_date,
      };
}
