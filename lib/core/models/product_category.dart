import 'package:flutter/material.dart';
import 'package:healing/core/models/product.dart';

import 'comment.dart';

class ProductCategory {
  final String id;

  final String label;

  final String description;
  final String createdAt;

  final String updateAt;
  final List<Product> products;

  ProductCategory({
    this.id,
    this.description,
    this.createdAt,
    this.updateAt,
    this.label,
    this.products,
  }) {}

  factory ProductCategory.fromJson(Map<String, dynamic> json) {
    List<Product> list2 = [];

    try {
      if (json["products"] != null) {
        //print(
        //   "commmmmmmmmmmmmnvoanovnanvoanvooooooooooiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
        List<String>.from(
          ((json["products"]) as List).map(
            (son) {
              Product comm = Product(
                  label: son["label"] as String,
                  price: double.parse(son["price"]),
                  category: son["category"] as String,
                  owner: son["owner"] as String,
                  code: son["code"] as String,
                  image: son["image"] as String,
                  id: son["id"] as String,
                  createdAt: son["created_at"] as String,
                  updateAt: son["update_at"] as String);
              list2.add(comm);
            },
          ),
        );
      }
    } catch (e) {
      print(e);
    }

    try {
      ProductCategory doctor = ProductCategory(
        id: json["id"] != null ? json["id"] as String : "",
        description:
            json["description"] != null ? json["description"] as String : null,
        label: json["label"] != null ? json["label"] as String : null,
        createdAt:
            json["created_at"] != null ? json["created_at"] as String : null,
        updateAt:
            json["update_at"] != null ? json["update_at"] as String : null,
        products: list2,
      );
      return doctor;
    } catch (err) {
      return null;
    }
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "description": description,
        "createdAt": createdAt,
        "updateAt": updateAt,
        "label": label,
      };
}
