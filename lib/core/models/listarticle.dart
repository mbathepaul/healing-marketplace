import 'package:flutter/material.dart';
import 'package:healing/core/models/comment.dart';

import 'article.dart';

class ListArticle {
  final List<Article> articles;
  final String hasnext;

  ListArticle({
    this.articles,
    this.hasnext,
  }) {}
}
