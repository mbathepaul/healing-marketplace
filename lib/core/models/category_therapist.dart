import 'package:flutter/material.dart';

class CategoryTherapist {
  final String label;
  final String id;

  CategoryTherapist({this.label, this.id});
}
