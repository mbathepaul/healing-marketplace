import 'package:dartz/dartz.dart';
import 'package:healing/core/models/doctor.dart';
import 'package:healing/core/providers/base_provider.dart';
import 'package:healing/services/therapist_service.dart';

class TherapistProvider extends BaseProvider {
  Either<Exception, List<Doctor>> therapistList = Right([]);

  /*getTherapistList({Map<String, dynamic> filters}) async {
    this.toggleLoadingState();
    TherapistService.getTherapist(
            filters: this.getFiltersStringFromMap(filters))
        .then((therapist) {
      therapistList = Right(therapist);
      this.toggleLoadingState();
    }).catchError((error) {
      therapistList = Left(error);
      this.toggleLoadingState();
    });
  }*/

  getFiltersStringFromMap(Map<String, dynamic> filters) {
    String filtersString = "";

    if (filters != null) {
      filters.forEach((key, value) {
        filtersString += "$key=$value&";
      });
    }
    print(filtersString);
    return filtersString;
  }
}
