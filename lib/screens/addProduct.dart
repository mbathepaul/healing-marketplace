import 'package:flutter/material.dart';
import 'package:healing/core/models/category_therapist.dart';
import 'package:healing/core/models/product.dart';
import 'package:healing/core/models/product_category.dart';
import 'package:healing/screens/addProduct_Order.dart';
import 'package:healing/services/authService.dart';
import 'package:healing/services/therapist_service.dart';
import 'package:healing/core/models/article.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:intl/intl.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

/*
import 'package:flutter/material.dart';
import 'package:healing/services/therapist_service.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:healing/core/models/article.dart';

class AddProduct extends StatefulWidget {
  @override
  _AddProductState createState() => _AddProductState();
}

class _AddProductState extends State<AddProduct> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            color: Colors.white,
            child: Flex(
              direction: Axis.vertical,
              children: [
                Expanded(
                  flex: 1,
                  child: Container(
                    height: 210.0,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('assets/images/doctors/1.jpg'),
                        fit: BoxFit.fill,
                      ),
                    ),
                    child: Stack(
                      children: [
                        Positioned(
                          child: RaisedButton(
                            onPressed: () {
                              Navigator.pushNamed(context, '/product_add');
                            },
                            color: Colors.white,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            child: Container(
                              width: 120.0,
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.add,
                                    color: Color(0xFF7912DD),
                                  ),
                                  Text(
                                    'Add product',
                                    style: TextStyle(
                                      color: Color(0xFF7912DD),
                                      fontFamily: 'Coves Bold',
                                      fontSize: 16.0,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          top: 130.0,
                          left: 130.0,
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 4,
                  child: Container(
                    padding: EdgeInsets.only(top: 20.0, left: 10.0, bottom: 12),
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(),
                    ),
                    child: CharacterListView(),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
*/
class ProductsCategorie extends StatelessWidget {
  const ProductsCategorie(
      {Key key, this.title, this.image, this.price, this.products})
      : super(key: key);

  final String title;
  final AssetImage image;
  final double price;
  final List<Product> products;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.only(left: 10.0, right: 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title ?? 'Yoga Equipements',
            style: TextStyle(
              fontFamily: 'Source Pro Bold',
              color: Color(0xFF7912DD),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10.0),
            height: 160.0,
            width: MediaQuery.of(context).size.width,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: products.length,
              itemBuilder: (context, index) {
                return Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 5.0, right: 5.0),
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(20.0),
                          child: FadeInImage(
                            width: 130,
                            height: 125,
                            fit: BoxFit.fill,
                            placeholder:
                                AssetImage('assets/images/doctors/default.jpg'),
                            image: products[index].image != null
                                ? NetworkImage(products[index].image)
                                : AssetImage(
                                    'assets/images/doctors/default.jpg'),
                          )),
                    ),
                    SizedBox(height: 5.0),
                    Text(
                      products[index].price.toStringAsFixed(2) + '\$' ??
                          '20.00\$',
                      style: TextStyle(
                        fontFamily: 'Source Pro Bold',
                        color: Color(0xFF797979),
                        fontSize: 18.0,
                      ),
                    ),
                  ],
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

class AddProduct extends StatefulWidget {
  @override
  _AddProductState createState() => _AddProductState();
}

class _AddProductState extends State<AddProduct> {
  List<Article> articles = [];
  bool hasdata = false;
  @override
  void initState() {
    // TODO: implement initState

    super.initState();

    TherapistService().getcathegories_shop().then((value) {
      TherapistService.category_products = value;
      for (ProductCategory cate in TherapistService.category_products) {
        CategoryTherapist category =
            CategoryTherapist(id: cate.id, label: cate.label);
        TherapistService.category_products_id.add(category);
        TherapistService.category_products_label.add(cate.label);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: Color(0xFFECD2FF),
            child: Flex(
              direction: Axis.vertical,
              children: [
                Expanded(
                  flex: 1,
                  child: Container(
                    height: 210.0,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('assets/images/doctors/doc.jpg'),
                        fit: BoxFit.fill,
                      ),
                    ),
                    child: Stack(
                      children: [
                        Positioned(
                            top: 10,
                            left: 20,
                            child: GestureDetector(
                              onTap: () {
                                print("comment faire la guerre");
                                Navigator.pop(context);
                              },
                              child: Icon(
                                Icons.arrow_back,
                                color: Colors.white.withOpacity(0.8),
                                size: 35,
                              ),
                            )),
                        Positioned(
                          child: RaisedButton(
                            onPressed: () {
                              Navigator.pushNamed(context, '/product_add');
                            },
                            color: Colors.white,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            child: Container(
                              width: 120.0,
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.add,
                                    color: Color(0xFF7912DD),
                                  ),
                                  Text(
                                    'Add product',
                                    style: TextStyle(
                                      color: Color(0xFF7912DD),
                                      fontFamily: 'Coves Bold',
                                      fontSize: 16.0,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          top: 130.0,
                          left: 130.0,
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Container(
                      padding:
                          EdgeInsets.only(top: 20.0, left: 10.0, bottom: 12),
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(),
                      ),
                      child: CharacterListView()),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

/*
import 'package:flutter/material.dart';
import 'package:healing/core/models/article.dart';
import 'package:healing/services/therapist_service.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import 'blog.dart';

class AddProduct extends StatefulWidget {
  @override
  _AddProductState createState() => _AddProductState();
}

class _AddProductState extends State<AddProduct> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    TherapistService().getCategoryProduct();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: Color(0xFFECD2FF),
            child: Flex(
              direction: Axis.vertical,
              children: [
                Expanded(
                  flex: 6,
                  child: Container(
                    alignment: Alignment.topLeft,
                    padding: EdgeInsets.only(
                        left: 2.0, top: 5.0, bottom: 20, right: 2.0),
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Color(0xFFFFFFFF),
                          Color(0xFFECD2FF),
                        ],
                      ),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Container(
                            margin: EdgeInsets.only(),
                            padding: EdgeInsets.all(10.0),
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.grey.shade300,
                                width: 1.0,
                                style: BorderStyle.solid,
                              ),
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            child: Icon(
                              Icons.arrow_back_ios,
                              color: Colors.grey,
                              size: 15,
                            ),
                          ),
                        ),
                        Container(
                          height: 214.0,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image:
                                  AssetImage('assets/images/doctors/doc.jpg'),
                              fit: BoxFit.fill,
                            ),
                          ),
                          child: Stack(
                            children: [
                              Positioned(
                                child: RaisedButton(
                                  onPressed: () {
                                    Navigator.pushNamed(context, '/blog');
                                  },
                                  color: Colors.white,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20.0),
                                  ),
                                  child: Container(
                                    width: 120.0,
                                    child: Row(
                                      children: [
                                        Icon(
                                          Icons.add,
                                          color: Color(0xFF7912DD),
                                        ),
                                        Text(
                                          'Add product',
                                          style: TextStyle(
                                            color: Color(0xFF7912DD),
                                            fontFamily: 'Coves Bold',
                                            fontSize: 16.0,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                top: 110.0,
                                left: 130.0,
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 9,
                  child: Container(
                    padding: EdgeInsets.only(top: 20.0, left: 10.0, bottom: 12),
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(),
                    ),
                    child: ListView.builder(
                      itemCount: 20,
                      itemBuilder: (context, index) {
                        return ProductsCategorie();
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ProductsCategorie extends StatelessWidget {
  const ProductsCategorie({
    Key key,
    this.title,
    this.image,
    this.price,
  }) : super(key: key);

  final String title;
  final AssetImage image;
  final double price;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.only(left: 10.0, right: 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title ?? 'Yoga Equipements',
            style: TextStyle(
              fontFamily: 'Source Pro Bold',
              color: Color(0xFF7912DD),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10.0),
            height: 160.0,
            width: MediaQuery.of(context).size.width,
            child: Expanded(
              child: ListView.builder(
                itemCount: 20,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 5.0, right: 5.0),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(20.0),
                          child: image ??
                              Image.asset(
                                'assets/images/doctors/4.png',
                                height: 120.0,
                                width: 120.0,
                                fit: BoxFit.fill,
                              ),
                        ),
                      ),
                      SizedBox(height: 5.0),
                      Text(
                        '20.00\$',
                        style: TextStyle(
                          fontFamily: 'Source Pro Bold',
                          color: Color(0xFF797979),
                          fontSize: 18.0,
                        ),
                      ),
                    ],
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}

*/

class CharacterListView extends StatefulWidget {
  @override
  _CharacterListViewState createState() => _CharacterListViewState();
}

class _CharacterListViewState extends State<CharacterListView> {
  static const _pageSize = 3;
  List<ProductCategory> product_caterogy = [];
  String uri = "https://healing-market.herokuapp.com/blog-api/articles/?page=1";
  final PagingController<int, ProductCategory> _pagingController =
      PagingController(firstPageKey: 0);

  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });
    super.initState();
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      int i = 0;
      List<ProductCategory> product_caterogye =
          await TherapistService().getTherapistProducts(AuthService.id);
      for (ProductCategory cate in product_caterogye) {
        if (cate.products.length > 0) {
          product_caterogy.add(cate);
        }
        i++;
        print(cate.products.length + i);
      }
      final newItems = product_caterogy;

      final isLastPage = true;
      if (isLastPage) {
        _pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = pageKey + newItems.length;
        _pagingController.appendPage(newItems, nextPageKey);
      }
    } catch (error) {
      _pagingController.error = error;
    }
  }

  @override
  Widget build(BuildContext context) => PagedListView<int, ProductCategory>(
        pagingController: _pagingController,
        builderDelegate: PagedChildBuilderDelegate<ProductCategory>(
          itemBuilder: (context, item, index) => ProductsCategorie(
            title: item.label,
            products: item.products,
          ),
          newPageErrorIndicatorBuilder: (_) => Container(
            margin: EdgeInsets.only(bottom: 20, top: 10),
            child: Column(
              children: [
                Text("Sommething went wrong"),
                SizedBox(
                  height: 8,
                ),
                GestureDetector(
                  onTap: () {
                    print("refrech");
                    _pagingController.retryLastFailedRequest();
                  },
                  child: Icon(
                    Icons.refresh,
                    size: 40,
                    color: Color(0xFF7806D2),
                  ),
                )
              ],
            ),
          ),
        ),
      );

  @override
  void dispose() {
    _pagingController.dispose();
    super.dispose();
  }
}
/*
class CategoryListView extends StatefulWidget {
  @override
  _CategoryListViewState createState() => _CategoryListViewState();
}

class _CategoryListViewState extends State<CategoryListView> {
  static const _pageSize = 3;
  String uri = "https://healing-market.herokuapp.com/blog-api/articles/?page=1";
  final PagingController<int, Article> _pagingController =
      PagingController(firstPageKey: 0);

  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });
    super.initState();
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      final newItems = await TherapistService().getArticles(uri);
      uri = newItems.hasnext as String;
      final isLastPage = newItems.hasnext == null ? true : false;
      if (isLastPage) {
        _pagingController.appendLastPage(newItems.articles);
      } else {
        final nextPageKey = pageKey + newItems.articles.length;
        _pagingController.appendPage(newItems.articles, nextPageKey);
      }
    } catch (error) {
      _pagingController.error = error;
    }
  }

  String image;
  @override
  Widget build(BuildContext context) => PagedListView<int, Article>(
        pagingController: _pagingController,
        scrollDirection: Axis.horizontal,
        builderDelegate: PagedChildBuilderDelegate<Article>(
          itemBuilder: (context, item, index) => Column(
            children: [
              Container(
                margin: EdgeInsets.only(left: 5.0, right: 5.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(20.0),
                  child: image ??
                      Image.asset(
                        'assets/images/doctors/4.png',
                        height: 120.0,
                        width: 120.0,
                        fit: BoxFit.fill,
                      ),
                ),
              ),
              SizedBox(height: 5.0),
              Text(
                '20.00\$',
                style: TextStyle(
                  fontFamily: 'Source Pro Bold',
                  color: Color(0xFF797979),
                  fontSize: 18.0,
                ),
              ),
            ],
          ),
          newPageErrorIndicatorBuilder: (_) => Container(
            margin: EdgeInsets.only(bottom: 20, top: 10),
            child: Column(
              children: [
                Text("Sommething went wrong"),
                SizedBox(
                  height: 8,
                ),
                GestureDetector(
                  onTap: () {
                    print("refrech");
                    _pagingController.retryLastFailedRequest();
                  },
                  child: Icon(
                    Icons.refresh,
                    size: 40,
                    color: Color(0xFF7806D2),
                  ),
                )
              ],
            ),
          ),
        ),
      );

  @override
  void dispose() {
    _pagingController.dispose();
    super.dispose();
  }
}

*/
