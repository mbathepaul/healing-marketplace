import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:healing/core/models/category_therapist.dart';
import 'package:healing/core/models/product.dart';
import 'package:healing/core/models/product_category.dart';
import 'package:healing/screens/addProduct_Order.dart';
import 'package:healing/services/authService.dart';
import 'package:healing/services/therapist_service.dart';
import 'package:healing/core/models/article.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:intl/intl.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class AddProductOrder extends StatefulWidget {
  @override
  _AddProductState createState() => _AddProductState();
}

class _AddProductState extends State<AddProductOrder> {
  List<Article> articles = [];
  bool hasdata = false;
  int product_add = 0;
  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }

  Function onProduct_add(int quntity) {
    print("onennnnnnnnnnnnnnnnnnnnnnnnnnnnnooooooooooooooooooooooo");
    setState(() {
      product_add += quntity;
    });
  }

  @override
  Widget build(BuildContext context) {
    String id = (ModalRoute.of(context).settings.arguments as String);

    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: Color(0xFFECD2FF),
            child: Flex(
              direction: Axis.vertical,
              children: [
                Expanded(
                  flex: 3,
                  child: Container(
                    height: 210.0,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('assets/images/doctors/doc.jpg'),
                        fit: BoxFit.fill,
                      ),
                    ),
                    child: Stack(
                      children: [
                        Positioned(
                            top: 10,
                            left: 20,
                            child: GestureDetector(
                              onTap: () {
                                print("comment faire la guerre");
                                Navigator.pop(context);
                              },
                              child: Icon(
                                Icons.arrow_back,
                                color: Colors.white.withOpacity(0.8),
                                size: 35,
                              ),
                            )),
                        Positioned(
                          child: Icon(
                            Icons.shopping_cart_sharp,
                            color: Colors.white,
                            size: 40,
                          ),
                          top: 20.0,
                          right: 30,
                        ),
                        Positioned(
                          child: Container(
                              child: Container(
                            width: 16,
                            height: 16,
                            decoration: BoxDecoration(
                                color: Color(0xFF7912DD),
                                borderRadius: BorderRadius.circular(18),
                                border: Border.all(
                                    width: 2,
                                    color: Colors.black.withOpacity(0.05))),
                            child: Center(
                              child: Text(product_add.toString(),
                                  style: TextStyle(
                                      fontFamily: 'Lucida',
                                      fontSize: 9.0,
                                      color: Colors.white)),
                            ),
                          )),
                          top: 21.0,
                          right: 25,
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 7,
                  child: Container(
                      padding:
                          EdgeInsets.only(top: 20.0, left: 10.0, bottom: 12),
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(),
                      ),
                      child: CharacterListView(
                        id: id,
                        notifierParent: onProduct_add,
                      )),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class CharacterListView extends StatefulWidget {
  final Function notifierParent;
  CharacterListView({this.id, this.notifierParent});
  final String id;
  @override
  _CharacterListViewState createState() =>
      _CharacterListViewState(id: id, notifierParent: notifierParent);
}

class _CharacterListViewState extends State<CharacterListView> {
  final Function notifierParent;
  _CharacterListViewState({this.id, this.notifierParent});
  final String id;
  static const _pageSize = 3;
  List<ProductCategory> product_caterogy = [];
  String uri = "https://healing-market.herokuapp.com/blog-api/articles/?page=1";
  final PagingController<int, ProductCategory> _pagingController =
      PagingController(firstPageKey: 0);

  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });
    super.initState();
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      int i = 0;
      List<ProductCategory> product_caterogye =
          await TherapistService().getTherapistProducts(id);
      for (ProductCategory cate in product_caterogye) {
        if (cate.products.length > 0) {
          product_caterogy.add(cate);
        }
      }
      final newItems = product_caterogy;

      final isLastPage = true;
      if (isLastPage) {
        _pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = pageKey + newItems.length;
        _pagingController.appendPage(newItems, nextPageKey);
      }
    } catch (error) {
      _pagingController.error = error;
    }
  }

  @override
  Widget build(BuildContext context) => PagedListView<int, ProductCategory>(
        pagingController: _pagingController,
        builderDelegate: PagedChildBuilderDelegate<ProductCategory>(
          itemBuilder: (context, item, index) => ProductsCategorie(
            title: item.label,
            products: item.products,
            notifierParent: notifierParent,
          ),
          newPageErrorIndicatorBuilder: (_) => Container(
            margin: EdgeInsets.only(bottom: 20, top: 10),
            child: Column(
              children: [
                Text("Sommething went wrong"),
                SizedBox(
                  height: 8,
                ),
                GestureDetector(
                  onTap: () {
                    print("refrech");
                    _pagingController.retryLastFailedRequest();
                  },
                  child: Icon(
                    Icons.refresh,
                    size: 40,
                    color: Color(0xFF7806D2),
                  ),
                )
              ],
            ),
          ),
        ),
      );

  @override
  void dispose() {
    _pagingController.dispose();
    super.dispose();
  }
}

class ProductsCategorie extends StatelessWidget {
  final Function notifierParent;
  const ProductsCategorie(
      {Key key,
      this.title,
      this.image,
      this.price,
      this.products,
      this.notifierParent})
      : super(key: key);

  final String title;
  final AssetImage image;
  final double price;
  final List<Product> products;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.only(left: 2.0, right: 2.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title ?? 'Yoga Equipements',
            style: TextStyle(
              fontFamily: 'Source Pro Bold',
              color: Color(0xFF7912DD),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10.0, bottom: 10),
            height: 188.0,
            width: MediaQuery.of(context).size.width,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: products.length,
              itemBuilder: (context, index) {
                return ProductItem(
                    image: products[index].image,
                    price: products[index].price,
                    label: products[index].label,
                    notifierParent: notifierParent,
                    id: products[index].id);
              },
            ),
          ),
        ],
      ),
    );
  }
}

class ProductItem extends StatefulWidget {
  final double price;
  final String label;
  final String image;
  final String id;
  final Function notifierParent;
  ProductItem(
      {this.price, this.image, this.label, this.id, this.notifierParent});
  @override
  _ProductItemState createState() => _ProductItemState(
      price: price,
      label: label,
      image: image,
      notifierParent: this.notifierParent);
}

class _ProductItemState extends State<ProductItem> {
  final double price;
  final String label;
  final String image;
  final String id;
  final Function notifierParent;
  _ProductItemState(
      {this.price, this.image, this.label, this.id, this.notifierParent});
  List<Article> articles = [];
  bool hasdata = false;
  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }

  bool is_order = false;
  bool is_chargin = false;
  TextEditingController quantity = TextEditingController();

  @override
  Widget build(BuildContext context) {
    String id = (ModalRoute.of(context).settings.arguments as String);
    print("idddddddddddddddddddddiiiiiiiiiiiiiiiiiiiiiiiiii");
    print(id);
    return Container(
      margin: EdgeInsets.only(left: 6.0, right: 6.0, bottom: 5),
      width: 170,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5.0),
        border: Border.all(color: Colors.white, width: 1),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.09),
            spreadRadius: 0.0,
            blurRadius: 10.0,
            offset: Offset(0, 8.0),
          ),
        ],
      ),
      child: Column(
        children: [
          Container(
            child: ClipRRect(
                borderRadius: BorderRadius.circular(10.0),
                child: FadeInImage(
                  width: 170,
                  height: 130,
                  fit: BoxFit.fill,
                  placeholder: AssetImage('assets/images/doctors/default.jpg'),
                  image: image != null
                      ? NetworkImage(image)
                      : AssetImage('assets/images/doctors/default.jpg'),
                )),
          ),
          if (is_chargin == true) SizedBox(height: 10.0),
          if (is_chargin == true)
            CircularProgressIndicator(
              backgroundColor: Color(0xFF797979),
            ),
          if (is_order == true && is_chargin == false) SizedBox(height: 5.0),
          if (is_order == true && is_chargin == false)
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                  width: 5,
                ),
                CustomField(
                  controller: quantity,
                  label: 'Quantity',
                  helper: '',
                  actionText: '',
                  type: TextInputType.number,
                ),
                SizedBox(
                  width: 8,
                ),
                GestureDetector(
                  onTap: () {
                    print("texte");

                    if (quantity.text != "") {
                      setState(() {
                        is_chargin = true;
                      });
                      AuthService().oderlinee1(id, quantity.text).then((value) {
                        setState(() {
                          is_chargin = false;
                          is_order = false;
                        });
                        print(value);
                        if (value) {
                          notifierParent(int.parse(quantity.text));
                          Fluttertoast.showToast(
                            msg:
                                'your order was successful you will be contacted by the service provider!',
                            toastLength: Toast.LENGTH_LONG,
                            gravity: ToastGravity.BOTTOM,
                            backgroundColor: Colors.green,
                            textColor: Colors.white,
                            fontSize: 12.0,
                          );
                        } else {
                          Fluttertoast.showToast(
                            msg:
                                'Order failed, please try again, or contact support for help',
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.BOTTOM,
                            backgroundColor: Colors.red,
                            textColor: Colors.white,
                            fontSize: 13.0,
                          );
                        }
                      });
                    } else {
                      Fluttertoast.showToast(
                        msg: "Quantity can't be empty ",
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.BOTTOM,
                        backgroundColor: Colors.red,
                        textColor: Colors.white,
                        fontSize: 13.0,
                      );
                    }
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: 36.0,
                    width: "order now".length.toDouble() * 9,
                    decoration: BoxDecoration(
                      color: Color(0xFF320A3B),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: Text(
                      'Order',
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'Coves Bold',
                        fontSize: 17.0,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 5,
                ),
              ],
            ),
          if (is_order == false && is_chargin == false)
            Text(
              label,
              style: TextStyle(
                fontFamily: 'Source Pro Bold',
                color: Color(0xFF797979).withOpacity(0.8),
                fontSize: 14.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          if (is_order == false && is_chargin == false) SizedBox(height: 5.0),
          if (is_order == false && is_chargin == false)
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  price.toStringAsFixed(2) + '\$' ?? '20.00\$',
                  style: TextStyle(
                    fontFamily: 'Source Pro Bold',
                    color: Color(0xFF797979),
                    fontSize: 16.0,
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                GestureDetector(
                  onTap: () {
                    print("texte");
                    setState(() {
                      is_order = true;
                    });
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: 26.0,
                    width: "order now".length.toDouble() * 9,
                    decoration: BoxDecoration(
                      color: Color(0xFF320A3B),
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    child: Text(
                      'order now',
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'Coves Bold',
                        fontSize: 17.0,
                      ),
                    ),
                  ),
                )
              ],
            )
        ],
      ),
    );
  }
}

class CustomField extends StatelessWidget {
  const CustomField({
    Key key,
    @required this.label,
    @required this.controller,
    this.helper,
    this.bottomMargin = 10.0,
    this.type = TextInputType.text,
    this.isPassword = false,
    this.actionText,
    this.actionCallback,
    this.maxline,
    this.color,
  }) : super(key: key);

  final String label;
  final String helper;
  final String actionText;
  final double bottomMargin;
  final bool isPassword;
  final TextInputType type;
  final TextEditingController controller;
  final Function actionCallback;
  final int maxline;
  final bool color;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 0.0),
      height: 36,
      width: 56,
      margin: EdgeInsets.only(bottom: 0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5.0),
        border: Border.all(
            color: color == null ? Color(0xFF797979) : Color(0xFF7912DD),
            width: 1.5),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.2),
            spreadRadius: 0.0,
            blurRadius: 10.0,
            offset: Offset(0, 8.0),
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                label,
                style: TextStyle(
                  fontFamily: 'Source SemiBold',
                  fontSize: 8.0,
                  color: Color(0xFF474E6C),
                ),
              ),
              if (actionText != null)
                GestureDetector(
                  onTap: actionCallback ?? () {},
                  child: Text(
                    actionText,
                    style: TextStyle(
                      fontFamily: 'Source SemiBold',
                      fontSize: 8.0,
                      color: Color(0xFF474E6C).withOpacity(0.3),
                    ),
                  ),
                ),
            ],
          ),
          TextField(
            controller: controller,
            keyboardType: type,
            obscureText: isPassword,
            style: TextStyle(
              fontFamily: 'Source SemiBold',
              fontSize: 12.0,
              color: Color(0xFF474E6C).withOpacity(0.8),
            ),
            decoration: InputDecoration(
              isDense: true,
              floatingLabelBehavior: FloatingLabelBehavior.never,
              contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 5.0),
              labelText: helper,
              border: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent),
              ),
              labelStyle: TextStyle(
                fontFamily: 'Source SemiBold',
                fontSize: 12.0,
                color: Color(0xFF474E6C).withOpacity(0.5),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
