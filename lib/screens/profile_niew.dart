import 'dart:io';

import 'package:flutter/material.dart';
import 'package:healing/services/authService.dart';
import 'package:healing/services/therapist_service.dart';
import 'package:image_picker/image_picker.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class ProfileUI extends StatefulWidget {
  @override
  Profile createState() => Profile();
}

class Profile extends State<ProfileUI> {
  File _image;
  int group = 1;
  List<File> images = [];
  int _contimage = 0;
  bool _isLoading = false;
  bool _isadd = false;

  final picker = ImagePicker();
  changed() {
    print("bonjour");
  }

  String nbr_comments = AuthService.doctor.nb_reviews.toString();
  FlutterSecureStorage _localStorage = new FlutterSecureStorage();

  Future getImagelogo() async {
    PickedFile pickedFile = await picker.getImage(source: ImageSource.gallery);
    File image = File(pickedFile.path);
    setState(() {
      _isLoading = true;
    });
    await AuthService().uploadfile(image).then(
      (value) {
        if (value) {
          setState(() {
            _isLoading = false;
          });
          setState(
            () {
              if (pickedFile != null) {
                _image = image;
                AuthService.image = _image;
              } else {
                print('no image selected');
              }
            },
          );
          Fluttertoast.showToast(
            msg: 'Image successfully added!',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            backgroundColor: Colors.green,
            textColor: Colors.white,
            fontSize: 13.0,
          );
        } else {
          setState(() {
            _isLoading = false;
          });
          Fluttertoast.showToast(
            msg: 'An error occured during downloading',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 13.0,
          );
        }
      },
    );
    setState(() {
      _isLoading = false;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    TherapistService().getTherapistById().then((value) {
      setState(() {
        nbr_comments = value.length.toString();
      });
      _localStorage.write(key: "nb_reviews", value: value.length.toString());
    }).catchError((onError) {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white.withOpacity(0.96),
        body: LoadingOverlay(
          child: Column(
            children: [
              Stack(
                overflow: Overflow.visible,
                alignment: Alignment.center,
                children: [
                  Image(
                    height: MediaQuery.of(context).size.height / 3,
                    width: MediaQuery.of(context).size.width,
                    fit: BoxFit.cover,
                    image: AssetImage('assets/images/doctors/profile.jpg'),
                  ),
                  Positioned(
                      top: 35,
                      left: 20,
                      child: GestureDetector(
                        onTap: () {
                          print("comment faire la guerre");
                          Navigator.pop(context);
                        },
                        child: Icon(
                          Icons.arrow_back,
                          color: Colors.white.withOpacity(0.8),
                          size: 40,
                        ),
                      )),
                  Positioned(
                      bottom: -60,
                      child: Container(
                        alignment: Alignment.center,
                        height: 123.0,
                        width: 123,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(80.0),
                        ),
                        child: CircleAvatar(
                          radius: 45,
                          backgroundColor: Colors.white,
                          backgroundImage: AuthService.image != null
                              ? FileImage(AuthService.image)
                              : NetworkImage(AuthService.doctor.photos[0]),
                        ),
                      )),
                  Positioned(
                      bottom: -58,
                      left: MediaQuery.of(context).size.width / 2 + 35,
                      child: GestureDetector(
                          onTap: () {
                            changed();
                          },
                          child: Container(
                            alignment: Alignment.center,
                            height: 31.0,
                            width: 31,
                            decoration: BoxDecoration(
                              color: Color(0xFFB247BC),
                              borderRadius: BorderRadius.circular(15),
                            ),
                            child: Icon(
                              Icons.edit_outlined,
                              color: Colors.white,
                              size: 20,
                            ),
                          )))
                ],
              ),
              GestureDetector(
                onTap: () async {
                  getImagelogo();
                },
                onTapDown: (TapDownDetails details) {
                  if ((MediaQuery.of(context).size.width / 2 + 20) <
                          details.globalPosition.dx &&
                      (MediaQuery.of(context).size.width / 2 + 80) >
                          details.globalPosition.dx) {
                    if (details.globalPosition.dy <
                            (MediaQuery.of(context).size.height / 3 + 75) &&
                        details.globalPosition.dy >
                            (MediaQuery.of(context).size.height / 3 + 35)) {
                      print("on marche...................");
                    }
                  }
                },
                child: Container(
                  color: Colors.white.withOpacity(0.04),
                  height: 70,
                ),
              ),
              Center(
                child: Text(
                  AuthService.doctor.user.toString(),
                  style: TextStyle(
                      fontFamily: 'Source SemiBold',
                      fontSize: 18.0,
                      color: Color(0xFF474E6C).withOpacity(0.7),
                      fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(
                height: 5.0,
              ),
              Center(
                child: Text(
                  AuthService.doctor.label.toString(),
                  style: TextStyle(
                    fontFamily: 'Lucida',
                    fontSize: 11.0,
                    color: Color(0xFF474E6C).withOpacity(0.5),
                  ),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Expanded(
                  child: SingleChildScrollView(
                      child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Stack(
                          overflow: Overflow.visible,
                          alignment: Alignment.center,
                          children: [
                            Container(
                                height: 28,
                                width: 120,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  color: Color(0xFFB247BC),
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                child: Text(
                                  "expiration date",
                                  style: TextStyle(
                                      fontFamily: 'Lucida',
                                      fontSize: 11.0,
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                )),
                          ]),
                      Container(
                          alignment: Alignment.center,
                          height: 28,
                          width: 120,
                          decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.1),
                              borderRadius: BorderRadius.circular(12),
                              border: Border.all(
                                  width: 2,
                                  color: Colors.black.withOpacity(0.05))),
                          child: Text(
                            AuthService.doctor.end_subscription.toString(),
                            style: TextStyle(
                              fontFamily: 'Lucida',
                              fontSize: 11.0,
                              color: Color(0xFFB247BC),
                            ),
                          ))
                    ],
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      GestureDetector(
                        onTap: () {
                          print("commentaires");
                          Navigator.pushNamed(context, '/comment_therapist');
                        },
                        child: Stack(
                          overflow: Overflow.visible,
                          alignment: Alignment.center,
                          children: [
                            Container(
                                height: 100,
                                width: 120,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    color: Colors.white.withOpacity(0.1),
                                    borderRadius: BorderRadius.circular(12),
                                    border: Border.all(
                                        width: 2,
                                        color: Colors.black.withOpacity(0.05))),
                                child: Center(
                                    child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(Icons.comment_rounded,
                                        size: 35, color: Color(0xFFB247BC)),
                                    SizedBox(
                                      height: 5.0,
                                    ),
                                    Text(
                                      "reviews",
                                      style: TextStyle(
                                        fontFamily: 'Lucida',
                                        fontSize: 15.0,
                                        color: Color(0xFFB247BC),
                                      ),
                                    )
                                  ],
                                ))),
                            Positioned(
                                left: 100,
                                top: -8,
                                child: Container(
                                  width: 28,
                                  height: 28,
                                  decoration: BoxDecoration(
                                      color: Colors.red,
                                      borderRadius: BorderRadius.circular(18),
                                      border: Border.all(
                                          width: 2,
                                          color:
                                              Colors.black.withOpacity(0.05))),
                                  child: Center(
                                    child: Text(nbr_comments,
                                        style: TextStyle(
                                            fontFamily: 'Lucida',
                                            fontSize: 9.0,
                                            color: Colors.white)),
                                  ),
                                ))
                          ],
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          print("STORE");
                          Navigator.pushNamed(context, '/add_product');
                        },
                        child: Container(
                            height: 100,
                            width: 120,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                color: Colors.white.withOpacity(0.1),
                                borderRadius: BorderRadius.circular(12),
                                border: Border.all(
                                    width: 2,
                                    color: Colors.black.withOpacity(0.05))),
                            child: Center(
                                child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(Icons.storefront,
                                    size: 35, color: Color(0xFFB247BC)),
                                SizedBox(
                                  height: 5.0,
                                ),
                                Text(
                                  "Store",
                                  style: TextStyle(
                                    fontFamily: 'Lucida',
                                    fontSize: 15.0,
                                    color: Color(0xFFB247BC),
                                  ),
                                )
                              ],
                            ))),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      GestureDetector(
                        onTap: () {
                          print("subcription");
                          Navigator.pushNamed(context, '/payement');
                        },
                        child: Container(
                            height: 100,
                            width: 120,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                color: Colors.white.withOpacity(0.1),
                                borderRadius: BorderRadius.circular(12),
                                border: Border.all(
                                    width: 2,
                                    color: Colors.black.withOpacity(0.05))),
                            child: Center(
                                child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(Icons.loop,
                                    size: 35, color: Color(0xFFB247BC)),
                                SizedBox(
                                  height: 5.0,
                                ),
                                Text(
                                  "Subcription",
                                  style: TextStyle(
                                    fontFamily: 'Lucida',
                                    fontSize: 15.0,
                                    color: Color(0xFFB247BC),
                                  ),
                                )
                              ],
                            ))),
                      ),
                      GestureDetector(
                        onTap: () {
                          print("update");
                          Navigator.pushNamed(context, '/profile');
                        },
                        child: Container(
                            height: 100,
                            width: 120,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                color: Colors.white.withOpacity(0.1),
                                borderRadius: BorderRadius.circular(12),
                                border: Border.all(
                                    width: 2,
                                    color: Colors.black.withOpacity(0.05))),
                            child: Center(
                                child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(Icons.account_circle_outlined,
                                    size: 35, color: Color(0xFFB247BC)),
                                SizedBox(
                                  height: 5.0,
                                ),
                                Text(
                                  "Update profile",
                                  style: TextStyle(
                                    fontFamily: 'Lucida',
                                    fontSize: 15.0,
                                    color: Color(0xFFB247BC),
                                  ),
                                )
                              ],
                            ))),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      GestureDetector(
                        onTap: () {
                          print("subcription");
                          // Navigator.pushNamed(context, '/payement');
                          Navigator.pushNamed(context, '/booking');
                        },
                        child: Container(
                            height: 100,
                            width: 120,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                color: Colors.white.withOpacity(0.1),
                                borderRadius: BorderRadius.circular(12),
                                border: Border.all(
                                    width: 2,
                                    color: Colors.black.withOpacity(0.05))),
                            child: Center(
                                child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(Icons.date_range,
                                    size: 35, color: Color(0xFFB247BC)),
                                SizedBox(
                                  height: 5.0,
                                ),
                                Text(
                                  "Booking",
                                  style: TextStyle(
                                    fontFamily: 'Lucida',
                                    fontSize: 15.0,
                                    color: Color(0xFFB247BC),
                                  ),
                                )
                              ],
                            ))),
                      ),
                      GestureDetector(
                        onTap: () {},
                        child: Container(
                            height: 100,
                            width: 120,
                            alignment: Alignment.center,
                            child: Center(
                                child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SizedBox(
                                  height: 5.0,
                                ),
                              ],
                            ))),
                      )
                    ],
                  )
                ],
              )))
            ],
          ),
          isLoading: _isLoading,
          color: Colors.white,
          opacity: 0.5,
          progressIndicator: CircularProgressIndicator(
            backgroundColor: Color(0xFF9C27B0),
          ),
        ));
  }
}
