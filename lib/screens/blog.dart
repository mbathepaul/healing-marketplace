import 'package:flutter/material.dart';
import 'package:healing/services/therapist_service.dart';
import 'package:healing/core/models/article.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:intl/intl.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class BlogPage extends StatefulWidget {
  @override
  _BlogPageState createState() => _BlogPageState();
}

class _BlogPageState extends State<BlogPage> {
  List<Article> articles = [];
  bool hasdata = false;
  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: Color(0xFFECD2FF),
            child: Flex(
              direction: Axis.vertical,
              children: [
                Expanded(
                  flex: 1,
                  child: Container(
                    alignment: Alignment.topLeft,
                    padding: EdgeInsets.only(left: 20.0, top: 0.0, bottom: 20),
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Color(0xFFFFFFFF),
                          Color(0xFFECD2FF),
                        ],
                      ),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Container(
                            padding: EdgeInsets.all(10.0),
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.grey.shade300,
                                width: 1.0,
                                style: BorderStyle.solid,
                              ),
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            child: Icon(
                              Icons.arrow_back_ios,
                              color: Colors.grey,
                              size: 15,
                            ),
                          ),
                        ),
                        Text(
                          'Healing Market',
                          style: TextStyle(
                            fontFamily: 'Source Pro Bold',
                            fontSize: 25.0,
                            color: Color(0xFF7806D2),
                          ),
                        ),
                        Text(
                          'Little Notes',
                          style: TextStyle(
                            fontFamily: 'Source Pro SemiBold',
                            fontSize: 22.0,
                            color: Color(0xFF7806D2),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                    margin: EdgeInsets.only(left: 0.0),
                    padding: EdgeInsets.only(top: 20.0, left: 10.0),
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(50.0),
                        topLeft: Radius.circular(50.0),
                      ),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'See all topics',
                          style: TextStyle(
                            fontFamily: 'Source Pro SemiBold',
                            fontSize: 18.0,
                            color: Color(0xFF7806D2),
                          ),
                        ),
                      ],
                    )),
                Expanded(
                  flex: 4,
                  child: Container(
                      padding:
                          EdgeInsets.only(top: 20.0, left: 10.0, bottom: 12),
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(),
                      ),
                      child: CharacterListView()),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class Articles extends StatelessWidget {
  const Articles({
    Key key,
    this.image,
    this.title,
    this.date,
    this.comment,
    this.description,
    this.article,
  }) : super(key: key);

  final String image;
  final String title;
  final String date;
  final String description;
  final String comment;
  final Article article;

  @override
  Widget build(BuildContext context) {
    String strDt = this.date;
    DateTime parseDt = DateTime.parse(strDt);
    print(parseDt); // 1974-03-20 00:00:00.000
    return GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, '/article_detail',
              arguments: this.article);
        },
        child: Container(
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                width: 1,
                color: Color(0xFFF1F1F1),
              ),
            ),
          ),
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 10.0),
                  Container(
                    margin: EdgeInsets.only(bottom: 10.0),
                    height: 145,
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(20.0),
                        child: FadeInImage(
                          width: 110.0,
                          height: 180,
                          fit: BoxFit.fitHeight,
                          alignment: FractionalOffset.topCenter,
                          placeholder:
                              AssetImage('assets/images/doctors/2.jpg'),
                          image: image != null
                              ? NetworkImage(image)
                              : AssetImage('assets/images/backImage.jpg'),
                        )),
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          title ?? 'Your Spring Fitness Plan',
                          style: TextStyle(
                            fontFamily: 'Source Pro SemiBold',
                            fontSize: 16.0,
                            color: Color(0xFF7806D2),
                          ),
                        ),
                        SizedBox(height: 10.0),
                        Text(
                          description ??
                              'Lorem ipsum dolor sit amet consectetur les homme les plus dangereux sont ceux qui se prennent pour les plus fort, adipisicing elit. Maxime Mollitia la guerre en algerie sans trops gapsiller les autres car personne assez fort.',
                          textAlign: TextAlign.left,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 4,
                          style: TextStyle(
                            fontFamily: 'Source Pro SemiBold',
                            fontSize: 12.0,
                            color: Color(0xFFC8C8C8),
                          ),
                        ),
                        SizedBox(height: 20.0),
                        Row(
                          children: [
                            SizedBox(width: 40.0),
                            Container(
                              child: Text(
                                timeago.format(parseDt) ?? '3 days ago',
                                style: TextStyle(
                                  fontFamily: 'Source Pro SemiBold',
                                  fontSize: 11.0,
                                  color: Color(0xFF7806D2),
                                ),
                              ),
                            ),
                            SizedBox(width: 40.0),
                            Row(
                              children: [
                                Icon(
                                  Icons.comment,
                                  size: 10.0,
                                  color: Color(0xFF7806D2),
                                ),
                                SizedBox(
                                  width: 5.0,
                                ),
                                Text(
                                  comment ?? '15 comments',
                                  style: TextStyle(
                                    fontFamily: 'Source Pro SemiBold',
                                    fontSize: 11.0,
                                    color: Color(0xFF7806D2),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        SizedBox(height: 10.0),
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        ));
  }
}

class CharacterListView extends StatefulWidget {
  @override
  _CharacterListViewState createState() => _CharacterListViewState();
}

class _CharacterListViewState extends State<CharacterListView> {
  static const _pageSize = 3;
  String uri = "https://healing-market.herokuapp.com/blog-api/articles/?page=1";
  final PagingController<int, Article> _pagingController =
      PagingController(firstPageKey: 0);

  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });
    super.initState();
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      final newItems = await TherapistService().getArticles(uri);
      print(newItems.hasnext);
      uri = newItems.hasnext as String;
      final isLastPage = newItems.hasnext == null ? true : false;
      if (isLastPage) {
        _pagingController.appendLastPage(newItems.articles);
      } else {
        final nextPageKey = pageKey + newItems.articles.length;
        _pagingController.appendPage(newItems.articles, nextPageKey);
      }
    } catch (error) {
      _pagingController.error = error;
    }
  }

  @override
  Widget build(BuildContext context) => PagedListView<int, Article>(
        pagingController: _pagingController,
        builderDelegate: PagedChildBuilderDelegate<Article>(
          itemBuilder: (context, item, index) => Articles(
            image: item.image,
            title: item.label,
            comment: item.nb_comments.toString(),
            description: item.content,
            date: item.createdAt,
            article: item,
          ),
          firstPageProgressIndicatorBuilder: (_) => Container(
              height: 20,
              width: 20,
              child: Center(
                child: CircularProgressIndicator(
                  backgroundColor: Color(0xFF9C27B0),
                ),
              )),
          newPageErrorIndicatorBuilder: (_) => Container(
            margin: EdgeInsets.only(bottom: 20, top: 10),
            child: Column(
              children: [
                Text("Sommething went wrong"),
                SizedBox(
                  height: 8,
                ),
                GestureDetector(
                  onTap: () {
                    print("refrech");
                    _pagingController.retryLastFailedRequest();
                  },
                  child: Icon(
                    Icons.refresh,
                    size: 40,
                    color: Color(0xFF7806D2),
                  ),
                )
              ],
            ),
          ),
        ),
      );

  @override
  void dispose() {
    _pagingController.dispose();
    super.dispose();
  }
}
