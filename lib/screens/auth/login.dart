import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:healing/screens/dashboard/container.dart';
import 'package:healing/services/authService.dart';
import 'package:healing/widgets/custom_button.dart';
import 'package:healing/widgets/custom_field.dart';
import 'package:healing/screens/auth/laoder.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:flutter/scheduler.dart';
import 'package:email_validator/email_validator.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

final name = TextEditingController();
final password = TextEditingController();
bool indication = false;
bool _isLoading = false;

class _LoginScreenState extends State<LoginScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    return SafeArea(
      child: Scaffold(
          body: LoadingOverlay(
        child: SingleChildScrollView(
          child: Container(
            height: screenSize.height,
            width: screenSize.width,
            padding: EdgeInsets.fromLTRB(20.0, 40.0, 20.0, 20.0),
            decoration: BoxDecoration(
              color: Colors.white,
              image: DecorationImage(
                image: AssetImage('assets/images/background.jpg'),
                fit: BoxFit.fill,
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Image.asset('assets/images/logo.jpg',
                    width: 32.0, height: 32.0),
                SizedBox(height: 50.0),
                Text(
                  'Log in',
                  style: TextStyle(
                    fontFamily: 'Coves Bold',
                    fontSize: 36.0,
                    color: Color(0xFF011424),
                  ),
                ),
                SizedBox(height: 10.0),
                SizedBox(
                  width: (screenSize.width / 2) + 200.0,
                  child: Text(
                    'Please enter your information below to start using App.',
                    style: TextStyle(
                      fontFamily: 'Source',
                      fontSize: 18.0,
                      color: Color(0xFF011424),
                    ),
                  ),
                ),
                SizedBox(height: 100.0),
                CustomField(
                  label: 'name',
                  helper: 'Enter your name',
                  type: TextInputType.text,
                  controller: name,
                  bottomMargin: 20.0,
                ),
                CustomField(
                  label: 'password',
                  helper: 'Enter your password',
                  type: TextInputType.text,
                  controller: password,
                  bottomMargin: 20.0,
                  isPassword: true,
                ),
                CustomButton(
                  text: 'Log in',
                  onPressed: () async {
                    if (name.text != null && password != null) {
                      setState(() {
                        _isLoading = true;
                      });
                      await AuthService()
                          .login(name.text, password.text)
                          .then((val) {
                        if (val == true) {
                          Fluttertoast.showToast(
                            msg:
                                'You\'re authenticate, you will been redirected !',
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.BOTTOM,
                            backgroundColor: Colors.green,
                            textColor: Colors.white,
                            fontSize: 12.0,
                          );
                          setState(() {
                            _isLoading = false;
                          });
                          Navigator.pushNamedAndRemoveUntil(context,
                              "/container", (Route<dynamic> route) => false);
                        } else {
                          setState(() {
                            _isLoading = false;
                          });
                          Fluttertoast.showToast(
                            msg: val.toString(),
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.BOTTOM,
                            backgroundColor: Color(0xFF011424),
                            textColor: Colors.white,
                            fontSize: 15.0,
                          );
                        }
                      });
                    } else {
                      Fluttertoast.showToast(
                        msg: 'All fields are required',
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.BOTTOM,
                        backgroundColor: Color(0xFF011424),
                        textColor: Colors.white,
                        fontSize: 15.0,
                      );
                    }
                  },
                ),
                SizedBox(height: 30.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Don't have an account?  ",
                      style: TextStyle(
                        color: Color(0xFF474E6C).withOpacity(0.8),
                        fontFamily: 'Source Pro',
                        fontSize: 12.0,
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(context, '/package');
                      },
                      child: Text(
                        ' Sign up.',
                        style: TextStyle(
                          color: Color(0xFF320A3B),
                          fontFamily: 'Source SemiBold',
                          fontSize: 13.0,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 14.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Forgot password? ",
                      style: TextStyle(
                        color: Color(0xFF474E6C).withOpacity(0.8),
                        fontFamily: 'Source Pro',
                        fontSize: 12.0,
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(context, '/resetpassword');
                      },
                      child: Text(
                        ' Recover password.',
                        style: TextStyle(
                          color: Color(0xFF320A3B),
                          fontFamily: 'Source SemiBold',
                          fontSize: 13.0,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        isLoading: _isLoading,
        color: Colors.white,
        opacity: 0.5,
        progressIndicator: CircularProgressIndicator(
          backgroundColor: Color(0xFF9C27B0),
        ),
      )),
    );
  }
}
