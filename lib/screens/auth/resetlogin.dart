import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:healing/screens/dashboard/container.dart';
import 'package:healing/services/authService.dart';
import 'package:healing/widgets/custom_button.dart';
import 'package:healing/widgets/custom_field.dart';
import 'package:healing/screens/auth/laoder.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:flutter/scheduler.dart';
import 'package:email_validator/email_validator.dart';

class ResetScreen extends StatefulWidget {
  @override
  _ResetScreenState createState() => _ResetScreenState();
}

final name = TextEditingController();
final password = TextEditingController();
bool indication = false;
bool _isLoading = false;

class _ResetScreenState extends State<ResetScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    return SafeArea(
      child: Scaffold(
          body: LoadingOverlay(
        child: SingleChildScrollView(
          child: Container(
            height: screenSize.height,
            width: screenSize.width,
            padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 20.0),
            decoration: BoxDecoration(
              color: Colors.white,
              image: DecorationImage(
                image: AssetImage('assets/images/background.jpg'),
                fit: BoxFit.fill,
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Icon(
                    Icons.arrow_back,
                    color: Colors.grey,
                    size: 30,
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Image.asset('assets/images/logo.jpg',
                    width: 32.0, height: 32.0),
                SizedBox(height: 50.0),
                Text(
                  'Reset password',
                  style: TextStyle(
                    fontFamily: 'Coves Bold',
                    fontSize: 36.0,
                    color: Color(0xFF011424),
                  ),
                ),
                SizedBox(height: 10.0),
                SizedBox(
                  width: (screenSize.width / 2) + 200.0,
                  child: Text(
                    'Enter your email below to reset your password',
                    style: TextStyle(
                      fontFamily: 'Source',
                      fontSize: 18.0,
                      color: Color(0xFF011424),
                    ),
                  ),
                ),
                SizedBox(height: 80.0),
                CustomField(
                  label: 'email',
                  helper: 'Enter your email',
                  type: TextInputType.emailAddress,
                  controller: name,
                  bottomMargin: 20.0,
                ),
                SizedBox(height: 20.0),
                CustomButton(
                  text: 'Reset',
                  onPressed: () async {
                    final bool isValid = EmailValidator.validate(name.text);
                    if (isValid) {
                      setState(() {
                        _isLoading = true;
                      });
                      await AuthService().reset(name.text).then((val) {
                        if (val == true) {
                          Fluttertoast.showToast(
                            msg: 'An email has been sent!',
                            toastLength: Toast.LENGTH_LONG,
                            gravity: ToastGravity.BOTTOM,
                            backgroundColor: Colors.green,
                            textColor: Colors.white,
                            fontSize: 12.0,
                          );
                          setState(() {
                            _isLoading = false;
                          });
                          Navigator.pop(context);
                        } else {
                          setState(() {
                            _isLoading = false;
                          });
                          Fluttertoast.showToast(
                            msg: val.toString(),
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.BOTTOM,
                            backgroundColor: Color(0xFF011424),
                            textColor: Colors.white,
                            fontSize: 15.0,
                          );
                        }
                      });
                    } else {
                      Fluttertoast.showToast(
                        msg: 'Invalid email address',
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.BOTTOM,
                        backgroundColor: Color(0xFF011424),
                        textColor: Colors.white,
                        fontSize: 15.0,
                      );
                    }
                  },
                ),
                SizedBox(height: 30.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "",
                      style: TextStyle(
                        color: Color(0xFF474E6C).withOpacity(0.8),
                        fontFamily: 'Source Pro',
                        fontSize: 12.0,
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        'Login',
                        style: TextStyle(
                          color: Color(0xFF320A3B),
                          fontFamily: 'Source SemiBold',
                          fontSize: 15.0,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        isLoading: _isLoading,
        color: Colors.white,
        opacity: 0.5,
        progressIndicator: CircularProgressIndicator(
          backgroundColor: Color(0xFF9C27B0),
        ),
      )),
    );
  }
}
