import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:healing/services/authService.dart';
import 'package:healing/widgets/custom_button.dart';
import 'package:healing/widgets/custom_field.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:email_validator/email_validator.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

final name = TextEditingController();
final email = TextEditingController();
final password = TextEditingController();
final confirmPass = TextEditingController();
bool typeclient = true;
bool typetherapist = false;
bool _isLoading = false;
Function onchange;
String actionText;
Function actionCallback;

class _RegisterScreenState extends State<RegisterScreen> {
  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    onchange = () => {print("bonjour")};
    return SafeArea(
      child: Scaffold(
        body: LoadingOverlay(
          child: SingleChildScrollView(
            child: Container(
              height: screenSize.height,
              width: screenSize.width,
              padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 20.0),
              decoration: BoxDecoration(
                color: Colors.white,
                image: DecorationImage(
                  image: AssetImage('assets/images/background.jpg'),
                  fit: BoxFit.fill,
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Icon(
                      Icons.arrow_back,
                      color: Colors.grey,
                      size: 30,
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Image.asset('assets/images/logo.jpg',
                      width: 32.0, height: 32.0),
                  SizedBox(height: 20.0),
                  Text(
                    'Sign up',
                    style: TextStyle(
                      fontFamily: 'Coves Bold',
                      fontSize: 36.0,
                      color: Color(0xFF011424),
                    ),
                  ),
                  SizedBox(height: 10.0),
                  SizedBox(
                    width: (screenSize.width / 2) + 200.0,
                    child: Text(
                      'Fill your informations to create your account.',
                      style: TextStyle(
                        fontFamily: 'Source',
                        fontSize: 16.0,
                        color: Color(0xFF011424),
                      ),
                    ),
                  ),
                  SizedBox(height: 40.0),
                  CustomField(
                    label: 'Name',
                    helper: 'Enter your name',
                    type: TextInputType.text,
                    controller: name,
                    bottomMargin: 20.0,
                  ),
                  Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
                    margin: EdgeInsets.only(bottom: 20.0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(5.0),
                      border: Border.all(color: Color(0xFF9C27B0), width: 1.5),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.2),
                          spreadRadius: 0.0,
                          blurRadius: 10.0,
                          offset: Offset(0, 8.0),
                        ),
                      ],
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              'Email',
                              style: TextStyle(
                                fontFamily: 'Source SemiBold',
                                fontSize: 13.0,
                                color: Color(0xFF474E6C),
                              ),
                            ),
                            if (actionText != null)
                              GestureDetector(
                                onTap: actionCallback ?? () {},
                                child: Text(
                                  actionText,
                                  style: TextStyle(
                                    fontFamily: 'Source SemiBold',
                                    fontSize: 11.0,
                                    color: Color(0xFF474E6C).withOpacity(0.3),
                                  ),
                                ),
                              ),
                          ],
                        ),
                        SizedBox(height: 3.0),
                        TextField(
                          controller: email,
                          keyboardType: TextInputType.emailAddress,
                          obscureText: false,
                          style: TextStyle(
                            fontFamily: 'Source SemiBold',
                            fontSize: 12.0,
                            color: Color(0xFF474E6C).withOpacity(0.8),
                          ),
                          decoration: InputDecoration(
                            isDense: true,
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 5.0),
                            labelText: "abcd@gmail.com",
                            border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.transparent),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.transparent),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.transparent),
                            ),
                            labelStyle: TextStyle(
                              fontFamily: 'Source SemiBold',
                              fontSize: 12.0,
                              color: Color(0xFF474E6C).withOpacity(0.5),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  CustomField(
                    label: 'Password',
                    helper: 'Enter your password',
                    controller: password,
                    type: TextInputType.visiblePassword,
                    bottomMargin: 20.0,
                    isPassword: true,
                  ),
                  CustomField(
                    label: 'Confirm password',
                    helper: 'Confirm your password',
                    type: TextInputType.visiblePassword,
                    controller: confirmPass,
                    bottomMargin: 20.0,
                    isPassword: true,
                  ),
                  CustomButton(
                    text: 'Create account',
                    onPressed: () async {
                      print("valeur du text en entrée");
                      print("email........................" + email.text);
                      print(EmailValidator.validate(email.text));
                      final bool isValid = EmailValidator.validate(email.text);
                      if (!isValid) {
                        Fluttertoast.showToast(
                          msg: 'Invalid email address',
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.BOTTOM,
                          backgroundColor: Color(0xFF011424),
                          textColor: Colors.white,
                          fontSize: 15.0,
                        );
                      } else {
                        print(name.text == "");
                        if (name.text != "" &&
                            email.text != "" &&
                            password.text != "" &&
                            confirmPass.text != "") {
                          setState(() {
                            _isLoading = true;
                          });
                          await AuthService()
                              .register(
                                  name.text,
                                  email.text,
                                  password.text,
                                  confirmPass.text,
                                  typeclient.toString(),
                                  typetherapist.toString())
                              .then(
                            (value) {
                              if (value == true) {
                                setState(() {
                                  _isLoading = false;
                                });
                                Fluttertoast.showToast(
                                  msg: 'Account created successfully!',
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.BOTTOM,
                                  backgroundColor: Colors.green,
                                  textColor: Colors.white,
                                  fontSize: 13.0,
                                );
                                Navigator.pushNamed(context, '/login');
                              } else {
                                setState(() {
                                  _isLoading = false;
                                });
                                Fluttertoast.showToast(
                                  msg: value.toString(),
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.BOTTOM,
                                  backgroundColor: Color(0xFF011424),
                                  textColor: Colors.white,
                                  fontSize: 15.0,
                                );
                              }
                            },
                          );
                        } else {
                          Fluttertoast.showToast(
                            msg: 'All fields are required',
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.BOTTOM,
                            backgroundColor: Color(0xFF011424),
                            textColor: Colors.white,
                            fontSize: 15.0,
                          );
                        }
                      }
                    },
                  ),
                  SizedBox(height: 30.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "Already have an account?  ",
                        style: TextStyle(
                          color: Color(0xFF474E6C).withOpacity(0.8),
                          fontFamily: 'Source Pro',
                          fontSize: 12.0,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.popAndPushNamed(context, '/');
                        },
                        child: Text(
                          'Login.',
                          style: TextStyle(
                            color: Color(0xFF320A3B),
                            fontFamily: 'Source SemiBold',
                            fontSize: 12.0,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          isLoading: _isLoading,
          color: Colors.white,
          opacity: 0.5,
          progressIndicator: CircularProgressIndicator(
            backgroundColor: Color(0xFF9C27B0),
          ),
        ),
      ),
    );
  }
}
