import 'package:flutter/material.dart';
import 'package:healing/core/models/comment.dart';
import 'package:healing/services/therapist_service.dart';
import 'package:healing/core/models/article.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:intl/intl.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class CommentsTherapist extends StatefulWidget {
  @override
  _CommentsTherapistState createState() => _CommentsTherapistState();
}

class _CommentsTherapistState extends State<CommentsTherapist> {
  List<Article> articles = [];
  bool hasdata = false;
  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: Color(0xFFECD2FF),
            child: Flex(
              direction: Axis.vertical,
              children: [
                Container(
                    margin: EdgeInsets.only(left: 0.0),
                    padding: EdgeInsets.only(top: 10.0, left: 10.0),
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(5.0),
                        topLeft: Radius.circular(5.0),
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Container(
                            padding: EdgeInsets.all(10.0),
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.grey.shade300,
                                width: 1.0,
                                style: BorderStyle.solid,
                              ),
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            child: Icon(
                              Icons.arrow_back_ios,
                              color: Colors.grey,
                            ),
                          ),
                        ),
                        Text(
                          'See all reviews',
                          style: TextStyle(
                            fontFamily: 'Source Pro SemiBold',
                            fontSize: 18.0,
                            color: Color(0xFF7806D2),
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        )
                      ],
                    )),
                Expanded(
                  flex: 4,
                  child: Container(
                      padding:
                          EdgeInsets.only(top: 20.0, left: 10.0, bottom: 12),
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(),
                      ),
                      child: CharacterListView()),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class CharacterListView extends StatefulWidget {
  @override
  _CharacterListViewState createState() => _CharacterListViewState();
}

class _CharacterListViewState extends State<CharacterListView> {
  static const _pageSize = 3;
  final PagingController<int, Comm> _pagingController =
      PagingController(firstPageKey: 0);

  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });
    super.initState();
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      final newItems = await TherapistService().getTherapistById();
      final isLastPage = true;
      if (isLastPage) {
        _pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = pageKey + newItems.length;
        _pagingController.appendPage(newItems, nextPageKey);
      }
    } catch (error) {
      _pagingController.error = error;
    }
  }

  @override
  Widget build(BuildContext context) => PagedListView<int, Comm>(
        pagingController: _pagingController,
        builderDelegate: PagedChildBuilderDelegate<Comm>(
          itemBuilder: (context, item, index) => Comment(
            commentator: item.commentator,
            createdAt: item.createdAt,
            updateAt: item.updateAt,
            article: item.article,
            content: item.content,
          ),
          firstPageProgressIndicatorBuilder: (_) => Container(
              height: 20,
              width: 20,
              child: Center(
                child: CircularProgressIndicator(
                  backgroundColor: Color(0xFF9C27B0),
                ),
              )),
          newPageErrorIndicatorBuilder: (_) => Container(
            margin: EdgeInsets.only(bottom: 20, top: 10),
            child: Column(
              children: [
                Text("Sommething went wrong"),
                SizedBox(
                  height: 8,
                ),
                GestureDetector(
                  onTap: () {
                    print("refrech");
                    _pagingController.retryLastFailedRequest();
                  },
                  child: Icon(
                    Icons.refresh,
                    size: 40,
                    color: Color(0xFF7806D2),
                  ),
                )
              ],
            ),
          ),
        ),
      );

  @override
  void dispose() {
    _pagingController.dispose();
    super.dispose();
  }
}

class Comment extends StatelessWidget {
  const Comment({
    Key key,
    this.createdAt,
    this.article,
    this.updateAt,
    this.commentator,
    this.content,
  }) : super(key: key);

  final String commentator;
  final String content;
  final String createdAt;
  final String updateAt;

  final String article;

  @override
  Widget build(BuildContext context) {
    //String strDt = this.date;
    // DateTime parseDt = DateTime.parse(strDt);
    //print(parseDt); // 1974-03-20 00:00:00.000
    String strDt = createdAt;
    DateTime parseDt = DateTime.parse(strDt);
    return Container(
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ListTile(
              leading: CircleAvatar(
                backgroundImage: AssetImage('assets/images/doctors/avatar.jpg'),
              ),
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    this.commentator,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
                  ),
                  Text(timeago.format(parseDt),
                      style: TextStyle(
                          fontSize: 12,
                          color: Colors.grey,
                          fontWeight: FontWeight.bold)),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 70),
              decoration: BoxDecoration(
                color: Color(0xFF7806D2).withOpacity(0.05),
                borderRadius: BorderRadius.circular(30.0),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.1),
                    spreadRadius: 0.0,
                    blurRadius: 10.0,
                    offset: Offset(0, 10.0),
                  ),
                ],
              ),
              padding: EdgeInsets.all(10),
              child: Text(
                content ?? '',
                style: TextStyle(fontSize: 15),
              ),
            ),
          ]),
    );
  }
}
