import 'package:flutter/material.dart';
import 'package:healing/core/models/availibilities.dart';
import 'package:healing/core/models/booking.dart';
import 'package:healing/services/authService.dart';
import 'package:healing/services/therapist_service.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_rounded_date_picker/flutter_rounded_date_picker.dart';
import 'package:jiffy/jiffy.dart';
import 'package:intl/intl.dart';
import 'package:fluttertoast/fluttertoast.dart';

/// The app which hosts the home page which contains the calendar on i

/// The hove page which hosts the calendar
class MyHomePage extends StatefulWidget {
  /// Creates the home page to display teh calendar widget.

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  void _onSelectionChanged(DateRangePickerSelectionChangedArgs args) {
    // TODO: implement your code here
    print(args.value);
  }

  TimeOfDay selectedTime = TimeOfDay.now();
  TimeOfDay start_time = TimeOfDay.now();
  DateTime daye = DateTime.now();
  TimeOfDay end_time =
      TimeOfDay.fromDateTime(DateTime.now().add(const Duration(hours: 1)));

  List<Meeting> meetings = [];
  List<String> meetings_id = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    TherapistService().listAviabilities().then((value) {
      List<Meeting> mitt = [];
      for (Availibilitie bo in value) {
        String strst = bo.start_date;
        String endst = bo.end_date;
        DateTime start_da = DateTime.parse(strst);
        DateTime end_da = DateTime.parse(endst);
        Meeting me =
            Meeting('', start_da, end_da, const Color(0xFF0F8644), false);
        mitt.add(me);
        meetings_id.add(bo.id);
      }
      print("mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm");
      print(mitt.length);
      setState(() {
        meetings = mitt;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            padding: EdgeInsets.fromLTRB(10.0, 40.0, 20.0, 10.0),
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Column(children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      alignment: Alignment.center,
                      height: 35.0,
                      width: 47,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5.0),
                        color: Color(0xFFB247BC).withOpacity(0.18),
                      ),
                      child: Icon(
                        Icons.arrow_back,
                        color: Color(0xFF320A3B),
                        size: 25,
                      ),
                    ),
                  ),
                  Text(
                    'Shedule calendar',
                    style: TextStyle(
                      color: Color(0xFF320A3B),
                      fontFamily: 'Coves Bold',
                      fontSize: 18.0,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 40.0),
              SfCalendar(
                view: CalendarView.workWeek,
                onTap: (value) async {
                  print(value.appointments);
                  print(meetings.length);
                  if (value.appointments != null) {
                    Meeting m = value.appointments[0] as Meeting;
                    print(m.from);
                    print(m.to);
                    TimeOfDay start_update = TimeOfDay.fromDateTime(m.from);
                    TimeOfDay end_update = TimeOfDay.fromDateTime(m.to);
                    DateTime day_update = m.from;
                    print(meetings.indexOf(m));
                    bool update = false;

                    showModalBottomSheet(
                      context: context,
                      isScrollControlled: true,
                      isDismissible: true,
                      builder: (BuildContext context) {
                        return StatefulBuilder(builder:
                            (BuildContext context, StateSetter stateSetter) {
                          return Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.all(10.0),
                            height: 150.0,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(50.0),
                                topRight: Radius.circular(50.0),
                              ),
                            ),
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    GestureDetector(
                                      onTap: () async {
                                        DateTime newDateTime =
                                            await showRoundedDatePicker(
                                          context: context,
                                          height: 300,
                                          theme: ThemeData(
                                            primaryColor: Color(0xFF320A3B),
                                            accentColor: Color(0xFF320A3B),
                                            textTheme: TextTheme(
                                              caption: TextStyle(
                                                  color: Color(0xFF320A3B)),
                                            ),
                                          ),
                                        );
                                        stateSetter(() {
                                          if (newDateTime != day_update) {
                                            day_update = newDateTime;
                                            update = true;
                                          }
                                        });
                                      },
                                      child: Container(
                                        alignment: Alignment.center,
                                        height: 35.0,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(0.0),
                                          color: Colors.blue.withOpacity(0.3),
                                        ),
                                        child: Text(
                                          Jiffy([
                                            day_update.year,
                                            day_update.month,
                                            day_update.day
                                          ]).yMMMMd,
                                          style: TextStyle(
                                            fontSize: 16,
                                            fontFamily: 'Source SemiBold',
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        GestureDetector(
                                          onTap: () async {
                                            TimeOfDay new_start_time =
                                                await showTimePicker(
                                              context: context,
                                              initialTime: start_update,
                                              initialEntryMode:
                                                  TimePickerEntryMode.input,
                                              confirmText: "CONFIRM",
                                              cancelText: "NOT NOW",
                                              helpText: "BOOKING TIME",
                                            );
                                            stateSetter(() {
                                              if (new_start_time != null) {
                                                start_update = new_start_time;
                                                final DateTime startTimee =
                                                    DateTime(
                                                        day_update.year,
                                                        day_update.month,
                                                        day_update.day,
                                                        start_update.hour,
                                                        start_update.minute,
                                                        0);
                                                end_update =
                                                    TimeOfDay.fromDateTime(
                                                        startTimee.add(
                                                            const Duration(
                                                                hours: 1)));
                                                update = true;
                                              }
                                            });
                                          },
                                          child: Container(
                                              alignment: Alignment.center,
                                              height: 35.0,
                                              width: 80.0,
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(0.0),
                                                color: Colors.blue
                                                    .withOpacity(0.3),
                                              ),
                                              child: Text(
                                                DateFormat.jm()
                                                        .format(DateTime(
                                                            day_update.year,
                                                            day_update.month,
                                                            day_update.day,
                                                            start_update.hour,
                                                            start_update
                                                                .minute))
                                                        .toLowerCase() +
                                                    " ",
                                                style: TextStyle(
                                                  fontSize: 16,
                                                  fontFamily: 'Source SemiBold',
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              )),
                                        ),
                                        Text(
                                          " - ",
                                          style: TextStyle(
                                            fontSize: 20,
                                            fontFamily: 'Source SemiBold',
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                        GestureDetector(
                                          onTap: () async {
                                            TimeOfDay end_start_time =
                                                await showTimePicker(
                                              context: context,
                                              initialTime: end_update,
                                              initialEntryMode:
                                                  TimePickerEntryMode.input,
                                              confirmText: "CONFIRM",
                                              cancelText: "NOT NOW",
                                              helpText: "BOOKING TIME",
                                            );
                                            stateSetter(() {
                                              if (end_start_time != null) {
                                                end_update = end_start_time;
                                                update = true;
                                              }
                                            });
                                          },
                                          child: Container(
                                            alignment: Alignment.center,
                                            height: 35.0,
                                            width: 80.0,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(0.0),
                                              color:
                                                  Colors.blue.withOpacity(0.3),
                                            ),
                                            child: Text(
                                              DateFormat.jm()
                                                  .format(DateTime(
                                                      day_update.year,
                                                      day_update.month,
                                                      day_update.day,
                                                      end_update.hour,
                                                      end_update.minute))
                                                  .toLowerCase(),
                                              style: TextStyle(
                                                fontSize: 16,
                                                fontFamily: 'Source SemiBold',
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 30,
                                ),
                                if (update == false)
                                  GestureDetector(
                                      onTap: () async {
                                        AuthService()
                                            .deleteAvailibilities(meetings_id[
                                                meetings.indexOf(m)])
                                            .then((value) {
                                          if (value) {
                                            Fluttertoast.showToast(
                                              msg: 'delete successfully!!',
                                              toastLength: Toast.LENGTH_SHORT,
                                              gravity: ToastGravity.BOTTOM,
                                              backgroundColor:
                                                  Color(0xFF011424),
                                              textColor: Colors.white,
                                              fontSize: 15.0,
                                            );
                                            Navigator.pop(context);
                                            List<Meeting> ald = meetings;
                                            setState(() {
                                              meetings_id.remove(
                                                  meetings_id[ald.indexOf(m)]);
                                              ald.remove(m);
                                              meetings = ald;
                                            });
                                          } else {
                                            Fluttertoast.showToast(
                                              msg:
                                                  'An error occured during the delete addAvailibilities',
                                              toastLength: Toast.LENGTH_SHORT,
                                              gravity: ToastGravity.BOTTOM,
                                              backgroundColor:
                                                  Color(0xFF011424),
                                              textColor: Colors.white,
                                              fontSize: 15.0,
                                            );
                                            print("comment faire la guerre");
                                          }
                                        });
                                      },
                                      child: Container(
                                        alignment: Alignment.center,
                                        height: 35.0,
                                        width: 150,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(30.0),
                                          color: Colors.red,
                                        ),
                                        child: Text(
                                          'Delete ',
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontFamily: 'Coves Bold',
                                            fontSize: 18.0,
                                          ),
                                        ),
                                      )),
                                if (update == true)
                                  GestureDetector(
                                      onTap: () async {
                                        AuthService()
                                            .updateAvailibilities(
                                                meetings_id[
                                                    meetings.indexOf(m)],
                                                DateTime(
                                                        day_update.year,
                                                        day_update.month,
                                                        day_update.day,
                                                        start_update.hour,
                                                        start_update.minute)
                                                    .toString(),
                                                DateTime(
                                                        day_update.year,
                                                        day_update.month,
                                                        day_update.day,
                                                        end_update.hour,
                                                        end_update.minute)
                                                    .toString())
                                            .then((value) {
                                          if (value) {
                                            Fluttertoast.showToast(
                                              msg: 'update successfully!!',
                                              toastLength: Toast.LENGTH_SHORT,
                                              gravity: ToastGravity.BOTTOM,
                                              backgroundColor:
                                                  Color(0xFF011424),
                                              textColor: Colors.white,
                                              fontSize: 15.0,
                                            );
                                            Navigator.pop(context);
                                            Meeting meti = Meeting(
                                                '',
                                                DateTime(
                                                    day_update.year,
                                                    day_update.month,
                                                    day_update.day,
                                                    start_update.hour,
                                                    start_update.minute),
                                                DateTime(
                                                    day_update.year,
                                                    day_update.month,
                                                    day_update.day,
                                                    end_update.hour,
                                                    end_update.minute),
                                                const Color(0xFF0F8644),
                                                false);

                                            setState(() {
                                              meetings[meetings.indexOf(m)] =
                                                  meti;
                                            });
                                          } else {
                                            Fluttertoast.showToast(
                                              msg:
                                                  'An error occured during the update addAvailibilities',
                                              toastLength: Toast.LENGTH_SHORT,
                                              gravity: ToastGravity.BOTTOM,
                                              backgroundColor:
                                                  Color(0xFF011424),
                                              textColor: Colors.white,
                                              fontSize: 15.0,
                                            );
                                            print("comment faire la guerre");
                                          }
                                        });
                                      },
                                      child: Container(
                                        alignment: Alignment.center,
                                        height: 35.0,
                                        width: 150,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(30.0),
                                          color: Color(0xFF320A3B),
                                        ),
                                        child: Text(
                                          'update',
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontFamily: 'Coves Bold',
                                            fontSize: 18.0,
                                          ),
                                        ),
                                      ))
                              ],
                            ),
                          );
                        });
                      },
                    );
                  }
                },

                todayHighlightColor: Color(0xFF320A3B),
                cellBorderColor: Color(0xFF320A3B),
                dataSource: MeetingDataSource(meetings),
                // by default the month appointment display mode set as Indicator, we can
                // change the display mode as appointment using the appointment display
                // mode property
                monthViewSettings: const MonthViewSettings(
                    appointmentDisplayMode:
                        MonthAppointmentDisplayMode.appointment),
              ),
              SizedBox(height: 40.0),
              Container(
                  alignment: Alignment.center,
                  height: 35.0,
                  width: 300,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30.0),
                    color: Color(0xFF320A3B),
                  ),
                  child: GestureDetector(
                    onTap: () async {
                      /*
                      showTimePicker(
                        context: context,
                        initialTime: selectedTime,
                        initialEntryMode: TimePickerEntryMode.input,
                        confirmText: "CONFIRM",
                        cancelText: "NOT NOW",
                        helpText: "BOOKING TIME",
                      );
                      */
                      showModalBottomSheet(
                        context: context,
                        isScrollControlled: true,
                        isDismissible: true,
                        builder: (BuildContext context) {
                          return StatefulBuilder(builder:
                              (BuildContext context, StateSetter stateSetter) {
                            return Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.all(10.0),
                              height: 150.0,
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(50.0),
                                  topRight: Radius.circular(50.0),
                                ),
                              ),
                              child: Column(
                                children: [
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      GestureDetector(
                                        onTap: () async {
                                          DateTime newDateTime =
                                              await showRoundedDatePicker(
                                            context: context,
                                            height: 300,
                                            theme: ThemeData(
                                              primaryColor: Color(0xFF320A3B),
                                              accentColor: Color(0xFF320A3B),
                                              textTheme: TextTheme(
                                                caption: TextStyle(
                                                    color: Color(0xFF320A3B)),
                                              ),
                                            ),
                                          );
                                          stateSetter(() {
                                            if (newDateTime != null) {
                                              daye = newDateTime;
                                            }
                                          });
                                        },
                                        child: Container(
                                          alignment: Alignment.center,
                                          height: 35.0,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(0.0),
                                            color: Colors.blue.withOpacity(0.3),
                                          ),
                                          child: Text(
                                            Jiffy([
                                              daye.year,
                                              daye.month,
                                              daye.day
                                            ]).yMMMMd,
                                            style: TextStyle(
                                              fontSize: 16,
                                              fontFamily: 'Source SemiBold',
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          GestureDetector(
                                            onTap: () async {
                                              TimeOfDay new_start_time =
                                                  await showTimePicker(
                                                context: context,
                                                initialTime: start_time,
                                                initialEntryMode:
                                                    TimePickerEntryMode.input,
                                                confirmText: "CONFIRM",
                                                cancelText: "NOT NOW",
                                                helpText: "BOOKING TIME",
                                              );
                                              stateSetter(() {
                                                if (new_start_time != null) {
                                                  start_time = new_start_time;
                                                  final DateTime startTimee =
                                                      DateTime(
                                                          daye.year,
                                                          daye.month,
                                                          daye.day,
                                                          start_time.hour,
                                                          start_time.minute,
                                                          0);
                                                  end_time =
                                                      TimeOfDay.fromDateTime(
                                                          startTimee.add(
                                                              const Duration(
                                                                  hours: 1)));
                                                }
                                              });
                                            },
                                            child: Container(
                                                alignment: Alignment.center,
                                                height: 35.0,
                                                width: 80.0,
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          0.0),
                                                  color: Colors.blue
                                                      .withOpacity(0.3),
                                                ),
                                                child: Text(
                                                  DateFormat.jm()
                                                          .format(DateTime(
                                                              DateTime.now()
                                                                  .year,
                                                              DateTime.now()
                                                                  .month,
                                                              DateTime.now()
                                                                  .day,
                                                              start_time.hour,
                                                              start_time
                                                                  .minute))
                                                          .toLowerCase() +
                                                      " ",
                                                  style: TextStyle(
                                                    fontSize: 16,
                                                    fontFamily:
                                                        'Source SemiBold',
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                )),
                                          ),
                                          Text(
                                            " - ",
                                            style: TextStyle(
                                              fontSize: 20,
                                              fontFamily: 'Source SemiBold',
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          GestureDetector(
                                            onTap: () async {
                                              TimeOfDay end_start_time =
                                                  await showTimePicker(
                                                context: context,
                                                initialTime: end_time,
                                                initialEntryMode:
                                                    TimePickerEntryMode.input,
                                                confirmText: "CONFIRM",
                                                cancelText: "NOT NOW",
                                                helpText: "BOOKING TIME",
                                              );
                                              stateSetter(() {
                                                if (end_start_time != null) {
                                                  end_time = end_start_time;
                                                }
                                              });
                                            },
                                            child: Container(
                                              alignment: Alignment.center,
                                              height: 35.0,
                                              width: 80.0,
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(0.0),
                                                color: Colors.blue
                                                    .withOpacity(0.3),
                                              ),
                                              child: Text(
                                                DateFormat.jm()
                                                    .format(DateTime(
                                                        DateTime.now().year,
                                                        DateTime.now().month,
                                                        DateTime.now().day,
                                                        end_time.hour,
                                                        end_time.minute))
                                                    .toLowerCase(),
                                                style: TextStyle(
                                                  fontSize: 16,
                                                  fontFamily: 'Source SemiBold',
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                  SizedBox(
                                    height: 30,
                                  ),
                                  GestureDetector(
                                      onTap: () async {
                                        AuthService()
                                            .addAvailibilities(
                                                DateTime(
                                                        daye.year,
                                                        daye.month,
                                                        daye.day,
                                                        start_time.hour,
                                                        start_time.minute)
                                                    .toString(),
                                                DateTime(
                                                        daye.year,
                                                        daye.month,
                                                        daye.day,
                                                        end_time.hour,
                                                        end_time.minute)
                                                    .toString())
                                            .then((value) {
                                          if (value != null) {
                                            Fluttertoast.showToast(
                                              msg:
                                                  'availibilities added successfully!',
                                              toastLength: Toast.LENGTH_SHORT,
                                              gravity: ToastGravity.BOTTOM,
                                              backgroundColor:
                                                  Color(0xFF011424),
                                              textColor: Colors.white,
                                              fontSize: 15.0,
                                            );
                                            setState(() {
                                              meetings.add(Meeting(
                                                  '',
                                                  DateTime(
                                                      daye.year,
                                                      daye.month,
                                                      daye.day,
                                                      start_time.hour,
                                                      start_time.minute),
                                                  DateTime(
                                                      daye.year,
                                                      daye.month,
                                                      daye.day,
                                                      end_time.hour,
                                                      end_time.minute),
                                                  const Color(0xFF0F8644),
                                                  false));
                                              meetings_id.add(value);

                                              Navigator.pop(context);
                                            });
                                          } else {
                                            Fluttertoast.showToast(
                                              msg:
                                                  'An error occured during the addAvailibilities add',
                                              toastLength: Toast.LENGTH_SHORT,
                                              gravity: ToastGravity.BOTTOM,
                                              backgroundColor:
                                                  Color(0xFF011424),
                                              textColor: Colors.white,
                                              fontSize: 15.0,
                                            );
                                            print("comment faire la guerre");
                                          }
                                        });
                                      },
                                      child: Container(
                                        alignment: Alignment.center,
                                        height: 35.0,
                                        width: 200,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(30.0),
                                          color: Color(0xFF320A3B),
                                        ),
                                        child: Text(
                                          'add availability',
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontFamily: 'Coves Bold',
                                            fontSize: 18.0,
                                          ),
                                        ),
                                      ))
                                ],
                              ),
                            );
                          });
                        },
                      );
                    },
                    child: Text(
                      'Set Shedule calendar',
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'Coves Bold',
                        fontSize: 18.0,
                      ),
                    ),
                  )),
            ])));
  }

  List<Meeting> _getDataSource() {
    final List<Meeting> meetings = <Meeting>[];
    final DateTime today = DateTime.now();
    final DateTime startTime =
        DateTime(today.year, today.month, today.day, 9, 0, 0);
    final DateTime endTime = startTime.add(const Duration(hours: 2));
    final DateTime startTimee =
        DateTime(today.year, today.month, today.day, 9, 1, 0);
    final DateTime endTimee = startTime.add(const Duration(hours: 3));
    meetings.add(Meeting(
        'Conference', startTime, endTime, const Color(0xFF0F8644), false));
    meetings.add(Meeting(
        'Conference', startTimee, endTimee, const Color(0xFF0F8644), false));
    return meetings;
  }
}

/// An object to set the appointment collection data source to calendar, which
/// used to map the custom appointment data to the calendar appointment, and
/// allows to add, remove or reset the appointment collection.
class MeetingDataSource extends CalendarDataSource {
  /// Creates a meeting data source, which used to set the appointment
  /// collection to the calendar
  MeetingDataSource(List<Meeting> source) {
    appointments = source;
  }

  @override
  DateTime getStartTime(int index) {
    return _getMeetingData(index).from;
  }

  @override
  DateTime getEndTime(int index) {
    return _getMeetingData(index).to;
  }

  @override
  String getSubject(int index) {
    return _getMeetingData(index).eventName;
  }

  @override
  Color getColor(int index) {
    return _getMeetingData(index).background;
  }

  @override
  bool isAllDay(int index) {
    return _getMeetingData(index).isAllDay;
  }

  Meeting _getMeetingData(int index) {
    final dynamic meeting = appointments[index];
    Meeting meetingData;
    if (meeting is Meeting) {
      meetingData = meeting;
    }

    return meetingData;
  }
}

/// Custom business object class which contains properties to hold the detailed
/// information about the event data which will be rendered in calendar.
class Meeting {
  /// Creates a meeting class with required details.
  Meeting(this.eventName, this.from, this.to, this.background, this.isAllDay);

  /// Event name which is equivalent to subject property of [Appointment].
  String eventName;

  /// From which is equivalent to start time property of [Appointment].
  DateTime from;

  /// To which is equivalent to end time property of [Appointment].
  DateTime to;

  /// Background which is equivalent to color property of [Appointment].
  Color background;

  /// IsAllDay which is equivalent to isAllDay property of [Appointment].
  bool isAllDay;
}
