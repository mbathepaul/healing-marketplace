import 'package:flutter/material.dart';
import 'package:healing/core/models/doctor.dart';
import 'package:healing/widgets/therapeutist.dart';
import 'package:healing/services/therapist_service.dart';

class TherapeutistList extends StatefulWidget {
  const TherapeutistList({
    Key key,
  }) : super(key: key);

  @override
  _TherapeutistListState createState() => _TherapeutistListState();
}

class _TherapeutistListState extends State<TherapeutistList> {
  List<Doctor> doctors = [
    Doctor(
      id: "vnodvurbyu",
      user: 'Simon Christiansen',
      description: 'Neurologist',
      phone: '4.0',
      image: 'assets/images/doctors/1.jpg',
    ),
    Doctor(
      id: "vconovnaoubsye",
      user: 'Christine Swords',
      description: 'Neurologist',
      phone: '4.0',
      image: 'assets/images/doctors/2.jpg',
    ),
    Doctor(
      id: "vnoanvoubnur",
      user: 'Theresa Mullins',
      description: 'Neurologist',
      phone: '4.0',
      image: 'assets/images/doctors/3.jpg',
    ),
    Doctor(
      id: "vnoanvoanvon",
      user: 'Clara McDonald',
      description: 'Neurologist',
      phone: '4.0',
      image: 'assets/images/doctors/4.png',
    ),
    Doctor(
      id: "vaovnoanvoanovn",
      user: 'Chloe Coldman',
      description: 'Neurologist',
      phone: '4.0',
      image: 'assets/images/doctors/5.png',
    ),
  ];

  num get rate => null;
  String categrie = "";

  @override
  Widget build(BuildContext context) {
    categrie = ModalRoute.of(context).settings.arguments.toString();
    print(categrie);
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 25.0),
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        padding: EdgeInsets.all(10.0),
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.grey.shade300,
                            width: 1.0,
                            style: BorderStyle.solid,
                          ),
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: Colors.grey,
                        ),
                      ),
                    ),
                    Container(
                      child: Text(
                        categrie != null ? categrie : 'Neurology',
                        style: TextStyle(
                          fontFamily: 'Coves Bold',
                          color: Colors.grey,
                          fontSize: 20.0,
                        ),
                      ),
                    ),
                    Container(
                      child: Icon(
                        Icons.more_horiz,
                        color: Colors.grey,
                      ),
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(
                    top: 10.0,
                  ),
                  height: MediaQuery.of(context).size.height - 151,
                  width: MediaQuery.of(context).size.width - 10,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 10.0,
                      ),
                      FutureBuilder(
                        future: TherapistService().getTherapist(categrie),
                        builder: (BuildContext context,
                            AsyncSnapshot<List<Doctor>> snapshot) {
                          if (snapshot.hasData) {
                            List<Doctor> doctors = snapshot.data;

                            return Container(
                              padding: EdgeInsets.only(top: 10.0, bottom: 10),
                              height: 480,
                              child:
                                  doctors.length != null && doctors.length > 0
                                      ? ListView.builder(
                                          itemCount: doctors.length,
                                          itemBuilder: (context, index) {
                                            return Therapeutist(
                                              key: new Key(doctors[index].id),
                                              name: doctors[index].user,
                                              image: doctors[index].image,
                                              rate: doctors[index].rate,
                                              doctor: doctors[index],
                                              clique: true,
                                            );
                                          },
                                        )
                                      : Text(
                                          "No results for the search term",
                                          style: TextStyle(
                                              fontFamily: 'Coves Bold',
                                              color: Colors.grey,
                                              fontSize: 22.0,
                                              fontWeight: FontWeight.bold),
                                        ),
                            );
                          }
                          return Center(
                            child: CircularProgressIndicator(
                              backgroundColor: Color(0xFF320A3B),
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
