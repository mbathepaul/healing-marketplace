import 'package:flutter/material.dart';
import 'package:healing/core/models/doctor.dart';
import 'package:healing/widgets/therapeutist.dart';
import 'package:healing/services/therapist_service.dart';

class SearchtList extends StatefulWidget {
  const SearchtList({
    Key key,
  }) : super(key: key);

  @override
  _SearchtListState createState() => _SearchtListState();
}

class _SearchtListState extends State<SearchtList> {
  num get rate => null;
  String search = "";

  @override
  Widget build(BuildContext context) {
    search = ModalRoute.of(context).settings.arguments.toString();
    print(search);
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(left: 10.0, right: 0.0, top: 10.0),
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        padding: EdgeInsets.all(10.0),
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.grey.shade300,
                            width: 1.0,
                            style: BorderStyle.solid,
                          ),
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: Colors.grey,
                        ),
                      ),
                    ),
                    Container(
                      child: Text(
                        "Search Results..",
                        style: TextStyle(
                          fontFamily: 'Coves Bold',
                          color: Colors.grey,
                          fontSize: 20.0,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(
                    top: 0.0,
                    left: 10,
                  ),
                  height: MediaQuery.of(context).size.height - 100,
                  width: MediaQuery.of(context).size.width - 10,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 10.0,
                      ),
                      FutureBuilder(
                        future: TherapistService().getTherapist(search),
                        builder: (BuildContext context,
                            AsyncSnapshot<List<Doctor>> snapshot) {
                          if (snapshot.hasData) {
                            List<Doctor> doctors = snapshot.data;

                            return Container(
                              padding: EdgeInsets.only(top: 10.0, left: 10),
                              height: MediaQuery.of(context).size.height - 112,
                              child:
                                  doctors.length != null && doctors.length > 0
                                      ? ListView.builder(
                                          itemCount: doctors.length,
                                          itemBuilder: (context, index) {
                                            return Therapeutist(
                                              key: new Key(doctors[index].id),
                                              name: doctors[index].user,
                                              image: doctors[index].image,
                                              rate: doctors[index].rate,
                                              doctor: doctors[index],
                                              clique: true,
                                            );
                                          },
                                        )
                                      : Text(
                                          "No results for the search term",
                                          style: TextStyle(
                                              fontFamily: 'Coves Bold',
                                              color: Colors.grey,
                                              fontSize: 22.0,
                                              fontWeight: FontWeight.bold),
                                        ),
                            );
                          }
                          return Center(
                            child: CircularProgressIndicator(
                              backgroundColor: Color(0xFF320A3B),
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
