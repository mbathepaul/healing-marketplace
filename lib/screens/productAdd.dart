import 'dart:io';

import 'package:flutter/material.dart';
import 'package:healing/core/models/product_category.dart';
import 'package:healing/services/therapist_service.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:healing/services/authService.dart';
import 'package:healing/widgets/custom_button.dart';
import 'package:healing/widgets/custom_field1.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:dropdownfield/dropdownfield.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import "package:healing/core/models/category_therapist.dart";

class ProductAddPage extends StatefulWidget {
  @override
  _ProductAddPageState createState() => new _ProductAddPageState();
}

class _ProductAddPageState extends State<ProductAddPage> {
  File _image;
  int group = 1;
  List<File> images = [];
  int _contimage = 0;
  bool _isLoading = false;
  static String id_sign_up;
  String cathegory_id = "";
  final picker = ImagePicker();
  var controller = new MoneyMaskedTextController(rightSymbol: ' US\$');
  Future getImage() async {
    PickedFile pickedFile = await picker.getImage(source: ImageSource.gallery);
    File image = File(pickedFile.path);
    setState(() {
      _image = image;
    });
  }

  String phpMsg;
  String regInfo;

  TextEditingController price = new TextEditingController();
  TextEditingController category = new TextEditingController();
  TextEditingController description = new TextEditingController();

  sendData() async {
    var value = controller.text.split("U")[0];
    var valueof = value.replaceAll(" ", "");

    if (valueof.split(".").length > 1) {
      valueof = valueof.replaceFirst(".", "");
    }
    var flot = valueof.replaceAll(",", ".");
    print(flot);
    var prix = double.parse(flot);
    print(prix);

    setState(() {
      _isLoading = true;
    });
    String id_category = "";
    for (CategoryTherapist x in cathegoryliste) {
      if (x.label == cathegory_id) {
        id_category = x.id;
      }
    }
    print(
        "connnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnddddddddddddddddddddddddddddddd");
    print(id_category);
    await AuthService()
        .addProduct(_image, prix, id_category, description.text)
        .then(
      (value) {
        if (value) {
          setState(() {
            _isLoading = false;
          });

          Fluttertoast.showToast(
            msg: 'Product successfully added!',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            backgroundColor: Colors.green,
            textColor: Colors.white,
            fontSize: 12.0,
          );
          Navigator.pop(context);
        } else {
          setState(() {
            _isLoading = false;
          });
          Fluttertoast.showToast(
            msg: 'An error occured during downloading',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 13.0,
          );
        }
      },
    );
  }

  List<String> cathegorylist = TherapistService.category_products_label;
  List<CategoryTherapist> cathegoryliste =
      TherapistService.category_products_id;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    /*
    TherapistService().getcathegories().then((value) => {
          
        });
        */
    TherapistService().getcathegories_shop().then((value) => {
          setState(() {
            TherapistService.category_products = value.cast<ProductCategory>();
          })
        });
    /*
    TherapistService().getcathegories_id().then((value) => {
          setState(() {
            cathegoryliste = value.cast<CategoryTherapist>();
            print(cathegoryliste.length);
          })
        });
    */
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text('Add product'),
          backgroundColor: Color(0xFF7912DD).withOpacity(0.7),
        ),
        body: Scaffold(
            body: LoadingOverlay(
          child: Container(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
              // Column is a vertical, linear layout.
              child: Container(
                child: ListView(
                  children: <Widget>[
                    if (_image == null)
                      SizedBox(
                        height: 20,
                      ),
                    if (_image != null)
                      SizedBox(
                        height: 10,
                      ),
                    if (_image != null)
                      GestureDetector(
                          onTap: () async {
                            getImage();
                          },
                          child: Center(
                            child: Container(
                              width: 120.0,
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.image,
                                    color: Color(0xFF7912DD),
                                    size: 20,
                                  ),
                                  Text(
                                    'Change image',
                                    style: TextStyle(
                                      color: Color(0xFF7912DD),
                                      fontFamily: 'Coves Bold',
                                      fontSize: 16.0,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )),
                    if (_image != null)
                      SizedBox(
                        height: 10,
                      ),
                    _image == null
                        ? GestureDetector(
                            onTap: () async {
                              getImage();
                            },
                            child: Center(
                              child: Container(
                                width: 120.0,
                                child: Row(
                                  children: [
                                    Icon(
                                      Icons.image,
                                      color: Color(0xFF7912DD),
                                      size: 20,
                                    ),
                                    Text(
                                      ' Select image',
                                      style: TextStyle(
                                        color: Color(0xFF7912DD),
                                        fontFamily: 'Coves Bold',
                                        fontSize: 18.0,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ))
                        : Container(
                            height: 200,
                            margin: EdgeInsets.only(left: 5.0, right: 5.0),
                            child: Image.file(
                              _image,
                              fit: BoxFit.contain,
                            ),
                          ),
                    Text("                    "),
                    Text("                    "),
                    CustomField(
                      controller: controller,
                      label: 'Price',
                      helper: 'Enter the Price',
                      actionText: '',
                      type: TextInputType.number,
                      color: true,
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(
                          horizontal: 15.0, vertical: 10.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(5.0),
                        border:
                            Border.all(color: Color(0xFF7912DD), width: 1.5),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.2),
                            spreadRadius: 0.0,
                            blurRadius: 10.0,
                            offset: Offset(0, 8.0),
                          ),
                        ],
                      ),
                      child: DropDownField(
                        onValueChanged: (dynamic value) {
                          print("valeur " + value);
                          cathegory_id = value;
                        },
                        value: cathegory_id,
                        required: false,
                        hintText: 'Choose a category ',
                        labelText: 'category ',
                        items: cathegorylist,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    CustomField(
                      controller: description,
                      label: 'Name',
                      helper: 'Enter the Name',
                      actionText: '',
                      type: TextInputType.text,
                      color: true,
                    ),
                    Text("                    "),
                    RaisedButton(
                      onPressed: () {
                        sendData();
                        // Validate will return true if the form is valid, or false if
                        // the form is invalid.
                      },
                      child: Text(
                        'Send',
                        style:
                            TextStyle(color: Color(0xFF7912DD), fontSize: 18),
                      ),
                    )
                  ],
                ),
              )),
          isLoading: _isLoading,
          color: Colors.white,
          opacity: 0.5,
          progressIndicator: CircularProgressIndicator(
            backgroundColor: Color(0xFF9C27B0),
          ),
        )));
  }
}
