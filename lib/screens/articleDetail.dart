import 'package:flutter/material.dart';
import 'package:healing/core/models/article.dart';
import 'package:healing/core/models/comment.dart';
import 'package:healing/services/authService.dart';
import 'package:healing/widgets/custom_field1.dart';
import 'package:jiffy/jiffy.dart';
import 'package:jiffy/src/enums/units.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:loading_overlay/loading_overlay.dart';

class ArticleDetail extends StatefulWidget {
  @override
  _Detail createState() => _Detail();
}

class _Detail extends State<ArticleDetail> {
  TextEditingController commentText = TextEditingController();
  bool _isLoading = false;
  bool in_send = false;
  List<Comm> comments = [];
  @override
  Widget build(BuildContext context) {
    Article article = (ModalRoute.of(context).settings.arguments as Article);
    String strDt = article.createdAt;
    DateTime parseDt = DateTime.parse(strDt);

    print(Jiffy([parseDt.year, parseDt.month, parseDt.day])
        .yMMMMd); // January 19, 2021
    // 1974-03-20 00:00:00.000
    print("testabnoanvn....................................");
    comments = article.comments;
    ScrollController _scrollController = new ScrollController();

    return Scaffold(
        backgroundColor: Colors.white,
        body: LoadingOverlay(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 24,
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 24, vertical: 12),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            GestureDetector(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: Container(
                                padding: EdgeInsets.all(10.0),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.grey.shade300,
                                    width: 1.0,
                                    style: BorderStyle.solid,
                                  ),
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                child: Icon(
                                  Icons.arrow_back_ios,
                                  color: Colors.grey,
                                  size: 20,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 30,
                            ),
                            Text(
                              article.category,
                              style: TextStyle(
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black.withOpacity(.8)),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(""),
                            Text(
                              Jiffy([parseDt.year, parseDt.month, parseDt.day])
                                  .yMMMMd,
                              style:
                                  TextStyle(color: Colors.grey, fontSize: 12),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 5,
                        ),
                      ],
                    ),

                    // ClipOval(child: Image.asset('assets/user.png',width: 59,))
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: Colors.white,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(left: 15, right: 15),
                          child: Center(
                            child: FadeInImage(
                              height: 180,
                              width: MediaQuery.of(context).size.width,
                              fit: BoxFit.fitWidth,
                              alignment: FractionalOffset.topCenter,
                              placeholder:
                                  AssetImage('assets/images/doctors/1.jpg'),
                              image: article.image != null
                                  ? NetworkImage(article.image)
                                  : AssetImage('assets/images/backImage.jpg'),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 15, top: 12, bottom: 13, right: 15),
                          child: Text(
                            article.label,
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Color(0xFF7806D2)),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 15, top: 8, bottom: 20, right: 15),
                          child: Text(
                            article.content,
                            style: TextStyle(
                                fontSize: 15,
                                color: Colors.black.withOpacity(.8)),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10, bottom: 40),
                          child: Row(
                            children: <Widget>[
                              SizedBox(
                                width: 110,
                              ),
                              GestureDetector(
                                onTap: () {
                                  print("bonjour");
                                  showModalBottomSheet(
                                      context: context,
                                      isScrollControlled: true,
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.vertical(
                                              top: Radius.circular(25.0))),
                                      builder: (context) {
                                        return FractionallySizedBox(
                                            heightFactor: 0.9,
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                  left: 0.0,
                                                  top: 20,
                                                  right: 20),
                                              height: 500.0,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.only(
                                                  topLeft:
                                                      Radius.circular(50.0),
                                                  topRight:
                                                      Radius.circular(50.0),
                                                ),
                                              ),
                                              child: StatefulBuilder(
                                                builder: (BuildContext context,
                                                    StateSetter stateSetter) {
                                                  return Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment.end,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Expanded(
                                                          child:
                                                              ListView.builder(
                                                                  itemCount:
                                                                      comments
                                                                          .length,
                                                                  shrinkWrap:
                                                                      true,
                                                                  controller:
                                                                      _scrollController,
                                                                  itemBuilder:
                                                                      (context,
                                                                          index) {
                                                                    return Comment(
                                                                        commentator: article
                                                                            .comments[
                                                                                index]
                                                                            .commentator,
                                                                        createdAt: article
                                                                            .comments[
                                                                                index]
                                                                            .createdAt,
                                                                        updateAt: article
                                                                            .comments[
                                                                                index]
                                                                            .updateAt,
                                                                        content: article
                                                                            .comments[
                                                                                index]
                                                                            .content,
                                                                        article: article
                                                                            .comments[index]
                                                                            .article);
                                                                  })),
                                                      Padding(
                                                        padding:
                                                            EdgeInsets.only(
                                                                left: 20,
                                                                right: 0),
                                                        child: Container(
                                                          height: 65,
                                                          width: MediaQuery.of(
                                                                      context)
                                                                  .size
                                                                  .width -
                                                              20,
                                                          padding: EdgeInsets
                                                              .symmetric(
                                                                  horizontal:
                                                                      15.0,
                                                                  vertical:
                                                                      10.0),
                                                          margin:
                                                              EdgeInsets.only(
                                                                  bottom: 20),
                                                          decoration:
                                                              BoxDecoration(
                                                            color: Colors.white,
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        20.0),
                                                            border: Border.all(
                                                                color: Colors
                                                                    .white
                                                                    .withOpacity(
                                                                        0.5),
                                                                width: 1.5),
                                                            boxShadow: [
                                                              BoxShadow(
                                                                color: Colors
                                                                    .black
                                                                    .withOpacity(
                                                                        0.2),
                                                                spreadRadius:
                                                                    0.0,
                                                                blurRadius:
                                                                    10.0,
                                                                offset: Offset(
                                                                    0, 8.0),
                                                              ),
                                                            ],
                                                          ),
                                                          child: Row(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: <Widget>[
                                                              Expanded(
                                                                child:
                                                                    TextField(
                                                                  controller:
                                                                      commentText,
                                                                  keyboardType:
                                                                      TextInputType
                                                                          .text,
                                                                  maxLines: 2,
                                                                  obscureText:
                                                                      false,
                                                                  style:
                                                                      TextStyle(
                                                                    fontFamily:
                                                                        'Source SemiBold',
                                                                    fontSize:
                                                                        18.0,
                                                                    color: Color(
                                                                            0xFF474E6C)
                                                                        .withOpacity(
                                                                            0.8),
                                                                  ),
                                                                  decoration:
                                                                      InputDecoration(
                                                                    isDense:
                                                                        true,
                                                                    floatingLabelBehavior:
                                                                        FloatingLabelBehavior
                                                                            .never,
                                                                    contentPadding:
                                                                        EdgeInsets.fromLTRB(
                                                                            0,
                                                                            0,
                                                                            0,
                                                                            0.0),
                                                                    labelText:
                                                                        "Add your comment here...",
                                                                    border:
                                                                        OutlineInputBorder(
                                                                      borderSide:
                                                                          BorderSide(
                                                                              color: Colors.transparent),
                                                                    ),
                                                                    focusedBorder:
                                                                        OutlineInputBorder(
                                                                      borderSide:
                                                                          BorderSide(
                                                                              color: Colors.transparent),
                                                                    ),
                                                                    enabledBorder:
                                                                        OutlineInputBorder(
                                                                      borderSide:
                                                                          BorderSide(
                                                                              color: Colors.transparent),
                                                                    ),
                                                                    labelStyle:
                                                                        TextStyle(
                                                                      fontFamily:
                                                                          'Source SemiBold',
                                                                      fontSize:
                                                                          12.0,
                                                                      color: Color(
                                                                              0xFF474E6C)
                                                                          .withOpacity(
                                                                              0.5),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                              in_send == false
                                                                  ? GestureDetector(
                                                                      onTap:
                                                                          () async {
                                                                        print(
                                                                            "send");

                                                                        stateSetter(() =>
                                                                            in_send =
                                                                                true);
                                                                        AuthService()
                                                                            .addCommentArticle(article.id.toString(),
                                                                                commentText.text)
                                                                            .then((value) {
                                                                          if (value) {
                                                                            stateSetter(() {
                                                                              in_send = false;

                                                                              Comm co = Comm(article: article.label, commentator: AuthService.username, updateAt: DateTime.now().toString(), createdAt: DateTime.now().toString(), content: commentText.text);
                                                                              comments.add(co);
                                                                              commentText.text = "";
                                                                            });
                                                                            _scrollController.animateTo(
                                                                              _scrollController.position.maxScrollExtent + 5,
                                                                              duration: const Duration(milliseconds: 100),
                                                                              curve: Curves.fastOutSlowIn,
                                                                            );
                                                                            Fluttertoast.showToast(
                                                                              msg: 'Comment add successfully!',
                                                                              toastLength: Toast.LENGTH_SHORT,
                                                                              gravity: ToastGravity.BOTTOM,
                                                                              backgroundColor: Color(0xFF011424),
                                                                              textColor: Colors.white,
                                                                              fontSize: 15.0,
                                                                            );
                                                                            print("comment faire la guerre");
                                                                          } else {
                                                                            stateSetter(() =>
                                                                                in_send = false);
                                                                            Fluttertoast.showToast(
                                                                              msg: 'An error occured during adding the comment',
                                                                              toastLength: Toast.LENGTH_SHORT,
                                                                              gravity: ToastGravity.BOTTOM,
                                                                              backgroundColor: Color(0xFF011424),
                                                                              textColor: Colors.white,
                                                                              fontSize: 15.0,
                                                                            );
                                                                            print("ça n'a pas marché");
                                                                          }
                                                                        });
                                                                      },
                                                                      child:
                                                                          Icon(
                                                                        Icons
                                                                            .send,
                                                                        size:
                                                                            45,
                                                                        color: Color(0xFF7806D2)
                                                                            .withOpacity(0.9),
                                                                      ),
                                                                    )
                                                                  : CircularProgressIndicator(
                                                                      backgroundColor:
                                                                          Color(
                                                                              0xFF9C27B0),
                                                                    ),
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  );
                                                },
                                              ),
                                            ));
                                      });
                                },
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Icon(
                                      Icons.comment,
                                      color: Color(0xFF7806D2),
                                    ),
                                    SizedBox(
                                      width: 4,
                                    ),
                                    Text(
                                      article.nb_comments.toString() +
                                          " Comments",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Color(0xFF7806D2)),
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ))
              ],
            ),
          ),
          isLoading: _isLoading,
          color: Colors.white,
          opacity: 0.5,
          progressIndicator: CircularProgressIndicator(
            backgroundColor: Color(0xFF9C27B0),
          ),
        ));
  }
}

class Comment extends StatelessWidget {
  const Comment({
    Key key,
    this.createdAt,
    this.article,
    this.updateAt,
    this.commentator,
    this.content,
  }) : super(key: key);

  final String commentator;
  final String content;
  final String createdAt;
  final String updateAt;

  final String article;

  @override
  Widget build(BuildContext context) {
    //String strDt = this.date;
    // DateTime parseDt = DateTime.parse(strDt);
    //print(parseDt); // 1974-03-20 00:00:00.000
    String strDt = createdAt;
    DateTime parseDt = DateTime.parse(strDt);
    return Container(
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ListTile(
              leading: CircleAvatar(
                backgroundImage: AssetImage('assets/images/doctors/avatar.jpg'),
              ),
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    this.commentator,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
                  ),
                  Text(timeago.format(parseDt),
                      style: TextStyle(
                          fontSize: 12,
                          color: Colors.grey,
                          fontWeight: FontWeight.bold)),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 70),
              decoration: BoxDecoration(
                color: Color(0xFF7806D2).withOpacity(0.05),
                borderRadius: BorderRadius.circular(30.0),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.1),
                    spreadRadius: 0.0,
                    blurRadius: 10.0,
                    offset: Offset(0, 10.0),
                  ),
                ],
              ),
              padding: EdgeInsets.all(10),
              child: Text(
                content ?? '',
                style: TextStyle(fontSize: 15),
              ),
            ),
          ]),
    );
  }
}
