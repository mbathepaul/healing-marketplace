import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:healing/core/models/availibilities.dart';
import 'package:healing/core/models/booking.dart';
import 'package:healing/core/models/comment.dart';
import 'package:healing/core/models/doctor.dart';
import 'package:healing/services/therapist_service.dart';
import 'package:healing/widgets/contact_button_mes.dart';
import 'package:healing/widgets/custom_field.dart';
import 'package:healing/widgets/like_comment.dart';
import 'package:healing/widgets/tag_button.dart';
import 'package:healing/widgets/therapeutist.dart';
import 'package:healing/services/authService.dart';
import 'package:healing/services/CallsAndMessagesService.dart';
import 'package:healing/services/service_locator.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_rounded_date_picker/flutter_rounded_date_picker.dart';
import 'package:flutter_date_pickers/flutter_date_pickers.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:intl/intl.dart';
import 'package:jiffy/jiffy.dart';

class TherapeutistDetails extends StatefulWidget {
  @override
  _TherapeutistDetailsState createState() => _TherapeutistDetailsState();
}

class _TherapeutistDetailsState extends State<TherapeutistDetails> {
  TextEditingController commentController = TextEditingController();
  bool selected = false;

  List<Doctor> doctors = [
    Doctor(
      user: 'Christine Swords',
      description: 'Neurologist',
      phone: '4.0',
      image: 'assets/images/doctors/2.jpg',
    ),
  ];
  bool _isLoading = false;
  List<Comm> reviews = [];
  ScrollController _scrollController = new ScrollController();
  bool in_send = false;
  TextEditingController commentText = TextEditingController();
  List<Meeting> meetings = [];
  List<String> meetings_id = [];

  bool fist_loard = true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  final CallsAndMessagesService _service = locator<CallsAndMessagesService>();
  void _onSelectionChanged(DateRangePickerSelectionChangedArgs args) {
    // TODO: implement your code here
  }
  DateTime start_book;
  DateTime end_book;
  @override
  Widget build(BuildContext context) {
    Doctor doctor = (ModalRoute.of(context).settings.arguments as Doctor);
    List<Comm> comments = doctor.reviews;
    // Create week date picker with passed parameters

    if (fist_loard == true) {
      TherapistService().listAviabilities_id(doctor.id).then((value) {
        if (value != null) {
          List<Meeting> mitt = [];
          for (Availibilitie bo in value) {
            String strst = bo.start_date;
            String endst = bo.end_date;
            DateTime start_da = DateTime.parse(strst);
            DateTime end_da = DateTime.parse(endst);
            Meeting me =
                Meeting('', start_da, end_da, const Color(0xFF0F8644), false);
            mitt.add(me);
            meetings_id.add(bo.id);
          }

          setState(() {
            meetings = mitt;
            fist_loard = false;
          });
        }
      }).catchError((onError) {
        print("une erreurnnnnnnnnnnnnnnnnnnnnnnnnnnnnn");
        print(onError);
      });
    }

    test() {
      print("bonjour");
    }

    print(
        "type........................................................................");

    return Scaffold(
      body: SafeArea(
        child: LoadingOverlay(
          child: SingleChildScrollView(
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Flex(
                direction: Axis.vertical,
                children: [
                  Expanded(
                    flex: 1,
                    child: Container(
                      padding:
                          EdgeInsets.only(left: 20.0, right: 20.0, top: 25.0),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                padding: EdgeInsets.all(10.0),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.grey.shade300,
                                    width: 1.0,
                                    style: BorderStyle.solid,
                                  ),
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                child: GestureDetector(
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                  child: Icon(
                                    Icons.arrow_back_ios,
                                    color: Colors.grey,
                                  ),
                                ),
                              ),
                              Container(
                                child: Text(
                                  doctor.label != null
                                      ? doctor.label
                                      : 'Neurology',
                                  style: TextStyle(
                                    fontFamily: 'Coves Bold',
                                    color: Colors.grey,
                                    fontSize: 20.0,
                                  ),
                                ),
                              ),
                              Container(
                                  child: GestureDetector(
                                onTap: () {
                                  print("bonjour");
                                  TherapistService()
                                      .getTherapistProducts(doctor.id);
                                  Navigator.pushNamed(context, '/product_order',
                                      arguments: doctor.id);
                                },
                                child: Container(
                                  alignment: Alignment.center,
                                  height: 30.0,
                                  width: 80,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30.0),
                                    color: Color(0xFF320A3B),
                                  ),
                                  child: Text(
                                    'Store',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: 'Coves Bold',
                                      fontSize: 19.0,
                                    ),
                                  ),
                                ),
                              )),
                            ],
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Expanded(
                            child: Therapeutist(
                              key: new Key(doctor.id),
                              name: doctor.user,
                              image: doctor.image,
                              rate: doctor.rate,
                              doctor: doctor,
                              clique: false,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      padding: EdgeInsets.only(
                          top: 5.0, left: 20.0, right: 20.0, bottom: 3.0),
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30.0),
                        color: Colors.grey.shade200,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'About The Holistic Provider',
                            style: TextStyle(
                              fontFamily: 'Coves Bold',
                              fontSize: 19.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(
                            doctor.description != null
                                ? doctor.description
                                : '',
                            style: TextStyle(
                                fontFamily: 'Source',
                                letterSpacing: 1.0,
                                wordSpacing: 2.0,
                                fontSize: 13),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Expanded(
                            child: GridView.count(
                              // Create a grid with 2 columns. If you change the scrollDirection to
                              // horizontal, this would produce 2 rows.
                              crossAxisCount: 3,
                              childAspectRatio: 2.5,
                              mainAxisSpacing: 0,
                              crossAxisSpacing: 10,
                              // Generate 100 Widgets that display their index in the List
                              children:
                                  List.generate(doctor.tags.length, (index) {
                                return Center(
                                    child: TagButton(
                                  title: doctor.tags[index],
                                ));
                              }),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              bool book_update = false;
                              showModalBottomSheet(
                                context: context,
                                isScrollControlled: true,
                                isDismissible: true,
                                builder: (BuildContext context) {
                                  return StatefulBuilder(builder:
                                      (BuildContext context,
                                          StateSetter stateSetter) {
                                    return Container(
                                      alignment: Alignment.center,
                                      padding: EdgeInsets.all(10.0),
                                      height: 560.0,
                                      width: MediaQuery.of(context).size.width,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(50.0),
                                          topRight: Radius.circular(50.0),
                                        ),
                                      ),
                                      child: Column(
                                        children: [
                                          SizedBox(
                                            height: 5,
                                          ),
                                          SfCalendar(
                                            view: CalendarView.workWeek,
                                            onTap: (value) {
                                              print(meetings.length);
                                              if (value.appointments != null) {
                                                Meeting m = value
                                                    .appointments[0] as Meeting;
                                                start_book = m.from;
                                                end_book = m.to;
                                                stateSetter(() {
                                                  book_update = true;
                                                });
                                              } else {
                                                stateSetter(() {
                                                  book_update = false;
                                                });
                                              }
                                            },
                                            todayHighlightColor:
                                                Color(0xFF320A3B),
                                            cellBorderColor: Color(0xFF320A3B),
                                            dataSource:
                                                MeetingDataSource(meetings),
                                            // by default the month appointment display mode set as Indicator, we can
                                            // change the display mode as appointment using the appointment display
                                            // mode property
                                            monthViewSettings:
                                                const MonthViewSettings(
                                                    appointmentDisplayMode:
                                                        MonthAppointmentDisplayMode
                                                            .appointment),
                                          ),
                                          SizedBox(
                                            height: 8.0,
                                          ),
                                          if (book_update == true)
                                            Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceAround,
                                                children: [
                                                  Container(
                                                    alignment: Alignment.center,
                                                    height: 20.0,
                                                    decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              0.0),
                                                      color: Colors.blue
                                                          .withOpacity(0.3),
                                                    ),
                                                    child: Text(
                                                      Jiffy([
                                                        start_book.year,
                                                        start_book.month,
                                                        start_book.day
                                                      ]).yMMMMd,
                                                      style: TextStyle(
                                                        fontSize: 12,
                                                        fontFamily:
                                                            'Source SemiBold',
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      ),
                                                    ),
                                                  ),
                                                  Row(
                                                    children: [
                                                      Container(
                                                          alignment:
                                                              Alignment.center,
                                                          height: 20.0,
                                                          width: 80.0,
                                                          decoration:
                                                              BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        0.0),
                                                            color: Colors.blue
                                                                .withOpacity(
                                                                    0.3),
                                                          ),
                                                          child: Text(
                                                            DateFormat.jm()
                                                                    .format(DateTime(
                                                                        start_book
                                                                            .year,
                                                                        start_book
                                                                            .month,
                                                                        start_book
                                                                            .day,
                                                                        start_book
                                                                            .hour,
                                                                        start_book
                                                                            .minute))
                                                                    .toLowerCase() +
                                                                " ",
                                                            style: TextStyle(
                                                              fontSize: 12,
                                                              fontFamily:
                                                                  'Source SemiBold',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                            ),
                                                          )),
                                                      Text(
                                                        " - ",
                                                        style: TextStyle(
                                                          fontSize: 15,
                                                          fontFamily:
                                                              'Source SemiBold',
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                      ),
                                                      Container(
                                                          alignment:
                                                              Alignment.center,
                                                          height: 20.0,
                                                          width: 80.0,
                                                          decoration:
                                                              BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        0.0),
                                                            color: Colors.blue
                                                                .withOpacity(
                                                                    0.3),
                                                          ),
                                                          child: Text(
                                                            DateFormat.jm()
                                                                    .format(DateTime(
                                                                        end_book
                                                                            .year,
                                                                        end_book
                                                                            .month,
                                                                        end_book
                                                                            .day,
                                                                        end_book
                                                                            .hour,
                                                                        end_book
                                                                            .minute))
                                                                    .toLowerCase() +
                                                                " ",
                                                            style: TextStyle(
                                                              fontSize: 12,
                                                              fontFamily:
                                                                  'Source SemiBold',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                            ),
                                                          ))
                                                    ],
                                                  ),
                                                ]),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          if (book_update == true)
                                            GestureDetector(
                                              onTap: () async {
                                                AuthService()
                                                    .bookinAdd(
                                                  doctor.id,
                                                  start_book.toString(),
                                                  end_book.toString(),
                                                )
                                                    .then((value) {
                                                  stateSetter(() {
                                                    book_update = false;
                                                  });
                                                  if (value) {
                                                    Fluttertoast.showToast(
                                                      msg:
                                                          'Booking successful will be notified by mail when service provider confirms booking!',
                                                      toastLength:
                                                          Toast.LENGTH_LONG,
                                                      gravity:
                                                          ToastGravity.BOTTOM,
                                                      backgroundColor:
                                                          Color(0xFF011424),
                                                      textColor: Colors.white,
                                                      fontSize: 15.0,
                                                    );
                                                  } else {
                                                    Fluttertoast.showToast(
                                                      msg:
                                                          'An error occured during booking',
                                                      toastLength:
                                                          Toast.LENGTH_SHORT,
                                                      gravity:
                                                          ToastGravity.BOTTOM,
                                                      backgroundColor:
                                                          Color(0xFF011424),
                                                      textColor: Colors.white,
                                                      fontSize: 15.0,
                                                    );
                                                    print(
                                                        "comment faire la guerre");
                                                  }
                                                });
                                              },
                                              child: Container(
                                                alignment: Alignment.center,
                                                height: 35.0,
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width -
                                                    100,
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          30.0),
                                                  color: Color(0xFF320A3B),
                                                ),
                                                child: Text(
                                                  'Book session',
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontFamily: 'Coves Bold',
                                                    fontSize: 18.0,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          Divider(
                                            height: 5,
                                            endIndent: 5,
                                            color: Colors.black,
                                          ),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          Text(
                                            "Direct line booking",
                                            style: TextStyle(
                                                fontSize: 16,
                                                fontFamily: 'Source SemiBold',
                                                fontWeight: FontWeight.bold),
                                          ),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          Text(
                                            "Do Mention heeling market place when contacting provider",
                                            style: TextStyle(
                                              fontSize: 12,
                                              fontFamily: 'Source SemiBold',
                                            ),
                                          ),
                                          SizedBox(
                                            height: 25,
                                          ),
                                          Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceAround,
                                            children: [
                                              Expanded(
                                                  child: ContactBookingButton(
                                                payload:
                                                    doctor.phone.toString(),
                                              )),
                                              Expanded(
                                                child: ContactBookingButton(
                                                  title: 'Message',
                                                  payload:
                                                      doctor.phone.toString(),
                                                  icon: Icon(
                                                    Icons.message,
                                                    color: Colors.white,
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                child: ContactBookingButton(
                                                  title: 'Mail',
                                                  payload:
                                                      doctor.email.toString(),
                                                  icon: Icon(
                                                    Icons.mail,
                                                    color: Colors.white,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    );
                                  });
                                },
                              );
                            },
                            child: Container(
                              alignment: Alignment.center,
                              height: 35.0,
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30.0),
                                color: Color(0xFF320A3B),
                              ),
                              child: Text(
                                'Book Now',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'Coves Bold',
                                  fontSize: 18.0,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      padding: EdgeInsets.all(20.0),
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      child: ListView(
                        children: [
                          GestureDetector(
                            onTap: () {
                              /*
                              print('clic on the button add comment');
                              showModalBottomSheet(
                                context: context,
                                builder: (BuildContext context) {
                                  return Container(
                                    padding: EdgeInsets.all(20.0),
                                    height: 200.0,
                                    width: MediaQuery.of(context).size.width,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(50.0),
                                        topRight: Radius.circular(50.0),
                                      ),
                                    ),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        CustomField(
                                          label: 'Add comment',
                                          helper: 'Add your comment here...',
                                          bottomMargin: 10.0,
                                          type: TextInputType.text,
                                          controller: commentController,
                                          actionCallback: () {
                                            print('Fill the field');
                                          },
                                        ),
                                        SizedBox(
                                          height: 10.0,
                                        ),
                                        GestureDetector(
                                          onTap: () async {
                                            if (AuthService.is_therapist ==
                                                false) {
                                              setState(() {
                                                _isLoading = true;
                                              });
                                              AuthService()
                                                  .addComment(
                                                      doctor.id.toString(),
                                                      commentController.text)
                                                  .then((value) {
                                                if (value) {
                                                  setState(() {
                                                    _isLoading = false;
                                                    // reviews.add(
                                                    //  commentController.text);
                                                  });

                                                  Fluttertoast.showToast(
                                                    msg:
                                                        'Comment add successfully!',
                                                    toastLength:
                                                        Toast.LENGTH_SHORT,
                                                    gravity:
                                                        ToastGravity.BOTTOM,
                                                    backgroundColor:
                                                        Color(0xFF011424),
                                                    textColor: Colors.white,
                                                    fontSize: 15.0,
                                                  );
                                                  print(
                                                      "comment faire la guerre");
                                                } else {
                                                  setState(() {
                                                    _isLoading = false;
                                                  });
                                                  Fluttertoast.showToast(
                                                    msg:
                                                        'An error occured during adding the comment',
                                                    toastLength:
                                                        Toast.LENGTH_SHORT,
                                                    gravity:
                                                        ToastGravity.BOTTOM,
                                                    backgroundColor:
                                                        Color(0xFF011424),
                                                    textColor: Colors.white,
                                                    fontSize: 15.0,
                                                  );
                                                  print("ça n'a pas marché");
                                                }
                                              });

                                              Navigator.pop(context);
                                            } else {
                                              Fluttertoast.showToast(
                                                msg:
                                                    'Only customers can add comments',
                                                toastLength: Toast.LENGTH_SHORT,
                                                gravity: ToastGravity.BOTTOM,
                                                backgroundColor: Colors.red,
                                                textColor: Colors.white,
                                                fontSize: 13.0,
                                              );
                                              print("ça n'a pas marché");
                                            }
                                          },
                                          child: Container(
                                            alignment: Alignment.center,
                                            height: 50.0,
                                            width: 100.0,
                                            decoration: BoxDecoration(
                                              color: Color(0xFF320A3B),
                                            ),
                                            child: Text(
                                              'Add',
                                              style: TextStyle(
                                                fontFamily: 'Coves Bold',
                                                color: Colors.white,
                                                fontSize: 15.0,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                },
                              );
                              */
                              showModalBottomSheet(
                                  context: context,
                                  isScrollControlled: true,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.vertical(
                                          top: Radius.circular(25.0))),
                                  builder: (context) {
                                    return FractionallySizedBox(
                                        heightFactor: 0.9,
                                        child: Container(
                                          padding: EdgeInsets.only(
                                              left: 0.0, top: 20, right: 20),
                                          height: 500.0,
                                          width:
                                              MediaQuery.of(context).size.width,
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(50.0),
                                              topRight: Radius.circular(50.0),
                                            ),
                                          ),
                                          child: StatefulBuilder(
                                            builder: (BuildContext context,
                                                StateSetter stateSetter) {
                                              return Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Expanded(
                                                      child: ListView.builder(
                                                          itemCount:
                                                              comments.length,
                                                          shrinkWrap: true,
                                                          controller:
                                                              _scrollController,
                                                          itemBuilder:
                                                              (context, index) {
                                                            return Comment(
                                                                commentator: doctor
                                                                    .reviews[
                                                                        index]
                                                                    .commentator,
                                                                createdAt: doctor
                                                                    .reviews[
                                                                        index]
                                                                    .createdAt,
                                                                updateAt: doctor
                                                                    .reviews[
                                                                        index]
                                                                    .updateAt,
                                                                content: doctor
                                                                    .reviews[
                                                                        index]
                                                                    .content,
                                                                article: doctor
                                                                    .reviews[
                                                                        index]
                                                                    .article);
                                                          })),
                                                  Padding(
                                                    padding: EdgeInsets.only(
                                                        left: 20, right: 0),
                                                    child: Container(
                                                      height: 65,
                                                      width:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width -
                                                              20,
                                                      padding:
                                                          EdgeInsets.symmetric(
                                                              horizontal: 15.0,
                                                              vertical: 10.0),
                                                      margin: EdgeInsets.only(
                                                          bottom: 20),
                                                      decoration: BoxDecoration(
                                                        color: Colors.white,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(20.0),
                                                        border: Border.all(
                                                            color: Colors.white
                                                                .withOpacity(
                                                                    0.5),
                                                            width: 1.5),
                                                        boxShadow: [
                                                          BoxShadow(
                                                            color: Colors.black
                                                                .withOpacity(
                                                                    0.2),
                                                            spreadRadius: 0.0,
                                                            blurRadius: 10.0,
                                                            offset:
                                                                Offset(0, 8.0),
                                                          ),
                                                        ],
                                                      ),
                                                      child: Row(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                          Expanded(
                                                            child: TextField(
                                                              controller:
                                                                  commentText,
                                                              keyboardType:
                                                                  TextInputType
                                                                      .text,
                                                              maxLines: 2,
                                                              obscureText:
                                                                  false,
                                                              style: TextStyle(
                                                                fontFamily:
                                                                    'Source SemiBold',
                                                                fontSize: 18.0,
                                                                color: Color(
                                                                        0xFF474E6C)
                                                                    .withOpacity(
                                                                        0.8),
                                                              ),
                                                              decoration:
                                                                  InputDecoration(
                                                                isDense: true,
                                                                floatingLabelBehavior:
                                                                    FloatingLabelBehavior
                                                                        .never,
                                                                contentPadding:
                                                                    EdgeInsets
                                                                        .fromLTRB(
                                                                            0,
                                                                            0,
                                                                            0,
                                                                            0.0),
                                                                labelText:
                                                                    "Add your comment here...",
                                                                border:
                                                                    OutlineInputBorder(
                                                                  borderSide:
                                                                      BorderSide(
                                                                          color:
                                                                              Colors.transparent),
                                                                ),
                                                                focusedBorder:
                                                                    OutlineInputBorder(
                                                                  borderSide:
                                                                      BorderSide(
                                                                          color:
                                                                              Colors.transparent),
                                                                ),
                                                                enabledBorder:
                                                                    OutlineInputBorder(
                                                                  borderSide:
                                                                      BorderSide(
                                                                          color:
                                                                              Colors.transparent),
                                                                ),
                                                                labelStyle:
                                                                    TextStyle(
                                                                  fontFamily:
                                                                      'Source SemiBold',
                                                                  fontSize:
                                                                      12.0,
                                                                  color: Color(
                                                                          0xFF474E6C)
                                                                      .withOpacity(
                                                                          0.5),
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          in_send == false
                                                              ? GestureDetector(
                                                                  onTap:
                                                                      () async {
                                                                    print(
                                                                        "send");
                                                                    if (AuthService.is_therapist ==
                                                                            false &&
                                                                        AuthService.token !=
                                                                            null) {
                                                                      stateSetter(() =>
                                                                          in_send =
                                                                              true);
                                                                      AuthService()
                                                                          .addComment(
                                                                              doctor.id.toString(),
                                                                              commentText.text)
                                                                          .then((value) {
                                                                        if (value) {
                                                                          stateSetter(
                                                                              () {
                                                                            in_send =
                                                                                false;

                                                                            Comm co = Comm(
                                                                                article: AuthService.username,
                                                                                commentator: AuthService.username,
                                                                                updateAt: DateTime.now().toString(),
                                                                                createdAt: DateTime.now().toString(),
                                                                                content: commentText.text);
                                                                            comments.add(co);
                                                                            commentText.text =
                                                                                "";
                                                                          });
                                                                          _scrollController
                                                                              .animateTo(
                                                                            _scrollController.position.maxScrollExtent +
                                                                                5,
                                                                            duration:
                                                                                const Duration(milliseconds: 100),
                                                                            curve:
                                                                                Curves.fastOutSlowIn,
                                                                          );
                                                                          Fluttertoast
                                                                              .showToast(
                                                                            msg:
                                                                                'Review add successfully!',
                                                                            toastLength:
                                                                                Toast.LENGTH_SHORT,
                                                                            gravity:
                                                                                ToastGravity.BOTTOM,
                                                                            backgroundColor:
                                                                                Color(0xFF011424),
                                                                            textColor:
                                                                                Colors.white,
                                                                            fontSize:
                                                                                15.0,
                                                                          );
                                                                          print(
                                                                              "comment faire la guerre");
                                                                        } else {
                                                                          stateSetter(() =>
                                                                              in_send = false);
                                                                          Fluttertoast
                                                                              .showToast(
                                                                            msg:
                                                                                'An error occured during adding the comment',
                                                                            toastLength:
                                                                                Toast.LENGTH_SHORT,
                                                                            gravity:
                                                                                ToastGravity.BOTTOM,
                                                                            backgroundColor:
                                                                                Color(0xFF011424),
                                                                            textColor:
                                                                                Colors.white,
                                                                            fontSize:
                                                                                15.0,
                                                                          );
                                                                          print(
                                                                              "ça n'a pas marché");
                                                                        }
                                                                      });
                                                                    } else {
                                                                      print(
                                                                          "comment faire la guerre en algerie");
                                                                      if (AuthService
                                                                              .token ==
                                                                          null) {
                                                                        Fluttertoast
                                                                            .showToast(
                                                                          msg:
                                                                              'you must log in before leaving review',
                                                                          toastLength:
                                                                              Toast.LENGTH_SHORT,
                                                                          gravity:
                                                                              ToastGravity.BOTTOM,
                                                                          backgroundColor:
                                                                              Colors.red,
                                                                          textColor:
                                                                              Colors.white,
                                                                          fontSize:
                                                                              13.0,
                                                                        );
                                                                      } else {
                                                                        Fluttertoast
                                                                            .showToast(
                                                                          msg:
                                                                              'Only customers can leave review',
                                                                          toastLength:
                                                                              Toast.LENGTH_SHORT,
                                                                          gravity:
                                                                              ToastGravity.BOTTOM,
                                                                          backgroundColor:
                                                                              Colors.red,
                                                                          textColor:
                                                                              Colors.white,
                                                                          fontSize:
                                                                              13.0,
                                                                        );
                                                                      }
                                                                    }
                                                                  },
                                                                  child: Icon(
                                                                    Icons.send,
                                                                    size: 45,
                                                                    color: Color(
                                                                            0xFF320A3B)
                                                                        .withOpacity(
                                                                            0.9),
                                                                  ),
                                                                )
                                                              : CircularProgressIndicator(
                                                                  backgroundColor:
                                                                      Color(
                                                                          0xFF320A3B),
                                                                ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              );
                                            },
                                          ),
                                        ));
                                  });
                            },
                            child: Container(
                              height: 40.0,
                              width: 50.0,
                              padding: EdgeInsets.symmetric(horizontal: 30),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(100.0),
                                color: Color(0xFF320A3B),
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Text(
                                    doctor.reviews.length.toString() +
                                        " reviews",
                                    style: TextStyle(
                                      fontFamily: 'Coves Bold',
                                      color: Colors.white,
                                      fontSize: 15,
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      Icon(Icons.add, color: Colors.white),
                                      Text(
                                        ' Leave review',
                                        style: TextStyle(
                                          fontFamily: 'Coves Bold',
                                          color: Colors.white,
                                          fontSize: 19,
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          isLoading: _isLoading,
          color: Colors.white,
          opacity: 0.5,
          progressIndicator: CircularProgressIndicator(
            backgroundColor: Color(0xFF9C27B0),
          ),
        ),
      ),
    );
  }

  List<Meeting> _getDataSource() {
    final List<Meeting> meetings = <Meeting>[];
    final DateTime today = DateTime.now();
    final DateTime startTime =
        DateTime(today.year, today.month, today.day, 9, 0, 0);
    final DateTime endTime = startTime.add(const Duration(hours: 2));
    meetings.add(Meeting(
        'Conference', startTime, endTime, const Color(0xFF0F8644), false));
    return meetings;
  }
}

class Comment extends StatelessWidget {
  const Comment({
    Key key,
    this.createdAt,
    this.article,
    this.updateAt,
    this.commentator,
    this.content,
  }) : super(key: key);

  final String commentator;
  final String content;
  final String createdAt;
  final String updateAt;

  final String article;

  @override
  Widget build(BuildContext context) {
    //String strDt = this.date;
    // DateTime parseDt = DateTime.parse(strDt);
    //print(parseDt); // 1974-03-20 00:00:00.000
    String strDt = createdAt;
    DateTime parseDt = DateTime.parse(strDt);
    return Container(
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ListTile(
              leading: CircleAvatar(
                backgroundImage: AssetImage('assets/images/doctors/avatar.jpg'),
              ),
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    this.commentator,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
                  ),
                  Text(timeago.format(parseDt),
                      style: TextStyle(
                          fontSize: 12,
                          color: Colors.grey,
                          fontWeight: FontWeight.bold)),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 70),
              decoration: BoxDecoration(
                color: Color(0xFF320A3B).withOpacity(0.05),
                borderRadius: BorderRadius.circular(30.0),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.1),
                    spreadRadius: 0.0,
                    blurRadius: 10.0,
                    offset: Offset(0, 10.0),
                  ),
                ],
              ),
              padding: EdgeInsets.all(10),
              child: Text(
                content ?? '',
                style: TextStyle(fontSize: 15),
              ),
            ),
          ]),
    );
  }
}

/// allows to add, remove or reset the appointment collection.
class MeetingDataSource extends CalendarDataSource {
  /// Creates a meeting data source, which used to set the appointment
  /// collection to the calendar
  MeetingDataSource(List<Meeting> source) {
    appointments = source;
  }

  @override
  DateTime getStartTime(int index) {
    return _getMeetingData(index).from;
  }

  @override
  DateTime getEndTime(int index) {
    return _getMeetingData(index).to;
  }

  @override
  String getSubject(int index) {
    return _getMeetingData(index).eventName;
  }

  @override
  Color getColor(int index) {
    return _getMeetingData(index).background;
  }

  @override
  bool isAllDay(int index) {
    return _getMeetingData(index).isAllDay;
  }

  Meeting _getMeetingData(int index) {
    final dynamic meeting = appointments[index];
    Meeting meetingData;
    if (meeting is Meeting) {
      meetingData = meeting;
    }

    return meetingData;
  }
}

/// Custom business object class which contains properties to hold the detailed
/// information about the event data which will be rendered in calendar.
class Meeting {
  /// Creates a meeting class with required details.
  Meeting(this.eventName, this.from, this.to, this.background, this.isAllDay);

  /// Event name which is equivalent to subject property of [Appointment].
  String eventName;

  /// From which is equivalent to start time property of [Appointment].
  DateTime from;

  /// To which is equivalent to end time property of [Appointment].
  DateTime to;

  /// Background which is equivalent to color property of [Appointment].
  Color background;

  /// IsAllDay which is equivalent to isAllDay property of [Appointment].
  bool isAllDay;
}
