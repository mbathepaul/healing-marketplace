import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:healing/core/models/booking.dart';
import 'package:healing/screens/auth/login.dart';
import 'package:healing/services/payment-service.dart';
import 'package:healing/services/therapist_service.dart';
import 'package:healing/widgets/custom_field.dart';
import 'package:healing/widgets/therapeutist.dart';
import 'package:image_picker/image_picker.dart';
import 'package:healing/services/authService.dart';
import 'package:healing/widgets/custom_button.dart';
import 'package:healing/widgets/custom_field.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:dropdownfield/dropdownfield.dart';
import 'package:jiffy/jiffy.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:flutter_rounded_date_picker/flutter_rounded_date_picker.dart';
import 'package:flutter_date_pickers/flutter_date_pickers.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

class Booking extends StatefulWidget {
  @override
  _BookingState createState() => _BookingState();
}

class _BookingState extends State<Booking> {
  int group = 1;

  int _contimage = 0;
  bool _isLoading = false;
  static String id_sign_up;

  int _currentSteps = 0;

  bool typeclient = false;
  bool typetherapist = true;

  Position _currentPosition;
  String lat = "";
  String long = "";
  String cathegory_id = "";
  List<String> cathegorylist = [];
  bool _isadd = false;

  List<String> names = [
    "Victor IIome",
    "John Mc Laren",
    "Lucas scharlk",
    "Kamdem Dieunedor",
    "Lonlat Matias",
    "Ewane Edigue",
    "Bobdou Lomba",
    "NDI Forchan",
    "MBIM PNIT",
    "Bobdou Lomba",
  ];
  List<Booki> bookings = [];
  bool charing = true;

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
    TherapistService().bookingList().then((value) {
      setState(() {
        charing = false;

        bookings = new List.from(value.reversed);
      });
    });
  }

  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  bool isSwitched = false;
  var textValue = 'Switch is OFF';

  void toggleSwitch(bool value) {
    if (isSwitched == false) {
      setState(() {
        isSwitched = true;
        textValue = 'Switch Button is ON';
      });
      print('Switch Button is ON');
    } else {
      setState(() {
        isSwitched = false;
        textValue = 'Switch Button is OFF';
      });
      print('Switch Button is OFF');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LoadingOverlay(
        child: SafeArea(
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
            decoration: BoxDecoration(
              color: Colors.white,
              image: DecorationImage(
                image: AssetImage('assets/images/background.jpg'),
                fit: BoxFit.fill,
              ),
            ),
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Container(
                            alignment: Alignment.center,
                            height: 35.0,
                            width: 47,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5.0),
                              color: Color(0xFFB247BC).withOpacity(0.18),
                            ),
                            child: Icon(
                              Icons.arrow_back,
                              color: Color(0xFF320A3B),
                              size: 25,
                            ),
                          ),
                        ),
                        Container(
                            alignment: Alignment.center,
                            height: 35.0,
                            width: 180,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30.0),
                              color: Color(0xFF320A3B),
                            ),
                            child: GestureDetector(
                              onTap: () async {
                                Navigator.pushNamed(context, '/calandar');
                              },
                              child: Text(
                                'Shedule calendar',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'Coves Bold',
                                  fontSize: 18.0,
                                ),
                              ),
                            )),
                      ],
                    ),
                  ),
                  SizedBox(height: 40.0),
                  Center(
                    child: Text(
                      'Bookings',
                      style: TextStyle(
                        color: Color(0xFF320A3B),
                        fontFamily: 'Coves Bold',
                        fontSize: 28.0,
                      ),
                    ),
                  ),
                  SizedBox(height: 30.0),
                  if (charing == false)
                    Expanded(
                        child: ListView.builder(
                      itemCount: bookings.length,
                      itemBuilder: (BuildContext context, int index) {
                        String strDt = bookings[index].start_date;
                        DateTime parseDt = DateTime.parse(strDt);

                        return Container(
                          padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                          color: (index % 2 == 0)
                              ? Color(0xFFB247BC).withOpacity(0.09)
                              : Color(0xFFB247BC).withOpacity(0.18),
                          child: ListTile(
                              title: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                bookings[index].booker,
                                style: TextStyle(
                                  color: Color(0xFF320A3B),
                                  fontFamily: 'Coves Bold',
                                  fontSize: 17.0,
                                ),
                              ),
                              Text(
                                Jiffy([
                                  parseDt.year,
                                  parseDt.month,
                                  parseDt.day
                                ]).yMMMMd,
                                style: TextStyle(
                                  color: Color(0xFF320A3B),
                                  fontFamily: 'Coves Bold',
                                  fontSize: 13.0,
                                ),
                              ),
                              Row(
                                children: [
                                  if (bookings[index].status == 0)
                                    Text(
                                      "accept",
                                      style: TextStyle(
                                        color: Color(0xFF320A3B),
                                        fontFamily: 'Coves Bold',
                                        fontSize: 15.0,
                                      ),
                                    ),
                                  if (bookings[index].status == 1)
                                    Text(
                                      "reject",
                                      style: TextStyle(
                                        color: Color(0xFF320A3B),
                                        fontFamily: 'Coves Bold',
                                        fontSize: 15.0,
                                      ),
                                    ),
                                  Switch(
                                    onChanged: (value) {
                                      int statu = bookings[index].status;
                                      statu == 1 ? statu = 0 : statu = 1;
                                      if (statu == 1) {
                                        TherapistService()
                                            .update_booking(bookings[index].id)
                                            .then((value) {
                                          if (value) {
                                            Fluttertoast.showToast(
                                              msg:
                                                  'Booking accept successfully!!',
                                              toastLength: Toast.LENGTH_SHORT,
                                              gravity: ToastGravity.BOTTOM,
                                              backgroundColor:
                                                  Color(0xFF011424),
                                              textColor: Colors.white,
                                              fontSize: 15.0,
                                            );
                                            setState(() {
                                              bookings[index].status = statu;
                                            });
                                          } else {
                                            Fluttertoast.showToast(
                                              msg:
                                                  'An error occured during booking',
                                              toastLength: Toast.LENGTH_SHORT,
                                              gravity: ToastGravity.BOTTOM,
                                              backgroundColor:
                                                  Color(0xFF011424),
                                              textColor: Colors.white,
                                              fontSize: 15.0,
                                            );
                                            print("comment faire la guerre");
                                          }
                                        });
                                      } else {
                                        TherapistService()
                                            .update_booking_rejet(
                                                bookings[index].id)
                                            .then((value) {
                                          if (value) {
                                            Fluttertoast.showToast(
                                              msg:
                                                  'Booking reject successfully!!',
                                              toastLength: Toast.LENGTH_SHORT,
                                              gravity: ToastGravity.BOTTOM,
                                              backgroundColor:
                                                  Color(0xFF011424),
                                              textColor: Colors.white,
                                              fontSize: 15.0,
                                            );
                                            setState(() {
                                              bookings[index].status = statu;
                                            });
                                          } else {
                                            Fluttertoast.showToast(
                                              msg:
                                                  'An error occured during booking',
                                              toastLength: Toast.LENGTH_SHORT,
                                              gravity: ToastGravity.BOTTOM,
                                              backgroundColor:
                                                  Color(0xFF011424),
                                              textColor: Colors.white,
                                              fontSize: 15.0,
                                            );
                                            print("comment faire la guerre");
                                          }
                                        });
                                      }
                                    },
                                    value: bookings[index].status == 1,
                                    activeColor: Colors.blue,
                                    activeTrackColor: Colors.yellow,
                                    inactiveThumbColor: Colors.redAccent,
                                    inactiveTrackColor: Colors.orange,
                                  )
                                ],
                              )
                            ],
                          )),
                        );
                      },
                    )),
                  if (charing == true)
                    Center(
                      child: CircularProgressIndicator(
                        backgroundColor: Color(0xFF320A3B),
                      ),
                    )
                ],
              ),
            ),
          ),
        ),
        isLoading: _isLoading,
        color: Colors.white,
        opacity: 0.5,
        progressIndicator: CircularProgressIndicator(
          backgroundColor: Color(0xFF9C27B0),
        ),
      ),
    );
  }
}
