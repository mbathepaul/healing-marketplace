import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:healing/services/payment-service.dart';
import 'package:healing/services/therapist_service.dart';
import 'package:healing/widgets/custom_field.dart';
import 'package:healing/widgets/therapeutist.dart';
import 'package:image_picker/image_picker.dart';
import 'package:healing/services/authService.dart';
import 'package:healing/widgets/custom_button.dart';
import 'package:healing/widgets/custom_field.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:dropdownfield/dropdownfield.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:email_validator/email_validator.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  File _image;
  int group = 1;
  List<File> images = [];
  int _contimage = 0;
  bool _isLoading = false;
  static String id_sign_up;

  int _currentSteps = 0;
  TextEditingController businessName = TextEditingController();
  TextEditingController businessDescription = TextEditingController();
  TextEditingController zipCode = TextEditingController();
  TextEditingController tags = TextEditingController();
  TextEditingController mainCategory = TextEditingController();

  TextEditingController username = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController password1 = TextEditingController();
  TextEditingController password2 = TextEditingController();
  TextEditingController tag = TextEditingController();

  bool typeclient = false;
  bool typetherapist = true;

  TextEditingController label = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController region = TextEditingController();
  TextEditingController city = TextEditingController();
  TextEditingController description = TextEditingController();
  TextEditingController facebook = TextEditingController();
  TextEditingController instagram = TextEditingController();
  TextEditingController website = TextEditingController();

  Position _currentPosition;
  String lat = "";
  String long = "";
  String cathegory_id = "";
  List<String> cathegorylist = [];
  bool _isadd = false;

  final picker = ImagePicker();

  Future getImage() async {
    PickedFile pickedFile = await picker.getImage(source: ImageSource.gallery);
    File image = File(pickedFile.path);
    setState(() {
      _isLoading = true;
    });
    await AuthService().uploadfile(image).then(
      (value) {
        if (value) {
          setState(() {
            _isLoading = false;
          });
          setState(
            () {
              if (pickedFile != null) {
                images.add(image);
              } else {
                print('no image selected');
              }
            },
          );
          Fluttertoast.showToast(
            msg: 'Image successfully added!',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            backgroundColor: Colors.green,
            textColor: Colors.white,
            fontSize: 12.0,
          );
        } else {
          setState(() {
            _isLoading = false;
          });
          Fluttertoast.showToast(
            msg: 'An error occured during downloading',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 13.0,
          );
        }
      },
    );
    setState(() {
      _isLoading = false;
    });
  }

  Future getImagelogo() async {
    PickedFile pickedFile = await picker.getImage(source: ImageSource.gallery);
    File image = File(pickedFile.path);
    setState(() {
      _isLoading = true;
    });
    await AuthService().uploadfile(image).then(
      (value) {
        if (value) {
          setState(() {
            _isLoading = false;
          });
          setState(
            () {
              if (pickedFile != null) {
                _image = image;
              } else {
                print('no image selected');
              }
            },
          );
          Fluttertoast.showToast(
            msg: 'Image successfully added!',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            backgroundColor: Colors.green,
            textColor: Colors.white,
            fontSize: 13.0,
          );
        } else {
          setState(() {
            _isLoading = false;
          });
          Fluttertoast.showToast(
            msg: 'An error occured during downloading',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 13.0,
          );
        }
      },
    );
    setState(() {
      _isLoading = false;
    });
  }

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
    StripeService.init();
    TherapistService().getcathegories().then((value) => {
          setState(() {
            cathegorylist = value.cast<String>();
          })
        });
  }

  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LoadingOverlay(
        child: SafeArea(
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 20.0),
            decoration: BoxDecoration(
              color: Colors.white,
              image: DecorationImage(
                image: AssetImage('assets/images/background.jpg'),
                fit: BoxFit.fill,
              ),
            ),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Icon(
                      Icons.arrow_back,
                      color: Colors.grey,
                      size: 30,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Image.asset('assets/images/logo.jgp',
                          width: 32.0, height: 32.0),
                      Text(
                        'Sign up',
                        style: TextStyle(
                          fontFamily: 'Coves Bold',
                          fontSize: 36.0,
                          color: Color(0xFF011424),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10.0),
                  SizedBox(
                    width: (MediaQuery.of(context).size.width / 2) + 100.0,
                    child: Text(
                      'Welcome on The Healing Marketplace. \nPlease fill in the information below to create your account.',
                      style: TextStyle(
                        fontFamily: 'Source',
                        fontSize: 18.0,
                        color: Color(0xFF011424),
                      ),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    child: Container(
                      padding: EdgeInsets.all(20.0),
                      child: Theme(
                        data: ThemeData(
                          primaryColor: Color(0xFF320A3B),
                        ),
                        child: Stepper(
                          type: StepperType.horizontal,
                          steps: _steps(),
                          currentStep: this._currentSteps,
                          onStepTapped: (step) async {
                            if (step == 1) {
                              print("comment faire");
                            }

                            setState(() {
                              this._currentSteps = step;
                            });
                          },
                          onStepContinue: () async {
                            print(this._currentSteps);
                            if (this._currentSteps < this._steps().length - 1) {
                              print('object');

                              if (this._currentSteps == 0) {
                                print("valeur du text en entrée");
                                print("email........................" +
                                    email.text);
                                print(EmailValidator.validate(email.text));
                                final bool isValid =
                                    EmailValidator.validate(email.text);
                                if (!isValid) {
                                  Fluttertoast.showToast(
                                    msg: 'Invalid email address',
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.BOTTOM,
                                    backgroundColor: Color(0xFF011424),
                                    textColor: Colors.white,
                                    fontSize: 15.0,
                                  );
                                } else {
                                  if (username.text != "" &&
                                      email.text != "" &&
                                      password1.text != "" &&
                                      password2.text != "") {
                                    setState(() {
                                      _isLoading = true;
                                    });
                                    await AuthService()
                                        .register(
                                            username.text,
                                            email.text,
                                            password1.text,
                                            password2.text,
                                            typeclient.toString(),
                                            typetherapist.toString())
                                        .then(
                                      (value) {
                                        if (value == true) {
                                          Fluttertoast.showToast(
                                            msg:
                                                'Account created successfully!',
                                            toastLength: Toast.LENGTH_SHORT,
                                            gravity: ToastGravity.BOTTOM,
                                            backgroundColor: Colors.green,
                                            textColor: Colors.white,
                                            fontSize: 13.0,
                                          );
                                          setState(() {
                                            _isLoading = false;
                                          });

                                          setState(() {
                                            this._currentSteps =
                                                this._currentSteps + 1;
                                          });
                                          // Navigator.pushNamed(context, '/login');
                                        } else {
                                          setState(() {
                                            _isLoading = false;
                                          });
                                          Fluttertoast.showToast(
                                            msg: value.toString(),
                                            toastLength: Toast.LENGTH_SHORT,
                                            gravity: ToastGravity.BOTTOM,
                                            backgroundColor: Colors.red,
                                            textColor: Colors.white,
                                            fontSize: 13.0,
                                          );
                                        }
                                      },
                                    );
                                  } else {
                                    Fluttertoast.showToast(
                                      msg: 'All fields are required',
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.BOTTOM,
                                      backgroundColor: Colors.red,
                                      textColor: Colors.white,
                                      fontSize: 12.0,
                                    );
                                  }
                                }
                              } else if (this._currentSteps == 1) {
                                geolocator
                                    .isLocationServiceEnabled()
                                    .then((val) {
                                  if (!val) {
                                    showCupertinoDialog(
                                      context: context,
                                      builder: (context) {
                                        return Theme(
                                            data: ThemeData(
                                                dialogBackgroundColor:
                                                    Colors.black,
                                                dialogTheme: DialogTheme(
                                                    backgroundColor:
                                                        Colors.black)),
                                            child: Dialog(
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                              ),
                                              elevation: 0,
                                              backgroundColor:
                                                  Colors.transparent,
                                              child: Stack(
                                                children: <Widget>[
                                                  Container(
                                                    padding: EdgeInsets.only(
                                                        left: 25,
                                                        top: 20,
                                                        right: 25,
                                                        bottom: 20),
                                                    margin: EdgeInsets.only(
                                                        top: 45),
                                                    decoration: BoxDecoration(
                                                        shape:
                                                            BoxShape.rectangle,
                                                        color: Colors.white,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(20),
                                                        boxShadow: [
                                                          BoxShadow(
                                                              color:
                                                                  Colors.black,
                                                              offset:
                                                                  Offset(0, 10),
                                                              blurRadius: 10),
                                                        ]),
                                                    child: Column(
                                                      mainAxisSize:
                                                          MainAxisSize.min,
                                                      children: <Widget>[
                                                        Text(
                                                          "To continue, you must turn on the device location, which uses Google's location service",
                                                          style: TextStyle(
                                                              fontSize: 18,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600),
                                                        ),
                                                        SizedBox(
                                                          height: 30,
                                                        ),
                                                        Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          children: [
                                                            GestureDetector(
                                                              onTap: () {
                                                                Navigator.of(
                                                                        context)
                                                                    .pop();
                                                              },
                                                              child: Text(
                                                                "No THANKS",
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                      .green,
                                                                  fontFamily:
                                                                      'Source SemiBold',
                                                                  fontSize:
                                                                      17.0,
                                                                ),
                                                              ),
                                                            ),
                                                            GestureDetector(
                                                              onTap: () {
                                                                Navigator.of(
                                                                        context)
                                                                    .pop();
                                                              },
                                                              child: Text(
                                                                "OK",
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                      .green,
                                                                  fontFamily:
                                                                      'Source SemiBold',
                                                                  fontSize:
                                                                      17.0,
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                        SizedBox(
                                                          height: 8,
                                                        ),
                                                      ],
                                                    ),
                                                  ), // bottom part
                                                  // top part
                                                ],
                                              ),
                                            ));
                                      },
                                    );
                                  } else {
                                    setState(() {
                                      _isLoading = true;
                                    });
                                    geolocator
                                        .getCurrentPosition(
                                            desiredAccuracy:
                                                LocationAccuracy.best)
                                        .then((Position position) async {
                                      _currentPosition = position;
                                      lat =
                                          _currentPosition.latitude.toString();
                                      long =
                                          _currentPosition.longitude.toString();
                                      if (cathegory_id != "" &&
                                          address.text != "" &&
                                          phone.text != "" &&
                                          region.text != "" &&
                                          city.text != "" &&
                                          description.text != "" &&
                                          tag.text != "") {
                                        await AuthService()
                                            .registerTherapist2(
                                                cathegory_id,
                                                address.text,
                                                phone.text,
                                                region.text,
                                                city.text,
                                                description.text,
                                                facebook.text,
                                                instagram.text,
                                                website.text,
                                                _currentPosition.latitude
                                                    .toString(),
                                                _currentPosition.longitude
                                                    .toString(),
                                                tag.text)
                                            .then(
                                          (value) {
                                            if (value) {
                                              images.clear();
                                              Fluttertoast.showToast(
                                                msg:
                                                    'Account created successfully!',
                                                toastLength: Toast.LENGTH_SHORT,
                                                gravity: ToastGravity.BOTTOM,
                                                backgroundColor: Colors.green,
                                                textColor: Colors.white,
                                                fontSize: 13.0,
                                              );
                                              setState(() {
                                                _isLoading = false;
                                              });
                                              setState(() {
                                                this._currentSteps =
                                                    this._currentSteps + 1;
                                              });
                                              // Navigator.pushNamed(context, '/login');
                                            } else {
                                              setState(() {
                                                _isLoading = false;
                                              });
                                              Fluttertoast.showToast(
                                                msg:
                                                    'An error occured during account creating',
                                                toastLength: Toast.LENGTH_SHORT,
                                                gravity: ToastGravity.BOTTOM,
                                                backgroundColor: Colors.red,
                                                textColor: Colors.white,
                                                fontSize: 13.0,
                                              );
                                            }
                                          },
                                        );
                                      } else {
                                        Fluttertoast.showToast(
                                          msg: 'All fields are required',
                                          toastLength: Toast.LENGTH_SHORT,
                                          gravity: ToastGravity.BOTTOM,
                                          backgroundColor: Colors.red,
                                          textColor: Colors.white,
                                          fontSize: 13.0,
                                        );
                                      }
                                    }).catchError((e) {
                                      print(e);
                                    });
                                  }
                                });
                              }
                            } else {
                              print(
                                  "on entre ici******************************************************************");
                              var response = await StripeService.payWithNewCard(
                                  amount: '1499', currency: 'USD');
                              print(
                                  "valeur de la responseddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd");
                              if (response != null && response.success) {
                                Fluttertoast.showToast(
                                  msg: 'Successful payment!',
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.BOTTOM,
                                  backgroundColor: Colors.green,
                                  textColor: Colors.white,
                                  fontSize: 13.0,
                                );
                                Navigator.pushNamed(context, '/login');
                              } else {
                                Fluttertoast.showToast(
                                  msg: 'An error occured during payment',
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.BOTTOM,
                                  backgroundColor: Colors.red,
                                  textColor: Colors.white,
                                  fontSize: 13.0,
                                );
                              }
                            }
                          },
                          onStepCancel: () {
                            if (this._currentSteps > 0) {
                              this._currentSteps = this._currentSteps - 1;
                            } else {
                              print('canceling');
                            }
                          },
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        isLoading: _isLoading,
        color: Colors.white,
        opacity: 0.5,
        progressIndicator: CircularProgressIndicator(
          backgroundColor: Color(0xFF9C27B0),
        ),
      ),
    );
  }

  List<Step> _steps() {
    List<Step> _mySteps = [
      Step(
        title: Text(''),
        state: 0 == this._currentSteps
            ? StepState.editing
            : 0 < this._currentSteps
                ? StepState.complete
                : StepState.indexed,
        content: Column(
          children: [
            CustomField(
              controller: username,
              label: 'Business name',
              helper: 'Enter your business name',
              actionText: '',
              type: TextInputType.text,
            ),
            CustomField(
              controller: email,
              label: 'Email',
              helper: 'Enter your email',
              actionText: '',
              type: TextInputType.emailAddress,
            ),
            CustomField(
              controller: password1,
              label: 'Password',
              helper: 'Enter your Password',
              actionText: '',
              type: TextInputType.text,
              isPassword: true,
            ),
            CustomField(
              controller: password2,
              label: 'Confirm password',
              helper: 'Confirm your password',
              actionText: '',
              type: TextInputType.text,
              isPassword: true,
            ),
          ],
        ),
        isActive: _currentSteps >= 0,
      ),
      Step(
        title: Text(''),
        state: 1 == this._currentSteps
            ? StepState.editing
            : 1 < this._currentSteps
                ? StepState.complete
                : StepState.indexed,
        content: Column(
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5.0),
                border: Border.all(color: Color(0xFF9C27B0), width: 1.5),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    spreadRadius: 0.0,
                    blurRadius: 10.0,
                    offset: Offset(0, 8.0),
                  ),
                ],
              ),
              child: DropDownField(
                onValueChanged: (dynamic value) {
                  print("valeur " + value);
                  cathegory_id = value;
                },
                value: cathegory_id,
                required: false,
                hintText: 'Choose a category ',
                labelText: 'category ',
                items: cathegorylist,
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            CustomField(
              controller: tag,
              label: 'Tags',
              helper: 'Separate tags with commas',
              actionText: '',
              type: TextInputType.text,
            ),
            CustomField(
              controller: address,
              label: 'Address',
              helper: 'Enter your address',
              actionText: '',
              type: TextInputType.text,
            ),
            CustomField(
              controller: phone,
              label: 'Phone Number',
              helper: 'Enter your phone number',
              actionText: '',
              type: TextInputType.number,
            ),
            CustomField(
              controller: region,
              label: 'State',
              helper: 'Enter your State',
              actionText: '',
              type: TextInputType.text,
            ),
            CustomField(
              controller: city,
              label: 'City',
              helper: 'Enter your city',
              actionText: '',
              type: TextInputType.text,
            ),
            CustomField(
              controller: description,
              label: 'Business Description',
              helper: 'Enter your business description',
              actionText: '',
              type: TextInputType.text,
            ),
            CustomField(
              controller: instagram,
              label: 'Instagram',
              helper: 'Enter your instagram account',
              actionText: '',
              type: TextInputType.text,
            ),
            CustomField(
              controller: facebook,
              label: 'Facebook',
              helper: 'https//facebook.com/userAccount',
              actionText: '',
              type: TextInputType.text,
            ),
            CustomField(
              controller: website,
              label: 'Website',
              helper: 'https//google.com',
              actionText: '',
              type: TextInputType.text,
            ),
          ],
        ),
        isActive: _currentSteps >= 0,
      ),
      Step(
        title: Text(''),
        state: 2 == this._currentSteps
            ? StepState.editing
            : 2 < this._currentSteps
                ? StepState.complete
                : StepState.indexed,
        content: Column(
          children: [
            SizedBox(
              height: 2.0,
            ),
            Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Text(
                        'Add logo of your compagny',
                        style: TextStyle(
                          fontFamily: 'Source',
                          fontSize: 15.0,
                          color: Colors.grey,
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        print('Load image');

                        getImagelogo();
                      },
                      child: Container(
                        padding: EdgeInsets.all(5.0),
                        decoration: BoxDecoration(
                            color: Color(0xFF320A3B),
                            border: Border.all(
                              color: Colors.grey.shade300,
                              width: 1.0,
                              style: BorderStyle.solid,
                            ),
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey,
                                blurRadius: 1.0,
                                offset: Offset(1, 1),
                              )
                            ]),
                        child: Icon(
                          Icons.photo_album,
                          color: Colors.white,
                          size: 25.0,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 2.0,
                ),
                Container(
                  alignment: Alignment.center,
                  height: 75.0,
                  width: MediaQuery.of(context).size.width,
                  child: _image == null
                      ? Text(
                          'No logo added',
                          style: TextStyle(
                            fontFamily: 'Corves Light',
                            color: Colors.grey,
                          ),
                        )
                      : ListView(
                          scrollDirection: Axis.horizontal,
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 5.0, right: 5.0),
                              child: Image.file(_image),
                            ),
                          ],
                        ),
                ),
              ],
            ),
            SizedBox(
              height: 2.0,
            ),
            Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Text(
                        'Add additionals images',
                        style: TextStyle(
                          fontFamily: 'Source',
                          fontSize: 15.0,
                          color: Colors.grey,
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        print('Load image');

                        getImage();
                      },
                      child: Container(
                        padding: EdgeInsets.all(5.0),
                        decoration: BoxDecoration(
                            color: Color(0xFF320A3B),
                            border: Border.all(
                              color: Colors.grey.shade300,
                              width: 1.0,
                              style: BorderStyle.solid,
                            ),
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey,
                                blurRadius: 1.0,
                                offset: Offset(1, 1),
                              )
                            ]),
                        child: Icon(
                          Icons.photo_album,
                          color: Colors.white,
                          size: 25.0,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 5.0,
                ),
                Container(
                  alignment: Alignment.center,
                  height: 75.0,
                  width: MediaQuery.of(context).size.width,
                  child: _image == null
                      ? Text(
                          'No image added',
                          style: TextStyle(
                            fontFamily: 'Corves Light',
                            color: Colors.grey,
                          ),
                        )
                      : ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: images.length,
                          itemBuilder: (context, index) {
                            return Container(
                              margin: EdgeInsets.only(left: 5.0, right: 5.0),
                              child: Image.file(images[index]),
                            );
                          },
                        ),
                ),
              ],
            ),
            SizedBox(
              height: 5.0,
            ),
            Flex(
              direction: Axis.horizontal,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '\$14.99 /Month',
                        style: TextStyle(
                          fontFamily: 'Coves Bold',
                          fontSize: 18.0,
                        ),
                      ),
                      Text(
                        'List your business for one low monthly fee to get unlimited visibility! Enjoy a 14-day free trial!',
                        style: TextStyle(
                          fontFamily: 'Source',
                        ),
                      ),
                    ],
                  ),
                ),
                Radio(
                  value: 1,
                  groupValue: group,
                  onChanged: (t) {
                    print('T');

                    setState(() {
                      group = t;
                    });
                  },
                ),
              ],
            ),
          ],
        ),
        isActive: _currentSteps >= 0,
      ),
    ];
    return _mySteps;
  }

  _getCurrentLocation() {
    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
        lat = _currentPosition.latitude.toString();
        long = _currentPosition.longitude.toString();
      });
    }).catchError((e) {
      print(e);
    });
  }
}
