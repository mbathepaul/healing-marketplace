import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class PackageScreen extends StatefulWidget {
  @override
  _PackageScreenState createState() => _PackageScreenState();
}

class _PackageScreenState extends State<PackageScreen> {
  String selected = 'patient';

  @override
  Widget build(BuildContext context) {
    WidgetsFlutterBinding.ensureInitialized();
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor: Color(0xFF320A3B),
      ),
    );

    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        margin: EdgeInsets.only(left: 5),
                        child: Icon(
                          Icons.arrow_back,
                          color: Colors.grey,
                          size: 30,
                        ),
                      )),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(height: 120),
                      Text(
                        'In which role will you like to sign up',
                        style: TextStyle(
                          fontFamily: 'Coves Bold',
                          fontSize: 23.0,
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(height: 40.0),
                      SizedBox(height: 25.0),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          PackageItem(
                            icon: Icons.receipt,
                            title: 'Patient',
                            selected: selected == 'patient',
                            callback: () {
                              setState(() {
                                selected = 'patient';
                              });
                            },
                          ),
                          PackageItem(
                            icon: Icons.monetization_on,
                            title: 'Therapeutist',
                            selected: selected == 'therapeutist',
                            callback: () {
                              setState(() {
                                selected = 'therapeutist';
                              });
                            },
                          ),
                        ],
                      ),
                      SizedBox(height: 80.0),
                      RaisedButton(
                        onPressed: () {
                          if (selected == 'patient')
                            Navigator.pushNamed(context, '/register');
                          else
                            Navigator.pushNamed(context, '/signup');
                        },
                        padding: const EdgeInsets.symmetric(
                          horizontal: 50.0,
                          vertical: 15.0,
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(5.0),
                          ),
                        ),
                        color: Color(0xFF320A3B),
                        child: Text(
                          'Next Step',
                          style: TextStyle(
                            fontFamily: 'Coves Light',
                            fontSize: 15.0,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                ])),
      ),
    );
  }
}

class PackageItem extends StatefulWidget {
  const PackageItem({
    Key key,
    this.icon,
    this.title,
    this.callback,
    this.selected = false,
  }) : super(key: key);

  final IconData icon;
  final String title;
  final Function callback;
  final bool selected;

  @override
  _PackageItemState createState() => _PackageItemState();
}

class _PackageItemState extends State<PackageItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.callback,
      child: Container(
        height: 130.0,
        width: 150.0,
        margin: EdgeInsets.only(right: 20.0),
        decoration: BoxDecoration(
          color: widget.selected ? Color(0xFF320A3B) : Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              blurRadius: 5.0,
              spreadRadius: 1.0,
              offset: Offset(0.0, 2.0),
            ),
          ],
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              widget.icon,
              color: widget.selected ? Colors.white : Color(0xFF320A3B),
              size: 60.0,
            ),
            SizedBox(height: 5.0),
            Text(
              widget.title ?? '',
              style: TextStyle(
                color: widget.selected ? Colors.white : Color(0xFF320A3B),
                fontFamily: 'Coves Light',
                fontSize: 18.0,
              ),
            )
          ],
        ),
      ),
    );
  }
}
