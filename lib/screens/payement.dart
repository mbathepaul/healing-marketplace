import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:healing/services/payment-service.dart';
import 'package:healing/services/therapist_service.dart';
import 'package:healing/widgets/custom_field.dart';
import 'package:healing/widgets/therapeutist.dart';
import 'package:image_picker/image_picker.dart';
import 'package:healing/services/authService.dart';
import 'package:healing/widgets/custom_button.dart';
import 'package:healing/widgets/custom_field.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:dropdownfield/dropdownfield.dart';
import 'package:loading_overlay/loading_overlay.dart';

class Payement extends StatefulWidget {
  @override
  _PayementState createState() => _PayementState();
}

class _PayementState extends State<Payement> {
  File _image;
  int group = 1;
  List<File> images = [];
  int _contimage = 0;
  bool _isLoading = false;
  static String id_sign_up;

  int _currentSteps = 0;

  bool typeclient = false;
  bool typetherapist = true;

  Position _currentPosition;
  String lat = "";
  String long = "";
  String cathegory_id = "";
  List<String> cathegorylist = [];
  bool _isadd = false;

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
    StripeService.init();
  }

  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LoadingOverlay(
        child: SafeArea(
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 20.0),
            decoration: BoxDecoration(
              color: Colors.white,
              image: DecorationImage(
                image: AssetImage('assets/images/background.jpg'),
                fit: BoxFit.fill,
              ),
            ),
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Container(
                          padding: EdgeInsets.all(10.0),
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.grey.shade300,
                              width: 1.0,
                              style: BorderStyle.solid,
                            ),
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Icon(
                            Icons.arrow_back_ios,
                            color: Colors.grey,
                          ),
                        ),
                      ),
                      Text(
                        'Subcription',
                        style: TextStyle(
                          fontFamily: 'Coves Bold',
                          fontSize: 28.0,
                          color: Color(0xFF9C27B0),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 45, left: 20),
                    child: Text(
                      '\$14.99 /Month',
                      style: TextStyle(
                        fontFamily: 'Coves Bold',
                        fontSize: 18.0,
                      ),
                    ),
                  ),
                  Row(
                    children: [
                      Container(
                        width: 250,
                        padding: EdgeInsets.only(bottom: 45, left: 20, top: 10),
                        child: Text(
                          'List your business for one low monthly free to get unlimited visibility!',
                          style: TextStyle(
                            fontFamily: 'Source',
                          ),
                        ),
                      ),
                      Radio(
                        value: 1,
                        groupValue: group,
                        activeColor: Color(0xFF9C27B0),
                        onChanged: (t) {
                          print('T');

                          setState(() {
                            group = t;
                          });
                        },
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 200,
                  ),
                  GestureDetector(
                    child: Center(
                      child: Container(
                        alignment: Alignment.center,
                        height: 37.0,
                        width: "make the payment".length.toDouble() * 12,
                        decoration: BoxDecoration(
                          color: Color(0xFF9C27B0),
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        child: Text(
                          'Make the payment',
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Coves Bold',
                            fontSize: 20.0,
                          ),
                        ),
                      ),
                    ),
                    onTap: () async {
                      print(
                          "on entre ici******************************************************************");
                      var response = await StripeService.payWithNewCard(
                          amount: '1499', currency: 'USD');
                      print(
                          "valeur de la responseddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd");
                      if (response != null && response.success) {
                        Fluttertoast.showToast(
                          msg: 'Successful payment!',
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.BOTTOM,
                          backgroundColor: Colors.green,
                          textColor: Colors.white,
                          fontSize: 13.0,
                        );
                        Navigator.pop(context);
                      } else {
                        Fluttertoast.showToast(
                          msg: 'An error occured during payment',
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.BOTTOM,
                          backgroundColor: Colors.red,
                          textColor: Colors.white,
                          fontSize: 13.0,
                        );
                      }
                    },
                  ),
                  SizedBox(height: 10.0),
                ],
              ),
            ),
          ),
        ),
        isLoading: _isLoading,
        color: Colors.white,
        opacity: 0.5,
        progressIndicator: CircularProgressIndicator(
          backgroundColor: Color(0xFF9C27B0),
        ),
      ),
    );
  }
}
