import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:healing/core/utils/status_bar.dart';
import 'package:healing/screens/auth/login.dart';
import 'package:healing/screens/dashboard/home.dart';
import 'package:healing/services/authService.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:stripe_payment/stripe_payment.dart';

class DashboardContainer extends StatefulWidget {
  @override
  _DashboardContainerState createState() => _DashboardContainerState();
}

class _DashboardContainerState extends State<DashboardContainer> {
  final List<Widget> screens = [
    HomeScreen(),
  ];

  int currentScreen;
  final PageStorageBucket bucket = PageStorageBucket();

  @override
  void initState() {
    currentScreen = 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    statusBar.setColor(context: context);

    return Scaffold(
      body: SafeArea(
        bottom: true,
        child: PageStorage(
          bucket: bucket,
          child: screens[currentScreen],
        ),
      ),
      extendBody: true,
      bottomNavigationBar: Container(
        height: 70.0,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Color(0xFF011424).withOpacity(0.08),
              spreadRadius: 5.0,
              blurRadius: 10.0,
            ),
          ],
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(50.0),
            topRight: Radius.circular(50.0),
          ),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            GestureDetector(
              onTap: () {
                if (AuthService.token != null) {
                  Navigator.pushNamed(context, '/blog');
                }
              },
              child: Icon(
                Icons.library_books_sharp,
                color: AuthService.token == null
                    ? Color(0xFF011424).withOpacity(0.4)
                    : Color(0xFF9C27B0),
                size: 30.0,
              ),
            ),
            GestureDetector(
              onTap: () async {
                if (AuthService.token != null) {
                  FlutterSecureStorage _localStorage =
                      new FlutterSecureStorage();
                  await _localStorage.deleteAll();
                  AuthService.token = null;
                  AuthService.email = null;
                  AuthService.username = null;
                  AuthService.id = null;
                  AuthService.doctor = null;
                  AuthService.is_therapist = null;
                  Fluttertoast.showToast(
                    msg: 'You have been disconnected',
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.TOP,
                    backgroundColor: Color(0xFF9C27B0),
                    textColor: Colors.white,
                    fontSize: 12.0,
                  );
                  Navigator.pushAndRemoveUntil(
                      context,
                      PageRouteBuilder(pageBuilder: (BuildContext context,
                          Animation animation, Animation secondaryAnimation) {
                        return LoginScreen();
                      }, transitionsBuilder: (BuildContext context,
                          Animation<double> animation,
                          Animation<double> secondaryAnimation,
                          Widget child) {
                        return new SlideTransition(
                          position: new Tween<Offset>(
                            begin: const Offset(1.0, 0.0),
                            end: Offset.zero,
                          ).animate(animation),
                          child: child,
                        );
                      }),
                      (Route route) => false);
                } else {
                  Navigator.pushNamed(context, '/login');
                }
              },
              child: Container(
                alignment: Alignment.center,
                width: 80.0,
                height: 50.0,
                decoration: BoxDecoration(
                  color: Color(0xFF9C27B0),
                  border: Border.all(
                    color: Colors.white,
                    width: 2.0,
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xFF011424).withOpacity(0.3),
                      spreadRadius: 1.0,
                      blurRadius: 10.0,
                      offset: Offset(0, 7.0),
                    ),
                  ],
                  borderRadius: BorderRadius.circular(15.0),
                ),
                child: Text(
                  AuthService.token == null ? 'Sign in' : 'Logout',
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'Coves Bold',
                  ),
                ),
              ),
            ),
            if (AuthService.is_therapist || AuthService.token == null)
              GestureDetector(
                onTap: () {
                  if (AuthService.is_therapist) {
                    Navigator.pushNamed(context, '/proifele_new');
                  }
                },
                child: Icon(
                  Icons.person_outline,
                  color: !AuthService.is_therapist
                      ? Color(0xFF011424).withOpacity(0.4)
                      : Color(0xFF9C27B0),
                  size: 30.0,
                ),
              ),
          ],
        ),
      ),
    );
  }
}
