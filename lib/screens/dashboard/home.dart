import 'dart:convert';
import 'dart:math';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:healing/core/models/category.dart';
import 'package:healing/core/models/doctor.dart';
import 'package:healing/services/authService.dart';
import 'package:healing/widgets/home_category.dart';
import 'package:healing/widgets/therapeutist.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:healing/services/therapist_service.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:healing/widgets/home_doctor.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  AnimationController _controller;
  Animation<double> _animation;
  AnimationController _offsetController;
  Animation<Offset> _offsetAnimation;
  AnimationController _searchController;
  Animation<double> _searchAnimation;
  bool connected = false;
  Position _currentPosition;
  String lat = "";
  String long = "";
  bool is_position = true;
  List<Doctor> doctors = [
    Doctor(
      user: 'Simon Christiansen',
      description: 'Specialist Dentist',
      phone: '4.0',
      image: 'assets/images/doctors/1.jpg',
    ),
    Doctor(
      user: 'Christine Swords',
      description: 'Nuclear Doctor',
      phone: '4.0',
      image: 'assets/images/doctors/2.jpg',
    ),
    Doctor(
      user: 'Theresa Mullins',
      description: 'Genetics Specialist',
      phone: '4.0',
      image: 'assets/images/doctors/3.jpg',
    ),
    Doctor(
      user: 'Clara McDonald',
      description: 'Gynecology Specialist',
      phone: '4.0',
      image: 'assets/images/doctors/4.png',
    ),
    Doctor(
      user: 'Chloe Coldman',
      description: 'Ophtalmologist',
      phone: '4.0',
      image: 'assets/images/doctors/5.png',
    ),
  ];

  List<Doctor> recommandeds = [];
  TextEditingController searhval = TextEditingController();
  List<Category> categories = [
    Category(
      title: 'Yoga',
      therapeutist: 25,
      color: Colors.orange,
      image: Image.asset('assets/icons/yoga.png'),
    ),
    Category(
      title: 'Energy Healing',
      therapeutist: 13,
      color: Colors.green,
      image: Image.asset('assets/icons/capsule.png'),
    ),
    Category(
      title: 'Herbal Healing',
      therapeutist: 10,
      color: Colors.blue,
      image: Image.asset('assets/icons/herbal.png'),
    ),
    Category(
      title: 'Meditation',
      therapeutist: 16,
      color: Colors.red,
      image: Image.asset('assets/icons/capsule.png'),
    ),
    Category(
      title: 'Holistic Medecine',
      therapeutist: 7,
      color: Colors.orange,
      image: Image.asset('assets/icons/holistic.png'),
    ),
    Category(
      title: 'Acupuncture',
      therapeutist: 23,
      color: Colors.red,
      image: Image.asset('assets/icons/acupuncture.png'),
    ),
    Category(
      title: 'Massage Therapy',
      therapeutist: 25,
      color: Colors.orange,
      image: Image.asset('assets/icons/massage.png'),
    ),
    Category(
      title: 'Aroma Therapy',
      therapeutist: 13,
      color: Colors.green,
      image: Image.asset('assets/icons/candle.png'),
    ),
    Category(
      title: 'Hypnotherapy',
      therapeutist: 10,
      color: Colors.blue,
      image: Image.asset('assets/icons/hypnosis.png'),
    ),
    Category(
      title: 'Chiropractic',
      therapeutist: 16,
      color: Colors.red,
      image: Image.asset('assets/icons/chiropractic.png'),
    ),
  ];
  Map<String, String> allValues;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

// Read value

// Read all values

// Delete value

// Delete all

// Write value

    TherapistService().getcategoryTherapists().then((value) {
      print(
          "comment faire la guerre en algeri;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;");
      print(value);
      for (Category category in categories) {
        category.therapeutist = value[category.title];
      }
    }).catchError((onError) {
      print(onError);
    });

    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      _currentPosition = position;
      lat = _currentPosition.latitude.toString();
      long = _currentPosition.longitude.toString();

      TherapistService().getrecommanded(lat, long).then((value) {
        setState(() {
          recommandeds = value.cast<Doctor>();
        });
      });
      print(lat + "    " + long);
    }).catchError((e) {
      print("on passe par ici");
      print(e);
    });
    geolocator.isLocationServiceEnabled().then((val) {
      if (!val) {
        showCupertinoDialog(
          context: context,
          builder: (context) {
            return Theme(
                data: ThemeData(
                    dialogBackgroundColor: Colors.black,
                    dialogTheme: DialogTheme(backgroundColor: Colors.black)),
                child: Dialog(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  elevation: 0,
                  backgroundColor: Colors.transparent,
                  child: Stack(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(
                            left: 25, top: 20, right: 25, bottom: 20),
                        margin: EdgeInsets.only(top: 45),
                        decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black,
                                  offset: Offset(0, 10),
                                  blurRadius: 10),
                            ]),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text(
                              "To get recommend healers, you must turn on your device location which uses google's location service.",
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.w600),
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: Text(
                                    "No THANKS",
                                    style: TextStyle(
                                      color: Colors.green,
                                      fontFamily: 'Source SemiBold',
                                      fontSize: 17.0,
                                    ),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: Text(
                                    "OK",
                                    style: TextStyle(
                                      color: Colors.green,
                                      fontFamily: 'Source SemiBold',
                                      fontSize: 17.0,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 8,
                            ),
                          ],
                        ),
                      ), // bottom part
                      // top part
                    ],
                  ),
                ));
          },
        );
      }
    });
  }
  /*void loadAnimations() async {
    // header text animation
    _offsetController =
        AnimationController(value: this, duration: const Duration(seconds: 1));
    _offsetAnimation = Tween<Offset>(
      begin: Offset(-10.0, 0.0),
      end: Offset.zero,
    ).animate(
      CurvedAnimation(
        parent: _offsetController,
        curve: Curves.bounceIn,
      ),
    );

    // avatar animation
    _controller = AnimationController(
      value: this,
      duration: const Duration(milliseconds: 1000),
    );
    _animation =
        CurvedAnimation(parent: _controller, curve: Curves.bounceInOut);

    // search textfield animation
    _searchController = AnimationController(
      value: this,
      duration: const Duration(seconds: 1),
    );
    _searchAnimation =
        CurvedAnimation(parent: _searchController, curve: Curves.bounceInOut);

    // start animations
    await _offsetController.forward();
    await _controller.forward();
    await _searchController.forward();
  }

  @override
  void initState() {
    super.initState();
    loadAnimations();
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
    _offsetController.dispose();
  }*/

  final scaffoldSate = GlobalKey<ScaffoldState>();
  FocusNode _focusNode = new FocusNode();
  double _overlap = 0;

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    print("authservice");
    print(AuthService.username);
    return Scaffold(
      body: SafeArea(
        key: scaffoldSate,
        child: Stack(
          children: <Widget>[
            Container(
              width: screenSize.width,
              height: screenSize.height,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/backImage.jpg'),
                  fit: BoxFit.fill,
                ),
              ),
              padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 15.0),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Welcome to',
                              style: TextStyle(
                                fontFamily: 'Source SemiBold',
                                fontSize: 13.0,
                                color: Color(0xFF011424).withOpacity(0.6),
                              ),
                            ),
                            SizedBox(height: 7.0),
                            Text(
                              'Healing Marketplace',
                              style: TextStyle(
                                fontFamily: 'Coves Bold',
                                fontSize: 22.0,
                                color: Color(0xFF011424),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Column(
                        children: [
                          Text(
                            AuthService.username == null
                                ? ""
                                : AuthService.username,
                            style: TextStyle(
                                fontFamily: 'Source SemiBold',
                                fontSize: 15.0,
                                color: Color(0xFF0A020A).withOpacity(0.9),
                                fontWeight: FontWeight.bold),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                child: Stack(
                                  children: <Widget>[
                                    IconButton(
                                      icon: Icon(
                                        Icons.notifications_none,
                                        color: Color(0xFF011424),
                                        size: 24.0,
                                      ),
                                      onPressed: () {
                                        _controller.reverse();
                                      },
                                    ),
                                    Positioned(
                                      top: 15.0,
                                      right: 15.0,
                                      child: Container(
                                        width: 8.0,
                                        height: 8.0,
                                        decoration: BoxDecoration(
                                          color: Colors.red,
                                          borderRadius:
                                              BorderRadius.circular(500.0),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(width: 10.0),
                              Container(
                                child: connected
                                    ? Container(
                                        width: 50.0,
                                        height: 50.0,
                                        decoration: BoxDecoration(
                                          color: Color(0xFF011424)
                                              .withOpacity(0.8),
                                          borderRadius:
                                              BorderRadius.circular(500.0),
                                          border: Border.all(
                                            color: Colors.white,
                                            width: 1.5,
                                          ),
                                        ),
                                      )
                                    : Container(),
                              ),
                            ],
                          ),
                        ],
                      )
                    ],
                  ), // end top bar
                  SizedBox(height: 10.0),

                  Text(
                    'At the Healing Market Place,  search for holistic healers in your area by type of care (Aromatherapy, hypnotherapy, etc) or by type of illness(headache, back pain)',
                    style: TextStyle(
                      fontFamily: 'Source Semi',
                      wordSpacing: 5.0,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 10.0),

                  Container(
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10.0),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.2),
                            blurRadius: 10.0,
                            spreadRadius: 3.0,
                          ),
                        ],
                      ),
                      child: TextField(
                        style: TextStyle(
                          fontFamily: 'Source SemiBold',
                          fontSize: 12.0,
                          color: Color(0xFF011424).withOpacity(0.8),
                        ),
                        onSubmitted: (search) {
                          print(search);
                          Navigator.pushNamed(context, '/searchresult',
                              arguments: search);
                        },
                        controller: searhval,
                        decoration: InputDecoration(
                          isDense: true,
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                          contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 5.0),
                          labelText: "Symptoms or Illness",
                          border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.transparent),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.transparent),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.transparent),
                          ),
                          labelStyle: TextStyle(
                            fontFamily: 'Source',
                            fontSize: 12.0,
                            color: Color(0xFF474E6C).withOpacity(0.7),
                          ),
                          prefixIcon: IconButton(
                            icon: Icon(
                              Icons.search,
                              color: Color(0xFF474E6C),
                            ),
                            onPressed: () {
                              FocusScope.of(context).unfocus();
                              if (AuthService.username == null) {
                                print(AuthService.token);
                                print("sans token");
                                Fluttertoast.showToast(
                                  msg: 'You must log in to search',
                                  toastLength: Toast.LENGTH_LONG,
                                  gravity: ToastGravity.TOP,
                                  backgroundColor: Color(0xFF011424),
                                  textColor: Colors.white,
                                  fontSize: 15.0,
                                );
                              } else {
                                print("on entre ici");
                                print(searhval.text);
                                if (lat != "" && long != "") {
                                  Navigator.pushNamed(context, '/searchresult',
                                      arguments: searhval.text +
                                          "&longitude=" +
                                          long +
                                          "&latitude=" +
                                          lat);
                                } else {
                                  Navigator.pushNamed(context, '/searchresult',
                                      arguments: searhval.text);
                                }
                              }
                            },
                          ),
                          suffixIcon: IconButton(
                            icon: Icon(
                              Icons.settings,
                              size: 20.0,
                              color: Color(0xFF474E6C),
                            ),
                            onPressed: null,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(height: 15.0),
                        Text(
                          "Recommended healers ",
                          textAlign: TextAlign.left,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16.0,
                          ),
                        ),
                        SizedBox(height: 15.0),
                      ],
                    ),
                  ),
                  Container(
                    child: CarouselSlider(
                      options: CarouselOptions(
                        height: 200,
                        aspectRatio: 2.0,
                        viewportFraction: 0.9,
                        initialPage: 0,
                        enableInfiniteScroll: true,
                        reverse: false,
                        autoPlay: true,
                        autoPlayInterval: Duration(seconds: 5),
                        autoPlayAnimationDuration: Duration(milliseconds: 2000),
                        autoPlayCurve: Curves.fastOutSlowIn,
                        scrollDirection: Axis.horizontal,
                      ),
                      items: recommandeds.map((i) {
                        return Builder(
                          builder: (BuildContext context) {
                            return HomeDoctorWidget(
                              key: new Key(i.id),
                              speciality: i.label,
                              image: i.image,
                              name: i.user,
                              doctor: i,
                            );
                          },
                        );
                      }).toList(),
                    ),
                  )
                  // end search field
                ],
              ),
            ),
            SlidingUpPanel(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(50.0),
                topRight: Radius.circular(50.0),
              ),
              boxShadow: [
                BoxShadow(
                  color: Color(0xFF011424).withOpacity(0.07),
                  spreadRadius: 2.0,
                  blurRadius: 20.0,
                ),
              ],
              padding: EdgeInsets.only(
                  right: 30.0, left: 30.0, top: 2.0, bottom: 10),
              minHeight: screenSize.height / 2 - 300.0,
              maxHeight: screenSize.height / 2 + 190.0,
              backdropEnabled: true,
              backdropColor: Color(0xFF011424),
              backdropOpacity: 0.3,
              panel: Container(
                  child: Column(
                children: <Widget>[
                  Container(
                    height: 4.0,
                    width: 20.0,
                    margin: EdgeInsets.only(bottom: 40.0),
                    decoration: BoxDecoration(
                      color: Color(0xFF011424).withOpacity(0.3),
                      borderRadius: BorderRadius.circular(100.0),
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                      itemCount: categories.length,
                      itemBuilder: (context, index) {
                        Category category = categories[index];

                        return HomeCategoryWidget(
                          title: category.title,
                          therapeutist: category.therapeutist,
                          color: category.color,
                          image: category.image,
                          lat: lat,
                          long: long,
                        );
                      },
                    ),
                  ),
                  SizedBox(height: 20.0),
                ],
              )),
            ),
          ],
        ),
      ),
    );
  }

  _getCurrentLocation() {
    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
        lat = _currentPosition.latitude.toString();
        long = _currentPosition.longitude.toString();
      });
    }).catchError((e) {
      print(e);
    });
  }
}

class _SecItem {
  _SecItem(this.key, this.value);

  final String key;
  final String value;
}
