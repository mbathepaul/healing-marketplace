import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:healing/core/models/availibilities.dart';
import 'package:healing/core/models/booking.dart';
import 'package:healing/core/models/category.dart';
import 'package:healing/core/models/comment.dart';
import 'package:healing/core/models/doctor.dart';
import 'package:healing/core/models/cathegorymap.dart';
import 'package:healing/core/models/listarticle.dart';
import 'package:healing/core/models/plan.dart';
import 'package:healing/core/models/product_category.dart';
import 'package:healing/core/resources/dio_wrapper.dart';
import 'package:healing/core/utils/constants.dart';
import 'package:healing/core/utils/extensions.dart';
import 'package:healing/services/authService.dart';
import "package:healing/core/models/article.dart";
import "package:healing/core/models/category_therapist.dart";

class TherapistService {
  static List<ProductCategory> category_products = [];
  static List<CategoryTherapist> category_products_id = [];
  static List<String> category_products_label = [];
  Future<List<Doctor>> getTherapist(your_query) async {
    try {
      final String uri =
          "https://healing-market.herokuapp.com/market-api/search/therapists/?query=" +
              your_query.toString();

      Response response = await dioWrapper
          .gettherapetist(uri, AuthService.token, useBase: true);

      if (((response.data["features"]) as List).length == 0) {
        List<Doctor> list = [];
        return list;
      }
      List<Doctor> doctors = [];

      List<Doctor>.from(
        ((response.data["features"]) as List).map(
          (json) {
            Doctor docto = Doctor.fromJson(json["properties"], json["id"]);
            if (docto != null) {
              doctors.add(docto);
            }
          },
        ),
      );

      return doctors;
    } catch (e) {
      print('An error occured during binging therapists');
    }
  }

  Future<List<String>> getcathegories() async {
    try {
      final String uri =
          "https://healing-market.herokuapp.com/market-api/categories";
      Response response;

      response = await dioWrapper.get(uri);

      var list = List<String>.from(
        ((response.data["results"]) as List).map(
          (json) {
            return json["label"] as String;
          },
        ),
      );

      return list;
    } catch (e) {
      print('An error occured during binging therapists');
    }
  }

  Future<List<ProductCategory>> getcathegories_shop() async {
    try {
      final String uri =
          "https://healing-market.herokuapp.com/shop-api/categories/";
      Response response;

      response = await dioWrapper.get(uri);

      List<ProductCategory> category_list = [];
      List<ProductCategory>.from(
        ((response.data["results"]) as List).map(
          (json) {
            ProductCategory docto = ProductCategory.fromJson(json);
            if (docto != null) {
              category_list.add(docto);
            }
          },
        ),
      );

      return category_list;
    } catch (e) {
      print(e);
      print('An error occured during binging therapists');
    }
  }

  Future<List<CategoryTherapist>> getcathegories_id() async {
    try {
      final String uri =
          "https://healing-market.herokuapp.com/market-api/categories";
      Response response;

      response = await dioWrapper.get(uri);

      var list = List<CategoryTherapist>.from(
        ((response.data["results"]) as List).map(
          (json) {
            CategoryTherapist category = CategoryTherapist(
                id: json["id"] as String, label: json["label"] as String);
            return category;
          },
        ),
      );

      return list;
    } catch (e) {
      print('An error occured during binging therapists');
    }
  }

  Future<ListArticle> getArticles(String uri) async {
    try {
      ListArticle listArticle;
      //  final String uri = "https://healing-market.herokuapp.com/blog-api/articles/";

      Response response = await dioWrapper
          .gettherapetist(uri, AuthService.token, useBase: true);
      List<Article> articles = [];

      List<Article>.from(
        ((response.data["results"]) as List).map(
          (json) {
            Article article = Article.fromJson(json);
            if (article != null) {
              articles.add(article);
            }
          },
        ),
      );

      listArticle =
          ListArticle(articles: articles, hasnext: response.data["next"]);

      return listArticle;
    } catch (e) {
      List<Article> li = [];
      return e;
    }
  }

  Future<ListArticle> getCategoryProduct() async {
    try {
      ListArticle listArticle;
      final String uri =
          "https://healing-market.herokuapp.com/shop-api/categories/";

      Response response = await dioWrapper
          .gettherapetist(uri, AuthService.token, useBase: true);
      List<Article> articles = [];

      return listArticle;
    } catch (e) {
      List<Article> li = [];
      return e;
    }
  }

  Future<List<String>> getplans() async {
    try {
      final String uri =
          "https://healing-market.herokuapp.com/membership-api/plans/";
      Response response;

      response = await dioWrapper.get(uri);

      var list = List<String>.from(
        ((response.data["results"]) as List).map(
          (json) {
            return Plan.fromJson(json);
          },
        ),
      );

      return list;
    } catch (e) {
      print('An error occured during binging therapists');
    }
  }

  Future<List<Doctor>> getrecommanded(lat, long) async {
    try {
      final String uri =
          "https://healing-market.herokuapp.com/market-api/search/therapists_location/?longitude=${double.parse(lat)}&latitude=${double.parse(long)}";
      Response response;

      print(uri);

      response = await dioWrapper.get(uri);

      List<Doctor> doctors = [];
      List<Doctor>.from(
        ((response.data["features"]) as List).map(
          (json) {
            Doctor docto = Doctor.fromJson(json["properties"], json["id"]);

            if (docto != null) {
              doctors.add(docto);
            }
          },
        ),
      );

      return doctors;
    } catch (e) {
      List<Doctor> doctors = [];
      print('An error occured during binging therapists');
      print(e);
      return doctors;
    }
  }

  Future<List<Doctor>> getcategoryterapist(lat, long) async {
    try {
      final String uri =
          "https://healing-market.herokuapp.com/market-api/categories";
      Response response;

      print(uri);
      response = await dioWrapper.get(uri);

      List<Doctor> doctors = [];
      List<Doctor>.from(
        ((response.data["features"]) as List).map(
          (json) {
            Doctor docto = Doctor.fromJson(json["properties"], json["id"]);

            if (docto != null) {
              doctors.add(docto);
            }
          },
        ),
      );

      return doctors;
    } catch (e) {
      print('An error occured during binging therapists');
      print(e);
    }
  }

  Future<Map<String, int>> getcategoryTherapists() async {
    try {
      final String uri =
          "https://healing-market.herokuapp.com/market-api/categories/";
      Response response;

      print(uri);
      response = await dioWrapper.get(uri);

      Map<String, int> category = new Map();
      List<Doctor>.from(
        ((response.data["results"]) as List).map(
          (json) {
            category[json["label"]] = json["nb_therapists"];
          },
        ),
      );

      return category;
    } catch (e) {
      print('An error occured during binging therapists');
      print(e);
    }
  }

  Future<List<Comm>> getTherapistById() async {
    try {
      ListArticle listArticle;

      final String uri =
          "https://healing-market.herokuapp.com/user-api/therapists/" +
              AuthService.id +
              '/detail';
      print(uri);
      Response response =
          await dioWrapper.gettoken(uri, AuthService.token, useBase: true);
      List<Comm> list = [];
      // print(response.data);
      if (response.data["properties"]["reviews"] != null) {
        List<String>.from(
          ((response.data["properties"]["reviews"]) as List).map(
            (son) {
              Comm comm = Comm(
                  article: son["therapist"] as String,
                  commentator: son["client"],
                  content: son["description"] as String,
                  createdAt: son["created_at"] as String,
                  updateAt: son["update_at"] as String);
              list.add(comm);
            },
          ),
        );
      }

      return list;
    } catch (e) {
      return e;
    }
  }

  Future<List<ProductCategory>> getTherapistProducts(id) async {
    try {
      ListArticle listArticle;

      final String uri =
          "https://healing-market.herokuapp.com/shop-api/therapists/" +
              id +
              '/products';
      print(uri);
      Response response =
          await dioWrapper.gettoken(uri, AuthService.token, useBase: true);
      List<Comm> list = [];
      // print(response.data);
      print(
          "dddddddddddddddddddhhhhhhdddddddddddddddddddddddddddddddddddddddddddddddddddddddddd");
      print(response.data);
      List<ProductCategory> category_list = [];
      List<ProductCategory>.from(
        ((response.data) as List).map(
          (json) {
            ProductCategory docto = ProductCategory.fromJson(json);
            if (docto != null) {
              category_list.add(docto);
            }
          },
        ),
      );
      print(category_list.length);

      return category_list;
    } catch (e) {
      return e;
    }
  }

  Future<List<Booki>> bookingList() async {
    try {
      final String uri =
          "https://healing-market.herokuapp.com/booking-api/therapists/bookings";

      Response response =
          await dioWrapper.gettoken(uri, AuthService.token, useBase: true);

      print(response.data);

      List<Booki> booking_list = [];
      List<Booki>.from(
        ((response.data) as List).map(
          (json) {
            Booki docto = Booki.fromJson(json);
            if (docto != null) {
              booking_list.add(docto);
            }
          },
        ),
      );
      print(booking_list.length);

      return booking_list;
    } catch (e) {
      print('An error occured during binging therapists');
    }
  }

  Future<List<Availibilitie>> listAviabilities() async {
    try {
      final String uri =
          "https://healing-market.herokuapp.com/booking-api/availibilities/?page=3";

      Response response =
          await dioWrapper.gettoken(uri, AuthService.token, useBase: true);

      print(response.data);

      List<Availibilitie> booking_list = [];
      List<Availibilitie>.from(
        ((response.data["results"]) as List).map(
          (json) {
            Availibilitie docto = Availibilitie.fromJson(json);
            if (docto != null) {
              booking_list.add(docto);
            }
          },
        ),
      );

      return booking_list;
    } catch (e) {
      print(e);
      print('une erreur ici');
    }
  }

  Future<List<Availibilitie>> listAviabilities_id(id) async {
    try {
      final String uri =
          "https://healing-market.herokuapp.com/booking-api/therapists/${id}/availibilities";

      Response response =
          await dioWrapper.gettoken(uri, AuthService.token, useBase: true);

      print(response.data);

      List<Availibilitie> booking_list = [];
      List<Availibilitie>.from(
        ((response.data) as List).map(
          (json) {
            Availibilitie docto = Availibilitie.fromJson(json);
            if (docto != null) {
              booking_list.add(docto);
            }
          },
        ),
      );

      return booking_list;
    } catch (e) {
      print(e);
      print('une erreur ici');
      return null;
    }
  }

  Future update_booking(id) async {
    try {
      final String uri =
          "https://healing-market.herokuapp.com/booking-api/bookings/${id}/start";

      Response response =
          await dioWrapper.gettoken(uri, AuthService.token, useBase: true);

      if (response.statusCode == 200) {
        return response.statusCode == 200;
      } else {
        return response.statusCode == 200;
      }
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future update_booking_rejet(id) async {
    try {
      final String uri =
          "https://healing-market.herokuapp.com/booking-api/bookings/${id}/cancel";

      Response response =
          await dioWrapper.gettoken(uri, AuthService.token, useBase: true);

      if (response.statusCode == 200) {
        return response.statusCode == 200;
      } else {
        return response.statusCode == 200;
      }
    } catch (e) {
      print(e);
      return false;
    }
  }
}
