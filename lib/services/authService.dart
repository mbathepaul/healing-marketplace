import 'dart:io';

import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart';
import 'package:healing/core/models/doctor.dart';
import 'package:healing/core/resources/dio_wrapper.dart';
import 'package:healing/screens/payement.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:healing/core/resources/http_wrapper.dart';

class AuthService {
  Dio dio = new Dio();
  static String token;
  static String username;
  static String email;
  static String id;
  static bool is_therapist = false;
  static Doctor doctor;
  static String id_sign_up;
  static File image;

  Future login(username, password) async {
    FlutterSecureStorage _localStorage = new FlutterSecureStorage();

    var response;
    try {
      var response = await dioWrapper.post(
        'https://healing-market.herokuapp.com/user-api/custom/login',
        params: {"username": username, "password": password},
      );

      if (response.statusCode == 200) {
        token = response.data["key"];
        AuthService.username = response.data["username"];
        AuthService.email = response.data["email"];
        AuthService.id = response.data["id"];
        AuthService.is_therapist = response.data["is_therapist"] != null
            ? response.data["is_therapist"]
            : null;
        if (AuthService.is_therapist) {
          var list = List<String>.from(
            ((response.data["photos"]) as List).map(
              (json) {
                return json["image"] as String;
              },
            ),
          );
          doctor = Doctor(
            id: AuthService.id,
            user: response.data["username"],
            email: response.data["email"],
            description: response.data["description"],
            label: response.data["category"],
            region: response.data["region"],
            address: response.data["address"],
            city: response.data["city"],
            phone: response.data["phone"],
            website: response.data["website"],
            facebook: response.data["facebook"],
            instalgram: response.data["instagram"],
            end_subscription: response.data["end_subscription"],
            nb_reviews: response.data["nb_reviews"],
            photos: list,
          );

          await _localStorage.write(key: "username", value: doctor.user);
          await _localStorage.write(key: "image", value: doctor.photos[0]);
          await _localStorage.write(key: "email", value: doctor.email);
          await _localStorage.write(
              key: "description", value: doctor.description);
          await _localStorage.write(key: "category", value: doctor.label);
          await _localStorage.write(key: "city", value: doctor.city);
          await _localStorage.write(key: "phone", value: doctor.phone);
          await _localStorage.write(key: "website", value: doctor.website);
          await _localStorage.write(key: "facebook", value: doctor.facebook);
          await _localStorage.write(key: "instagram", value: doctor.instalgram);
          await _localStorage.write(key: "address", value: doctor.address);
          await _localStorage.write(
              key: "nb_reviews", value: doctor.nb_reviews.toString());
          await _localStorage.write(
              key: "end_subscription", value: doctor.end_subscription);
          await _localStorage.write(key: "id", value: AuthService.id);
        }

        await _localStorage.write(key: "username", value: AuthService.username);
        await _localStorage.write(key: "token", value: AuthService.token);
        await _localStorage.write(key: "email", value: AuthService.email);
        await _localStorage.write(key: "id", value: AuthService.id);
        await _localStorage.write(
            key: "is_therapist", value: AuthService.is_therapist.toString());
      }

      return response.statusCode == 200;
    } on DioError catch (err) {
      try {
        print(err);
        print(err.response.data["message"]);
        return err.response.data["message"];
      } catch (e) {
        return "Network error";
      }
    }
  }

  Future reset(email) async {
    try {
      var response = await dioWrapper.post(
        'https://healing-market.herokuapp.com/rest-auth/password/reset/',
        params: {"email": email},
      );

      return response.statusCode == 200;
    } on DioError catch (err) {
      try {
        print(err);
        print(err.response.data["message"]);
        return err.response.data["message"];
      } catch (e) {
        return "Network error";
      }
    }
  }

  Future register(
      name, email, password, confirmPass, typeclient, typetherapist) async {
    try {
      var response = await dioWrapper.post(
          'https://healing-market.herokuapp.com/rest-auth/registration/',
          params: {
            "username": name,
            "email": email,
            "password1": password,
            "password2": confirmPass,
            "is_client": typeclient,
            "is_therapist": typetherapist
          });

      if (response.statusCode == 201) {
        AuthService.token = response.data["key"];
      }
      return response.statusCode == 201;
    } on DioError catch (err) {
      String message_erro;
      if (err.response.data["username"] != null) {
        message_erro = err.response.data["username"][0].toString();
      } else if (err.response.data["email"] != null) {
        message_erro = err.response.data["email"][0].toString();
      } else if (err.response.data["password"] != null) {
        message_erro = err.response.data["password"];
      } else if (err.response.data["password1"] != null) {
        message_erro = err.response.data["password1"][0].toString();
      } else if (err.response.data["non_field_errors"] != null) {
        message_erro = err.response.data["non_field_errors"][0].toString();
      } else {
        message_erro = "Something went wrong";
      }

      return message_erro;
    }
  }

  Future registerTherapist1(
      name, email, password, confirmPass, typeclient, typetherapist) async {
    try {
      var response = await dioWrapper.post(
          'http://healing-market.herokuapp.com/rest-auth/registration/',
          params: {
            "username": name,
            "email": email,
            "password1": password,
            "password2": confirmPass,
            "is_client": typeclient,
            "is_therapist": typetherapist
          });
      return response.statusCode == 201;
    } on DioError catch (err) {
      print(err);
      return false;
    }
  }

  Future registerTherapist2(
      category,
      addresse,
      phone,
      region,
      city,
      description,
      facebook,
      instagram,
      website,
      longitude,
      latitude,
      tag) async {
    try {
      var response = await dioWrapper.posttoken(
          'https://healing-market.herokuapp.com/user-api/therapists/', token,
          params: {
            "category": category,
            "address": addresse,
            "phone": phone,
            "region": region,
            "city": city,
            "description": description,
            "facebook": facebook,
            "instagram": instagram,
            "website": website,
            "tags": tag,
            "status": 1,
            "location": {
              "type": "Point",
              "coordinates": [double.parse(longitude), double.parse(latitude)],
            }
          });

      if (response.statusCode == 201) {
        AuthService.id_sign_up = response.data["id"];
        return addtag(response.data["id"], tag).then((value) => value);
      } else {
        return response.statusCode == 201;
      }
    } on DioError catch (err) {
      print(err);
      return false;
    }
  }

  Future updateTerapeutist(
    name,
    email,
    category,
    addresse,
    phone,
    region,
    city,
    description,
    facebook,
    instagram,
    website,
  ) async {
    try {
      var response = await dioWrapper.putoken(
          'https://healing-market.herokuapp.com/user-api/therapists/${AuthService.id}/',
          token,
          params: {
            "username": name,
            "email": email,
            "category": category,
            "address": addresse,
            "phone": phone,
            "region": region,
            "city": city,
            "description": description,
            "facebook": facebook,
            "instagram": instagram,
            "website": website,
            "status": 1,
          });

      if (response.statusCode == 200) {
        return response.statusCode == 200;
      } else {
        return response.statusCode == 200;
      }
    } on DioError catch (err) {
      print(err);
      return false;
    }
  }

  Future updateusermail(
    name,
    email,
  ) async {
    try {
      var response = await dioWrapper.putoken(
          'https://healing-market.herokuapp.com/user-api/users/${AuthService.id}/',
          token,
          params: {
            "username": name,
            "email": email,
          });

      return response.statusCode == 200;
    } on DioError catch (err) {
      print(err);

      return false;
    }
  }

  Future addtag(therapist, tag) async {
    try {
      var response = await dioWrapper.post(
          'https://healing-market.herokuapp.com/market-api/tag-providers/',
          params: {
            "therapist": therapist,
            "tag": tag,
          });

      return response.statusCode == 201;
    } on DioError catch (err) {
      print(err);

      return false;
    }
  }

  Future registerTherapist3(
      name, email, password, confirmPass, typeclient, typetherapist) async {
    try {
      var response = await dioWrapper.post(
          'http://healing-market.herokuapp.com/rest-auth/registration/',
          params: {
            "username": name,
            "email": email,
            "password1": password,
            "password2": confirmPass,
            "is_client": typeclient,
            "is_therapist": typetherapist
          });

      return response.statusCode = 201;
    } on DioError catch (err) {
      print(err);
      return false;
    }
  }

  Future addSuscription(payementmethod) async {
    try {
      var response = await dioWrapper.posttoken(
          'https://healing-market.herokuapp.com/membership-api/subscriptions/create',
          token,
          params: {"paymentMethodId": payementmethod});

      return response.statusCode == 201;
    } on DioError catch (err) {
      print(err);

      return false;
    }
  }

  Future orderlines(id, quantite) async {
    try {
      int quanti = 0;
      print("tokkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk");
      print(token);
      try {
        quanti = int.parse(quantite);
      } catch (e) {
        return false;
      }
      var response = await dioWrapper.posttoken(
          'https://healing-market.herokuapp.com/shop-api/orderlines/', token,
          params: {
            "product": id,
            "quantity": int.parse(quantite),
            "order": id,
          });

      return response.statusCode == 201;
    } on DioError catch (err) {
      print(err);

      return false;
    }
  }

  Future uploadfile(file) async {
    try {
      var response = await dioWrapper.sendFile(
          'https://healing-market.herokuapp.com/user-api/photos/', file, token);

      return response.statusCode == 201;
    } on DioError catch (err) {
      print(err);
      return false;
    }
  }

  /*Future add_product(
      File file, double price, String category, String name) async {
    print("on princcccccccccccccccccccccccccccccccccccccccccccccccccccccc");
    print(category);
    print(name);
    print(price);
    print(token);
    try {
      var response = await Http().uploadFile(
          'https://healing-market.herokuapp.com/shop-api/products/',
          file,
          "400",
          category,
          name,
          token);

      return response.statusCode == 201;
    } on DioError catch (err) {
      print(err);
      return false;
    }
  }*/

  Future addProduct(
      File file, double price, String category, String label) async {
    try {
      print("Uploading...");
      //create multipart request for POST or PATCH method
      String myUrl = "https://healing-market.herokuapp.com/shop-api/products/";
      //final prefs = await SharedPreferences.getInstance();
      //final key = 'token';
      //final value = prefs.get(key ) ?? 0;

      var request = http.MultipartRequest("POST", Uri.parse(myUrl));

      //create multipart using filepath, string or bytes
      request.headers["Authorization"] = "Token $token";
      var rec = await http.MultipartFile.fromPath("image", file.path);

      //add multipart to request
      request.files.add(rec);
      request.fields["price"] = price.toString();
      request.fields["label"] = label;
      request.fields["category"] = category;
      var response = await request.send();

      //Get the response from the server
      var responseData = await response.stream.toBytes();
      var responseString = String.fromCharCodes(responseData);
      return true;
    } catch (e) {
      return false;
    }
  }

  Future oderlinee1(id, quantite) async {
    try {
      print("Uploading...");
      //create multipart request for POST or PATCH method
      String myUrl =
          "https://healing-market.herokuapp.com/shop-api/orderlines/";
      //final prefs = await SharedPreferences.getInstance();
      //final key = 'token';
      //final value = prefs.get(key ) ?? 0;

      var request = http.MultipartRequest("POST", Uri.parse(myUrl));

      //create multipart using filepath, string or bytes
      request.headers["Authorization"] = "Token $token";

      //add multipart to request

      request.fields["quantity"] = quantite;
      request.fields["product"] = id;
      request.fields["order"] = "";
      var response = await request.send();

      //Get the response from the server
      var responseData = await response.stream.toBytes();
      var responseString = String.fromCharCodes(responseData);
      return true;
    } catch (e) {
      return false;
    }
  }

  Future uploadfiles(file) async {
    try {
      var response = await dioWrapper.sendFiles(
          'https://healing-market.herokuapp.com/user-api/photos/', file, token);

      return response.statusCode == 201;
    } on DioError catch (err) {
      print(err);
      return false;
    }
  }

  _save(String token) async {
    final prefs = await SharedPreferences.getInstance();
    final key = 'token';
    final value = token;
    prefs.setString(key, value);
  }

  static readtoken() async {
    final prefs = await SharedPreferences.getInstance();
    final key = 'token';
    final value = prefs.get(key) ?? 0;
    return value.toString();
  }

  Future updatereviews(id, value) async {
    try {
      var response = await dioWrapper.posttoken(
          'https://healing-market.herokuapp.com/market-api/ratings/', token,
          params: {
            "client": AuthService.id,
            "therapist": id,
            "value": value,
          });

      return response.statusCode == 201;
    } on DioError catch (err) {
      print(err);
      return false;
    }
  }

  Future addComment(id, value) async {
    try {
      var response = await dioWrapper.posttoken(
          'https://healing-market.herokuapp.com/market-api/reviews/', token,
          params: {
            "client": AuthService.id,
            "therapist": id,
            "description": value,
          });

      return response.statusCode == 201;
    } on DioError catch (err) {
      print(err);
      return false;
    }
  }

  Future bookinAdd(id, start_date, end_date) async {
    try {
      var response = await dioWrapper.posttoken(
          'https://healing-market.herokuapp.com/booking-api/bookings/', token,
          params: {
            "therapist": id,
            "start_date": start_date,
            "end_date": end_date
          });

      return response.statusCode == 201;
    } on DioError catch (err) {
      print(err);
      return false;
    }
  }

  Future addCommentArticle(id, value) async {
    try {
      var response = await dioWrapper.posttoken(
          'https://healing-market.herokuapp.com/blog-api/comments/', token,
          params: {
            "content": value,
            "article": id,
          });

      return response.statusCode == 201;
    } on DioError catch (err) {
      print(err);
      return false;
    }
  }

  Future addAvailibilities(start_time, end_time) async {
    try {
      var response = await dioWrapper.posttoken(
          'https://healing-market.herokuapp.com/booking-api/availibilities/',
          token,
          params: {
            "therapist": AuthService.id,
            "start_date": start_time,
            "end_date": end_time,
          });

      if (response.statusCode == 201) {
        return response.data["id"];
      }
      return;
    } on DioError catch (err) {
      print(err);
      return null;
    }
  }

  Future updateAvailibilities(id, start_time, end_time) async {
    try {
      var response = await dioWrapper.putoken(
          'https://healing-market.herokuapp.com/booking-api/availibilities/${id}/',
          token,
          params: {
            "therapist": AuthService.id,
            "start_date": start_time,
            "end_date": end_time,
          });

      print("statuiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
      print(response.statusCode);
      return response.statusCode == 200;
    } on DioError catch (err) {
      print(err);
      return null;
    }
  }

  Future deleteAvailibilities(id) async {
    print("suppression...........................................");
    print(id);
    print(
        "https://healing-market.herokuapp.com/booking-api/availibilities/${id}/");
    try {
      var response = await dioWrapper.deletetoken(
        'https://healing-market.herokuapp.com/booking-api/availibilities/${id}/',
        token,
      );
      print("satuoooooooooooooooooooooooooooooooooddddddddddddddddddd");
      print(response.statusCode);
      return response.statusCode == 204;
    } on DioError catch (err) {
      return false;
    }
  }

  Future updateBooking(
    status,
    start_date,
  ) async {
    try {
      var response = await dioWrapper.putoken(
        'https://healing-market.herokuapp.com/booking-api/bookings/${AuthService.id}/start',
        token,
      );

      if (response.statusCode == 200) {
        return response.statusCode == 200;
      } else {
        return response.statusCode == 200;
      }
    } on DioError catch (err) {
      print(err);
      return false;
    }
  }
}
