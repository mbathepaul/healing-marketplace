import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:healing/services/authService.dart';
import 'package:stripe_payment/stripe_payment.dart';
import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class StripeTransactionResponse {
  String message;
  bool success;
  StripeTransactionResponse({this.message, this.success});
}

class StripeService {
  static String apiBase = 'https://api.stripe.com/v1';
  static String paymentApiUrl = '${StripeService.apiBase}/payment_intents';
  static String secret =
      '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';
  static Map<String, String> headers = {
    'Authorization': 'Bearer ${StripeService.secret}',
    'Content-Type': 'application/x-www-form-urlencoded'
  };

  static init() {
    StripePayment.setOptions(StripeOptions(
        publishableKey:
            "pk_live_51IMxqoDkKBEu0UEdpUxmNXNux1ZX5rLcan6X8IYYawPBRyFwHs5XDkqG9oW5Rn4mFtRu2NMBfDtUKRJNKKQhxVb800EYckljkD",
        merchantId: "Test",
        androidPayMode: 'test'));
  }

  static Future<StripeTransactionResponse> payViaExistingCard(
      {String amount, String currency, CreditCard card}) async {
    try {
      var paymentMethod = await StripePayment.createPaymentMethod(
          PaymentMethodRequest(card: card));
      var paymentIntent =
          await StripeService.createPaymentIntent(amount, currency);
      var response = await StripePayment.confirmPaymentIntent(PaymentIntent(
          clientSecret: paymentIntent['client_secret'],
          paymentMethodId: paymentMethod.id));
      if (response.status == 'succeeded') {
        return new StripeTransactionResponse(
            message: 'Transaction successful', success: true);
      } else {
        return new StripeTransactionResponse(
            message: 'Transaction failed', success: false);
      }
    } on PlatformException catch (err) {
      return StripeService.getPlatformExceptionErrorResult(err);
    } catch (err) {
      return new StripeTransactionResponse(
          message: 'Transaction failed: ${err.toString()}', success: false);
    }
  }

  static Future<StripeTransactionResponse> payWithNewCard(
      {String amount, String currency}) async {
    try {
      var paymentMethod = await StripePayment.paymentRequestWithCardForm(
          CardFormPaymentRequest());

      if (paymentMethod != null) {
        return await AuthService()
            .addSuscription(paymentMethod.id)
            .then((value) {
          if (value) {
            print("ont entre ici");
            return new StripeTransactionResponse(
                message: 'Transaction successful', success: true);
          } else {
            return new StripeTransactionResponse(
                message: 'Transaction failed', success: false);
          }
        }).catchError((onError) {
          return new StripeTransactionResponse(
              message: 'Transaction failed', success: false);
        });
      } else {
        return new StripeTransactionResponse(
            message: 'Transaction failed', success: false);
      }
    } on PlatformException catch (err) {
      return StripeService.getPlatformExceptionErrorResult(err);
    } catch (err) {
      return new StripeTransactionResponse(
          message: 'Transaction failed: ${err.toString()}', success: false);
    }
  }

  static getPlatformExceptionErrorResult(err) {
    String message = 'Something went wrong';
    if (err.code == 'cancelled') {
      message = 'Transaction cancelled';
    }

    return new StripeTransactionResponse(message: message, success: false);
  }

  static Future<Map<String, dynamic>> createPaymentIntent(
      String amount, String currency) async {
    try {
      Map<String, dynamic> body = {
        'amount': amount,
        'currency': currency,
        'payment_method_types[]': 'card'
      };
      print("on entre ici");
      var response = await post(StripeService.paymentApiUrl, body);
      print("response");
      print(response.data);
      return response.data;
    } catch (err) {
      print('err charging user: ${err.toString()}');
    }
    return null;
  }

  static Future post(route, params) {
    Dio _dio;
    BaseOptions _options;
    print("******************************************************************");
    _options = BaseOptions(baseUrl: DotEnv().env["ENDPOINT"], headers: {
      'Authorization': 'Bearer ${StripeService.secret}',
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    _dio = Dio(_options);
    print("paramss");
    print(params);
    return _dio.post(route, data: params);
  }
}
