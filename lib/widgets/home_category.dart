import 'package:flutter/material.dart';
import 'package:healing/services/authService.dart';
import 'package:fluttertoast/fluttertoast.dart';

class HomeCategoryWidget extends StatelessWidget {
  const HomeCategoryWidget(
      {Key key,
      @required this.title,
      this.image,
      this.color,
      this.therapeutist = 0,
      this.onTap,
      this.lat,
      this.long})
      : super(key: key);

  final String title;
  final Color color;
  final Image image;
  final int therapeutist;
  final Function onTap;
  final String lat;
  final String long;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap ??
          () {
            if (AuthService.username == null) {
              print(AuthService.token);
              print("sans token");
              Fluttertoast.showToast(
                msg: 'You must log in to search',
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.TOP,
                backgroundColor: Color(0xFF011424),
                textColor: Colors.white,
                fontSize: 15.0,
              );
            } else {
              /*
              if (lat != "" && long != "") {
                Navigator.pushNamed(context, '/therapeutistList',
                    arguments:
                        this.title + "&longitude=" + long + "&latitude=" + lat);
              } else {
                Navigator.pushNamed(context, '/therapeutistList',
                    arguments: this.title);
              }
              */
              Navigator.pushNamed(context, '/therapeutistList',
                  arguments: this.title);
            }
          },
      child: Container(
        height: 70,
        padding: EdgeInsets.all(15.0),
        margin: EdgeInsets.only(bottom: 15.0),
        decoration: BoxDecoration(
          border: Border.all(color: Color(0xFF9C27B0)),
          borderRadius: BorderRadius.circular(22.0),
        ),
        child: Row(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                color: color ?? Colors.blue,
                borderRadius: BorderRadius.circular(80.0),
              ),
              child: image ??
                  Image.asset('assets/icons/acupuncture.png',
                      width: 50, height: 50, fit: BoxFit.fill),
            ),
            SizedBox(width: 30.0),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        title,
                        style: TextStyle(
                          color: Color(0xFF011424),
                          fontFamily: 'Source SemiBold',
                          fontSize: 14.0,
                        ),
                      ),
                      SizedBox(height: 5.0),
                      Text(
                        '$therapeutist Therapeutist',
                        style: TextStyle(
                          color: Color(0xFF011424),
                          fontFamily: 'Source',
                          fontSize: 11.0,
                        ),
                      ),
                    ],
                  ),
                  Icon(
                    Icons.chevron_right,
                    color: Color(0xFF011424),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
