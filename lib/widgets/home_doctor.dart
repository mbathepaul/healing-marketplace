import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:healing/core/models/doctor.dart';

class HomeDoctorWidget extends StatelessWidget {
  const HomeDoctorWidget({
    Key key,
    @required this.name,
    this.speciality,
    this.rate,
    this.image,
    this.onTap,
    this.doctor,
  }) : super(key: key);

  final String name;
  final String speciality;
  final double rate;
  final String image;
  final Function onTap;
  final Doctor doctor;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap ??
          () {
            Navigator.pushNamed(context, '/therapeutist',
                arguments: this.doctor);
          },
      child: Container(
        height: 130.0,
        width: 240.0,
        decoration: BoxDecoration(
          color: Colors.white.withOpacity(0.6),
          borderRadius: BorderRadius.circular(20.0),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.1),
              spreadRadius: 0.0,
              blurRadius: 10.0,
              offset: Offset(0, 10.0),
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  height: 150.0,
                  width: 250.0,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    image: DecorationImage(
                      image: NetworkImage(this.image) ??
                          AssetImage('assets/images/doctors/1.jpg'),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                ),
                Positioned(
                  top: 15.0,
                  right: 25.0,
                  child: GestureDetector(
                    onTap: () {},
                    child: Container(
                      height: 30.0,
                      width: 30.0,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(500.0),
                      ),
                      child: Icon(
                        Icons.favorite,
                        color: Colors.red.shade400,
                        size: 15.0,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 10.0),
            Container(
              width: 250.0,
              margin: EdgeInsets.symmetric(horizontal: 10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    '$name',
                    style: TextStyle(
                        fontFamily: 'Source SemiBold',
                        fontWeight: FontWeight.bold,
                        color: Colors.black.withOpacity(0.9)),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        speciality ?? 'Specialist Dentist',
                        style: TextStyle(
                            fontFamily: 'Source',
                            color: Colors.black.withOpacity(0.9)),
                      ),
                      Row(
                        children: <Widget>[
                          Icon(
                            Icons.star,
                            color: Colors.orange,
                            size: 16.0,
                          ),
                          Text(
                            doctor.rate != null
                                ? doctor.rate.toStringAsFixed(1)
                                : "0.0",
                            style: TextStyle(
                                fontFamily: 'Source SemiBold',
                                fontSize: 13.0,
                                color: Colors.black.withOpacity(0.9)),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
