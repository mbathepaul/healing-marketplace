import 'package:flutter/material.dart';

class TagButton extends StatelessWidget {
  const TagButton({
    Key key,
    this.title,
    this.color,
  }) : super(key: key);

  final String title;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {},
      child: Container(
        alignment: Alignment.center,
        height: 33.0,
        padding: EdgeInsets.symmetric(horizontal: 10),
        width: title.length.toDouble() * 12,
        decoration: BoxDecoration(
          color: color ?? Color(0xFF92C384),
          borderRadius: BorderRadius.circular(20.0),
        ),
        child: Text(
          title ?? 'Cancer',
          style: TextStyle(
            color: Colors.white,
            fontFamily: 'Coves Bold',
          ),
        ),
      ),
    );
  }
}
