import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:healing/core/models/doctor.dart';
import 'package:healing/services/authService.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Therapeutist extends StatelessWidget {
  const Therapeutist({
    Key key,
    this.name,
    this.speciality,
    this.rate,
    this.image,
    this.onTap,
    this.doctor,
    this.clique,
  }) : super(key: key);

  final String name;
  final String speciality;
  final double rate;
  final String image;
  final Function onTap;
  final Doctor doctor;
  final clique;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap ??
          () {
            print("on passe par icivnoanvonvnnnnnnnnnnooooooooooooooooooo");
            if (clique == true) {
              Navigator.pushNamed(context, '/therapeutist',
                  arguments: this.doctor);
            }
          },
      child: Container(
        margin: EdgeInsets.only(bottom: 10.0, top: 10.0),
        height: 100.0,
        width: MediaQuery.of(context).size.width,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: FadeInImage(
                  width: 140.0,
                  placeholder: AssetImage('assets/images/doctors/2.jpg'),
                  image: image != null
                      ? NetworkImage(image)
                      : AssetImage('assets/images/doctors/2.jpg'),
                )),
            SizedBox(
              width: 30.0,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  name != null ? name : 'Christine Swords',
                  style: TextStyle(
                    fontFamily: 'Coves Bold',
                    fontSize: 17.0,
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  doctor.label != null ? doctor.label : 'Neurology specialist',
                  style: TextStyle(
                    fontFamily: 'Source',
                    color: Colors.grey,
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Row(
                  children: [
                    RatingBar.builder(
                      initialRating: rate != null ? rate : 0.0,
                      minRating: 0,
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      itemCount: 5,
                      itemSize: 15.0,
                      itemBuilder: (context, _) => Icon(
                        Icons.star,
                        color: Colors.amber,
                        size: 10.0,
                      ),
                      onRatingUpdate: (rating) {
                        if (AuthService.is_therapist == false) {
                          AuthService()
                              .updatereviews(doctor.id.toString(), rating)
                              .then((value) {
                            if (value) {
                              Fluttertoast.showToast(
                                msg: 'rating updated successfully!',
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.BOTTOM,
                                backgroundColor: Color(0xFF011424),
                                textColor: Colors.white,
                                fontSize: 15.0,
                              );
                              rating = rating;
                            } else {
                              Fluttertoast.showToast(
                                msg:
                                    'An error occured during the update rating',
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.BOTTOM,
                                backgroundColor: Color(0xFF011424),
                                textColor: Colors.white,
                                fontSize: 15.0,
                              );
                              print("ça ne marche pas");
                            }
                          });
                        } else {
                          Fluttertoast.showToast(
                            msg: 'Only customers can update rating',
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.BOTTOM,
                            backgroundColor: Colors.red,
                            textColor: Colors.white,
                            fontSize: 13.0,
                          );
                        }
                      },
                    ),
                    SizedBox(
                      width: 5.0,
                    ),
                    Text(
                      rate != null ? rate.toStringAsFixed(1) : "4.3",
                      style: TextStyle(
                        fontFamily: 'Source',
                        fontSize: 10.0,
                        color: Colors.black,
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 8.0,
                ),
                Row(children: [
                  Icon(
                    Icons.location_on,
                    size: 15,
                  ),
                  Container(
                    width: 130,
                    child: Text(
                      doctor.address != null ? doctor.address : '',
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontFamily: 'Source',
                          color: Colors.grey,
                          fontWeight: FontWeight.bold,
                          fontSize: 12),
                    ),
                  )
                ])
              ],
            )
          ],
        ),
      ),
    );
  }
}
