import 'package:flutter/material.dart';

class CustomField extends StatelessWidget {
  const CustomField({
    Key key,
    @required this.label,
    @required this.controller,
    this.helper,
    this.bottomMargin = 10.0,
    this.type = TextInputType.text,
    this.isPassword = false,
    this.actionText,
    this.actionCallback,
    this.maxline,
    this.color,
  }) : super(key: key);

  final String label;
  final String helper;
  final String actionText;
  final double bottomMargin;
  final bool isPassword;
  final TextInputType type;
  final TextEditingController controller;
  final Function actionCallback;
  final int maxline;
  final bool color;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
      margin: EdgeInsets.only(bottom: bottomMargin),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5.0),
        border: Border.all(
            color: color == null ? Color(0xFF9C27B0) : Color(0xFF7912DD),
            width: 1.5),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.2),
            spreadRadius: 0.0,
            blurRadius: 10.0,
            offset: Offset(0, 8.0),
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                label,
                style: TextStyle(
                  fontFamily: 'Source SemiBold',
                  fontSize: 13.0,
                  color: Color(0xFF474E6C),
                ),
              ),
              if (actionText != null)
                GestureDetector(
                  onTap: actionCallback ?? () {},
                  child: Text(
                    actionText,
                    style: TextStyle(
                      fontFamily: 'Source SemiBold',
                      fontSize: 11.0,
                      color: Color(0xFF474E6C).withOpacity(0.3),
                    ),
                  ),
                ),
            ],
          ),
          SizedBox(height: 3.0),
          TextField(
            controller: controller,
            keyboardType: type,
            obscureText: isPassword,
            maxLines: maxline ?? 1,
            style: TextStyle(
              fontFamily: 'Source SemiBold',
              fontSize: 12.0,
              color: Color(0xFF474E6C).withOpacity(0.8),
            ),
            decoration: InputDecoration(
              isDense: true,
              floatingLabelBehavior: FloatingLabelBehavior.never,
              contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 5.0),
              labelText: helper,
              border: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent),
              ),
              labelStyle: TextStyle(
                fontFamily: 'Source SemiBold',
                fontSize: 12.0,
                color: Color(0xFF474E6C).withOpacity(0.5),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
