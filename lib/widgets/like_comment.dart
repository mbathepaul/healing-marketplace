import 'package:flutter/material.dart';

class LikeComments extends StatefulWidget {
  const LikeComments({
    Key key,
  }) : super(key: key);

  @override
  _LikeCommentsState createState() => _LikeCommentsState();
}

class _LikeCommentsState extends State<LikeComments> {
  bool selected = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: Row(
        children: [
          Column(
            children: [
              GestureDetector(
                onTap: () {
                  print('like comment');

                  if (selected) {
                    selected = true;
                  } else {
                    selected = false;
                  }
                },
                child: Icon(
                  Icons.check,
                  size: 15.0,
                  color: selected == false ? Colors.grey : Colors.blue,
                ),
              ),
              Text(
                '480 likes',
                style: TextStyle(
                  fontFamily: 'Source',
                  fontSize: 10.0,
                  color: Colors.grey.shade500,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
