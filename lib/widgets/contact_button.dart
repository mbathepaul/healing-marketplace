import 'package:flutter/material.dart';
import 'package:healing/screens/auth/register.dart';
import 'package:healing/services/CallsAndMessagesService.dart';
import 'package:healing/services/service_locator.dart';

class ContactBookingButton extends StatefulWidget {
  const ContactBookingButton(
      {Key key, this.title, this.icon, this.type, this.payload})
      : super(key: key);

  final String title;
  final Icon icon;
  final String type;
  final String payload;

  @override
  _ContactBookingButtonState createState() => _ContactBookingButtonState();
}

class _ContactBookingButtonState extends State<ContactBookingButton> {
  final CallsAndMessagesService _service = locator<CallsAndMessagesService>();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        print(widget.type);
        if (widget.type == "call") {
          _service.call(widget.payload);
        }
        if (widget.type == "message") {
          _service.sendSms(widget.payload);
        }
        if (widget.type == "email") {
          _service.sendEmail(widget.payload);
        }
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            alignment: Alignment.center,
            height: 50.0,
            width: 50.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(500.0),
              color: Color(0xFF320A3B),
            ),
            child: widget.icon ??
                Icon(
                  Icons.call,
                  color: Colors.white,
                ),
          ),
          Text(
            widget.title ?? 'Call',
            style: TextStyle(
              fontFamily: 'Coves Bold',
              fontSize: 15.0,
            ),
          )
        ],
      ),
    );
  }
}
