import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:healing/core/providers/theme_provider.dart';
import 'package:healing/services/authService.dart';
import 'package:provider/provider.dart';
import 'core/healing.dart';
import 'package:healing/services/CallsAndMessagesService.dart';
import 'package:healing/services/service_locator.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'core/models/doctor.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  /// Initializing DotEnv plugin
  FlutterSecureStorage _localStorage = new FlutterSecureStorage();
  await DotEnv().load('.env');

  setupLocator();

  Map<String, String> value = await _localStorage.readAll();

  AuthService.username = value["username"];
  AuthService.email = value["email"];
  AuthService.id = value["id"];
  AuthService.is_therapist = value["is_therapist"] == "true";
  AuthService.token = value["token"];

  if (AuthService.is_therapist) {
    Doctor doctor = Doctor(
      id: value["id"],
      user: value["username"],
      email: value["email"],
      description: value["description"],
      label: value["category"],
      region: value["region"],
      address: value["address"],
      city: value["city"],
      phone: value["phone"],
      website: value["website"],
      facebook: value["facebook"],
      instalgram: value["instagram"],
      end_subscription: value["end_subscription"],
      nb_reviews: int.parse(value["nb_reviews"]),
      photos: [value["image"]],
    );
    AuthService.doctor = doctor;

    print(AuthService.token);
  }
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => ThemeProvider(),
        ),
      ],
      child: Healing(
        is_login: AuthService.token != null,
      ),
    ),
  );
}
