# Healing Marketplace

The Healing Marketplace Mobile App.

## Getting Started

The goal is to create an app to find healing doctors.

TODO (Screens):

- Auth (Sign up, Sign in) []
- Dashboard (Categories, Doctors, Navigation) []
- Search Doctors (With Filters) []
- Doctor Details []
- Book Doctor []
- Recent Doctors []
- Settings []
- Payment []